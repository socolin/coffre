#!/bin/sh

LIBRESSL_PATH=$1

if test "Z$LIBRESSL_PATH" = "Z"
then
  echo $0 "<path to libressl> if you don't have libressl, see http://penzin.net/libressl.html"
  exit 1
fi

if ! test -d $LIBRESSL_PATH
then
  echo 'Invalid path to libressl directory: '$LIBRESSL_PATH
  exit 1
fi

LIBRESSL_INCLUDE_PATH=${LIBRESSL_PATH}/include
if ! test -d $LIBRESSL_INCLUDE_PATH
then
  echo 'Missing include/ directory inside libressl path: '$LIBRESSL_INCLUDE_PATH
  exit 1
fi

LIBRESSL_LIB_PATH=${LIBRESSL_PATH}/lib
if ! test -d $LIBRESSL_LIB_PATH
then
  echo 'Missing lib/ directory inside libressl path: '$LIBRESSL_LIB_PATH
  exit 1
fi

echo 'CPPFLAGS+=-D_GNU_SOURCE -D_BSD_SOURCE `pkg-config --cflags libbsd-overlay` -D_DEFAULT_SOURCE -I'${LIBRESSL_INCLUDE_PATH} > Makefile.linux
echo 'LDLIBS+=-lcrypto -lbsd -lresolv -L'${LIBRESSL_LIB_PATH} >> Makefile.linux
echo 'LIBRESSL_LIB_PATH='${LIBRESSL_LIB_PATH} >> Makefile.linux
echo 'SRC_FILES+= compat/explicit_bzero.c compat/recallocarray.c' >> Makefile.linux

echo run coffre with LD_LIBRARY_PATH='"'${LIBRESSL_LIB_PATH}'" ./coffre'
