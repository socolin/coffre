#ifndef COFFRE_MSG_PROTOCOL_H
#define COFFRE_MSG_PROTOCOL_H

#define PROTO_MAX_PASSPHRASE_LENGTH 512

#include <stdbool.h>
#include "utils/string.h"
#include "types.h"

enum msg_opcode {
    OP_MSG_AUTH = 1,
    OP_MSG_AUTH_TOKEN,
    OP_MSG_AUTH_RESULT,
    OP_MSG_DEAUTH,
    OP_MSG_CREATE_SAFE,
    OP_MSG_CREATE_SAFE_RESULT,
    OP_MSG_CREATE_SECONDARY_KEY,
    OP_MSG_LIST_DIRECTORY,
    OP_MSG_LIST_DIRECTORY_RESULT,
    OP_MSG_START_UPLOAD,
    OP_MSG_START_UPLOAD_RESULT,
    OP_MSG_UPLOAD,
    OP_MSG_UPLOAD_RESULT,
    OP_MSG_UPLOAD_COMPLETE,
    OP_MSG_UPLOAD_COMPLETE_RESULT,
    OP_MSG_UPLOAD_CANCEL,
    OP_MSG_UPLOAD_CANCEL_RESULT,
    OP_MSG_SHARE_KEY,
    OP_MSG_SHARE_KEY_RESULT,
    OP_MSG_START_DOWNLOAD,
    OP_MSG_START_DOWNLOAD_RESULT,
    OP_MSG_DOWNLOAD,
    OP_MSG_DOWNLOAD_RESULT,
    OP_MSG_DOWNLOAD_CANCEL,
    OP_MSG_DOWNLOAD_CANCEL_RESULT,
    OP_MSG_DELETE_FILE,
    OP_MSG_DELETE_FILE_RESULT,
    OP_MSG_ERROR_RESULT,
    OP_MSG_MAX,
};

struct msg_generic {
    clientid client_id;
    operationid operation_id;
};

struct msg_generic *msg_generic_init(const clientid, operationid);

struct msg_generic_result {
    clientid client_id;
    operationid operation_id;
    bool success;
};

struct msg_generic_result *msg_generic_result_init(const clientid, operationid, bool success);

struct msg_error_result {
    clientid client_id;
    operationid operation_id;
    // FIXME: remove succes from all
    bool success;
    size_t data_size;
    char data[0];
};

struct string *msg_error_result_init(const clientid, const operationid, const char *, va_list);

struct msg_generic_data_result {
    clientid client_id;
    operationid operation_id;
    bool success;
    size_t data_size;
    char data[0];
};

struct msg_auth_result_data {
    clientid client_id;
    operationid operation_id;
    bool success;
    enum safe_mode mode;
};

struct msg_auth_result_data *msg_auth_result_data_init(const clientid, operationid, bool success, enum safe_mode mode);

struct string *msg_generic_data_result_init(const clientid, operationid, bool success);

struct msg_auth_data {
    clientid client_id;
    operationid operation_id;
    safe_name name;
    char passphrase[PROTO_MAX_PASSPHRASE_LENGTH];
};

struct msg_auth_data *msg_auth_data_init(const clientid, operationid, struct string *, struct string *);

struct msg_auth_token_data {
    clientid client_id;
    operationid operation_id;
    share_token token;
};

struct msg_auth_token_data *msg_auth_token_data_init(const clientid, operationid, struct string *);

struct msg_create_safe_data {
    clientid client_id;
    operationid operation_id;
    safe_name name;
    char passphrase[PROTO_MAX_PASSPHRASE_LENGTH];
};

struct msg_create_safe_data *msg_create_safe_data_init(const clientid, operationid, struct string *, struct string *);

struct msg_list_directory_data {
    clientid client_id;
    operationid operation_id;
    fileuid file_uid;
};

struct msg_list_directory_data *msg_list_directory_data_init(const clientid, operationid, struct string *);

struct msg_list_directory_result_data {
    clientid client_id;
    operationid operation_id;
    bool success;
    fileuid file_uid;
    size_t data_size;
    char data[0];
};

struct string *msg_list_directory_result_data_init(const clientid, operationid, const char *, bool);

struct msg_start_upload_data {
    clientid client_id;
    operationid operation_id;
    fileuid directory_uid;
    size_t data_size;
    char data[0];
};

struct string *msg_start_upload_data_init(const clientid, operationid, struct string *, struct string *);

struct msg_upload_data {
    clientid client_id;
    operationid operation_id;
    size_t data_size;
    char data[0];
};

struct string *msg_upload_data_init(const clientid, operationid, struct string *);

struct msg_share_key_data {
    clientid client_id;
    operationid operation_id;
    uint8_t mode;
};

struct msg_share_key_data *msg_share_key_data_init(const clientid, operationid, uint8_t);

struct msg_share_key_result_data {
    clientid client_id;
    operationid operation_id;
    bool success;
    char token[SHARE_KEY_TOKEN_LEN];
};

struct msg_share_key_result_data *msg_share_key_result_data_init(const clientid, operationid, bool, const char *);

struct msg_start_download_data {
    clientid client_id;
    operationid operation_id;
    fileuid file_uid;
};

struct msg_start_download_data *msg_start_download_data_init(const clientid, operationid, const struct string *);

struct msg_delete_file_data {
    clientid client_id;
    operationid operation_id;
    fileuid file_uid;
    fileuid directory_uid;
};

struct msg_delete_file_data *msg_delete_file_data_init(const clientid, operationid, const struct string *, const struct string *);


#endif //COFFRE_MSG_PROTOCOL_H
