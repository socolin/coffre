#ifndef COFFRE_PRIVSEP_H
#define COFFRE_PRIVSEP_H

#include <sys/types.h>

#include "msg_queue.h"

#define PRIVSEP_MSG_QUEUE_FILENO 3
#define PRIVSEP_CHILD_NAME_LEN 32
#define PRIVSEP_MAX_CHILD 10

#define PRIVSEP_MASTER_NAME "master"
#define PRIVSEP_NETWORK_NAME "netw"
#define PRIVSEP_SAFE_NAME "safe"

struct privsep_process {
    pid_t pid;
    int fd;
    char name[PRIVSEP_CHILD_NAME_LEN];
    struct msg_queue *queue;
    void *udata;
};

void privsep_process_destroy(struct privsep_process *process);
struct privsep_process *privsep_get_child_by_name(const char *name);
struct privsep_process *start_child(const char *, int (*)(struct privsep_process *), struct event_base *, process_msg_callback);

#endif //COFFRE_PRIVSEP_H
