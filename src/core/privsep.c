#include "privsep.h"

#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdio.h>
#include <event.h>

#include "conf.h"
#include "utils/log.h"
#include "msg_queue.h"
#include "compat/linux.h"

struct privsep_process *g_privsep_child[PRIVSEP_MAX_CHILD];
size_t g_privsep_count = 0;

static struct privsep_process *
privsep_process_init(const char *name, int fd)
{
    struct privsep_process *process = NULL;

    process = calloc(1, sizeof(struct privsep_process));
    if (process == NULL) {
        log_critical("Failed to calloc process struct: %m");
        return NULL;
    }

    process->fd = fd;
    strlcpy(process->name, name, sizeof(process->name));

    return process;
}

void
privsep_process_destroy(struct privsep_process *process)
{
    if (process == NULL) {
        return;
    }

    close(process->fd);
    msg_queue_destroy(process->queue);
    freezero(process, sizeof(*process));
}

struct privsep_process *
privsep_get_child_by_name(const char *name)
{
    struct privsep_process *child = NULL;

    for (size_t i = 0; i < g_privsep_count; i++) {
        if (strncmp(g_privsep_child[i]->name, name, PRIVSEP_CHILD_NAME_LEN) == 0) {
            child = g_privsep_child[i];
            break;
        }
    }

    return child;
}

struct privsep_process *
start_child(const char *name,
        int (*func)(struct privsep_process *),
        struct event_base *event,
        process_msg_callback msg_queue_cb)
{
    char buffer[64];
    pid_t pid;
    int sp[2];

    if (g_privsep_count >= PRIVSEP_MAX_CHILD) {
        log_critical("Too many child");
        return NULL;
    }

    if (strlen(name) >= PRIVSEP_CHILD_NAME_LEN) {
        log_critical("Child name too long");
        return NULL;
    }

    if (socketpair(AF_UNIX, SOCK_STREAM | SOCK_NONBLOCK, PF_UNSPEC, sp) == -1) {
        log_error("Failed to create socketpair: %m");
        return NULL;
    }

    pid = fork();
    switch (pid) {
        case -1: {
            log_error("Failed to fork %m");
            close(sp[0]);
            close(sp[1]);
            return NULL;
        }
        case 0: {
            setproctitle("%s", name);
            close(sp[1]);
            closelog();
            if (dup2(sp[0], PRIVSEP_MSG_QUEUE_FILENO) == -1) {
                log_error("Failed to dup2 %m");
                exit(1);
            }
            closefrom(PRIVSEP_MSG_QUEUE_FILENO + 1);

            struct privsep_process *parent = privsep_process_init(PRIVSEP_MASTER_NAME, PRIVSEP_MSG_QUEUE_FILENO);
            if (parent == NULL) {
                exit(1);
            }
            snprintf(buffer, sizeof(buffer), "coffre[%s]", name);
            openlog(buffer, g_config.log_flags, LOG_DAEMON);
            setlogmask(LOG_UPTO(g_config.log_level));

            func(parent);
            exit(0);
        }
        default: {
            close(sp[0]);

            struct privsep_process *child = privsep_process_init(name, sp[1]);
            if (child == NULL) {
                return NULL;
            }
            child->pid = pid;

            child->queue = msg_queue_init(child, event, msg_queue_cb);
            if (child->queue == NULL) {
                log_error("Failed to init `%s` queue", child->name);
                privsep_process_destroy(child);
                return NULL;
            }

            g_privsep_child[g_privsep_count++] = child;

            return child;
        }
    }
}
