#ifndef COFFRE_CONF_H
#define COFFRE_CONF_H

#include <sys/types.h>
#include <limits.h>
#include <arpa/inet.h>

#define DEFAULT_CONF_PATH "/etc/coffre.conf"

struct conf {
    int debug;
    char root[PATH_MAX];
    char username[LOGIN_NAME_MAX];
    uid_t userid;
    char groupname[LOGIN_NAME_MAX];
    uid_t groupid;
    in_port_t port;
    char ssl_cert[PATH_MAX];
    char ssl_key[PATH_MAX];
    char ssl_protocols[256];
    int log_level;
    int log_flags;
};

extern struct conf g_config;

void conf_init();
int conf_load(const char *);

#endif //COFFRE_CONF_H
