#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pwd.h>
#include <grp.h>

#include "conf.h"
#include "utils/log.h"

struct conf g_config;

static int
start_with(const char *str, ssize_t length, const char *start, int *pos)
{
    *pos = 0;
    while (*pos < length) {
        if (start[*pos] == '\0') {
            return 1;
        }
        if (str[*pos] != start[*pos]) {
            return 0;
        }
        (*pos)++;
    }

    return 1;
}

static int
parse_line(const char *line, ssize_t length)
{
    int pos = 0;
    struct passwd *user = NULL;
    struct group *group = NULL;
    const char *errstr;

    if (start_with(line, length, "root=", &pos)) {
        strlcpy(g_config.root, line + pos, sizeof(g_config.root));
        return 1;
    }

    if (start_with(line, length, "ssl_cert=", &pos)) {
        strlcpy(g_config.ssl_cert, line + pos, sizeof(g_config.ssl_cert));
        return 1;
    }

    if (start_with(line, length, "ssl_key=", &pos)) {
        strlcpy(g_config.ssl_key, line + pos, sizeof(g_config.ssl_key));
        return 1;
    }

    if (start_with(line, length, "ssl_protocols=", &pos)) {
        strlcpy(g_config.ssl_protocols, line + pos, sizeof(g_config.ssl_protocols));
        return 1;
    }

    if (start_with(line, length, "user=", &pos)) {
        strlcpy(g_config.username, line + pos, sizeof(g_config.username));
        user = getpwnam(g_config.username);
        if (user == NULL) {
            log_error("User not found: `%s'", g_config.username);
            return 0;
        }
        g_config.userid = user->pw_uid;
        return 1;
    }

    if (start_with(line, length, "group=", &pos)) {
        strlcpy(g_config.groupname, line + pos, sizeof(g_config.groupname));
        group = getgrnam(g_config.groupname);
        if (group == NULL) {
            log_error("Group not found: `%s'", g_config.groupname);
            return 0;
        }
        g_config.groupid = group->gr_gid;
        return 1;
    }

    if (start_with(line, length, "port=", &pos)) {
        long long port = strtonum(line + pos, 1, 65535, &errstr);
        if (errstr) {
            log_error("Invalid port `%s': %s", line + pos, errstr);
            return 0;
        }
        g_config.port = (uint16_t) (port);
        return 1;
    }

    return 0;
}

void
conf_init()
{
    bzero(&g_config, sizeof(g_config));
    g_config.port = 27972;
}

int
conf_load(const char *file_path)
{
    int rc = 0;
    char *line = NULL;
    FILE *conf_stream = NULL;
    ssize_t count = 0;
    size_t len = 0;
    int line_count = 1;

    log_info("Loading configuration: `%s'", file_path);

    conf_stream = fopen(file_path, "r");
    if (conf_stream == 0) {
        log_error("Failed to open configuration file `%s': %m", file_path);
        rc = -1;
        goto end;
    }

    while ((count = getline(&line, &len, conf_stream)) != -1) {
        line[--count] = '\0';
        if (!parse_line(line, count)) {
            log_error("Error while parsing configuration file: `%s' at line: %d", line, line_count);
            rc = -1;
            goto close;
        }
        line_count++;
    }

close:
    free(line);
    fclose(conf_stream);
end:
    return rc;
}
