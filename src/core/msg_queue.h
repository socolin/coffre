#ifndef COFFRE_MSG_QUEUE_H
#define COFFRE_MSG_QUEUE_H

#include <event.h>
#include "msg_queue.h"
#include "utils/string.h"

#ifdef DEBUG
#define MAX_QUEUE_SIZE 5
#else
#define MAX_QUEUE_SIZE 256
#endif

struct msg;
struct msg_queue;
struct privsep_process;

typedef int (*process_msg_callback)(struct msg_queue *, uint64_t, size_t, char *);

struct msg_queue *msg_queue_init(struct privsep_process *, struct event_base *, process_msg_callback);
void msg_queue_destroy(struct msg_queue *);

void msg_queue_on_read_event(struct bufferevent *, void *);
void msg_queue_on_write_event(struct bufferevent *, void *);
void msg_queue_on_error_event(struct bufferevent *, short, void *);
int msg_queue_send(struct msg_queue *, struct msg *);
struct privsep_process *msg_queue_get_process(struct msg_queue *);
struct bufferevent *msg_queue_get_event_buffer(struct msg_queue *);

/**
 * Create a new msg to be sent in msg queue
 * data args should be a malloc pointer, which will be freezero once msg will be sent
 */
struct msg *msg_init(uint64_t, size_t, void *);
/**
 * data args should be a malloc pointer, which will be freezero once msg will be sent
 */
int msg_queue_send_init(struct msg_queue *, uint64_t, size_t, void *);
/**
 * data string will be destroy
 */
int msg_queue_send_sinit(struct msg_queue *, uint64_t, struct string *);
void msg_destroy(struct msg *);

#endif //COFFRE_MSG_QUEUE_H
