#include "privsep_master.h"

#include <inttypes.h>
#include <string.h>

#include "privsep.h"
#include "msg_queue.h"
#include "msg_protocol.h"
#include "utils/log.h"

struct privsep_opcode {
    const char *name;
    const char *source;
    const char *target;
    size_t strict_packet_size;
    size_t min_packet_size;
};

struct privsep_opcode opcode_info[OP_MSG_MAX] = {
        {"OP_MSG_AUTH",                   PRIVSEP_NETWORK_NAME, PRIVSEP_SAFE_NAME,    sizeof(struct msg_auth_data),             0},
        {"OP_MSG_AUTH_TOKEN",             PRIVSEP_NETWORK_NAME, PRIVSEP_SAFE_NAME,    sizeof(struct msg_auth_token_data),       0},
        {"OP_MSG_AUTH_RESULT",            PRIVSEP_SAFE_NAME,    PRIVSEP_NETWORK_NAME, sizeof(struct msg_auth_result_data),      0},
        {"OP_MSG_DEAUTH",                 PRIVSEP_NETWORK_NAME, PRIVSEP_SAFE_NAME,    sizeof(struct msg_generic),               0},
        {"OP_MSG_CREATE_SAFE",            PRIVSEP_NETWORK_NAME, PRIVSEP_SAFE_NAME,    sizeof(struct msg_create_safe_data),      0},
        {"OP_MSG_CREATE_SAFE_RESULT",     PRIVSEP_SAFE_NAME,    PRIVSEP_NETWORK_NAME, sizeof(struct msg_auth_result_data),      0},
        {"OP_MSG_CREATE_SECONDARY_KEY",   PRIVSEP_NETWORK_NAME, PRIVSEP_SAFE_NAME,    0,                                        0},
        {"OP_MSG_LIST_DIRECTORY",         PRIVSEP_NETWORK_NAME, PRIVSEP_SAFE_NAME,    sizeof(struct msg_list_directory_data),   0},
        {"OP_MSG_LIST_DIRECTORY_RESULT",  PRIVSEP_SAFE_NAME,    PRIVSEP_NETWORK_NAME, 0,                                        sizeof(struct msg_list_directory_result_data)},
        {"OP_MSG_START_UPLOAD",           PRIVSEP_NETWORK_NAME, PRIVSEP_SAFE_NAME,    0,                                        sizeof(struct msg_start_upload_data)},
        {"OP_MSG_START_UPLOAD_RESULT",    PRIVSEP_SAFE_NAME,    PRIVSEP_NETWORK_NAME, 0,                                        sizeof(struct msg_generic_data_result)},
        {"OP_MSG_UPLOAD",                 PRIVSEP_NETWORK_NAME, PRIVSEP_SAFE_NAME,    0,                                        sizeof(struct msg_upload_data)},
        {"OP_MSG_UPLOAD_RESULT",          PRIVSEP_SAFE_NAME,    PRIVSEP_NETWORK_NAME, sizeof(struct msg_generic_result),        0},
        {"OP_MSG_UPLOAD_COMPLETE",        PRIVSEP_NETWORK_NAME, PRIVSEP_SAFE_NAME,    sizeof(struct msg_generic),               0},
        {"OP_MSG_UPLOAD_COMPLETE_RESULT", PRIVSEP_SAFE_NAME,    PRIVSEP_NETWORK_NAME, 0,                                        sizeof(struct msg_generic_data_result)},
        {"OP_MSG_UPLOAD_CANCEL",          PRIVSEP_NETWORK_NAME, PRIVSEP_SAFE_NAME,    sizeof(struct msg_generic),               0},
        {"OP_MSG_UPLOAD_CANCEL_RESULT",   PRIVSEP_SAFE_NAME,    PRIVSEP_NETWORK_NAME, sizeof(struct msg_generic_result),        0},
        {"OP_MSG_SHARE_KEY",              PRIVSEP_NETWORK_NAME, PRIVSEP_SAFE_NAME,    sizeof(struct msg_share_key_data),        0},
        {"OP_MSG_SHARE_KEY_RESULT",       PRIVSEP_SAFE_NAME,    PRIVSEP_NETWORK_NAME, sizeof(struct msg_share_key_result_data), 0},
        {"OP_MSG_START_DOWNLOAD",         PRIVSEP_NETWORK_NAME, PRIVSEP_SAFE_NAME,    sizeof(struct msg_start_download_data),   0},
        {"OP_MSG_START_DOWNLOAD_RESULT",  PRIVSEP_SAFE_NAME,    PRIVSEP_NETWORK_NAME, sizeof(struct msg_generic_result),        0},
        {"OP_MSG_DOWNLOAD",               PRIVSEP_NETWORK_NAME, PRIVSEP_SAFE_NAME,    sizeof(struct msg_generic),               0},
        {"OP_MSG_DOWNLOAD_RESULT",        PRIVSEP_SAFE_NAME,    PRIVSEP_NETWORK_NAME, 0,                                        sizeof(struct msg_generic_result)},
        {"OP_MSG_DOWNLOAD_CANCEL",        PRIVSEP_NETWORK_NAME, PRIVSEP_SAFE_NAME,    sizeof(struct msg_generic),               0},
        {"OP_MSG_DOWNLOAD_CANCEL_RESULT", PRIVSEP_SAFE_NAME,    PRIVSEP_NETWORK_NAME, sizeof(struct msg_generic_result),        0},
        {"OP_MSG_DELETE_FILE",            PRIVSEP_NETWORK_NAME, PRIVSEP_SAFE_NAME,    sizeof(struct msg_delete_file_data),      0},
        {"OP_MSG_DELETE_FILE_RESULT",     PRIVSEP_SAFE_NAME,    PRIVSEP_NETWORK_NAME, sizeof(struct msg_generic_result),        0},
        {"OP_MSG_ERROR_RESULT",           PRIVSEP_SAFE_NAME,    PRIVSEP_NETWORK_NAME, 0,                                        sizeof(struct msg_error_result)},
        {"OP_MSG_MAX", "", "",                                                        0,                                        0}
};

int
privsep_master_process_msg(struct msg_queue *queue, uint64_t opcode, size_t len, char *data)
{
    struct privsep_process *process = NULL;
    struct privsep_process *target = NULL;
    struct privsep_opcode *op_info = NULL;
    struct msg *msg = NULL;
    int rc = 0;

    if (opcode >= OP_MSG_MAX) {
        log_error("Invalid opcode: %" PRIu64, opcode);
        return -1;
    }

    process = msg_queue_get_process(queue);
    op_info = opcode_info + (opcode - 1);


    log_debug("%s (%zu bytes) [%s]->[%s]", op_info->name, len, process->name, op_info->target);

    if (strncmp(op_info->source, process->name, PRIVSEP_CHILD_NAME_LEN) != 0) {
        log_error("Opcode received from bad child: op=%" PRIu64 " child=`%s' normal=`%s'",
                  opcode, process->name, op_info->source);
        return -1;
    }

    if (op_info->strict_packet_size && op_info->strict_packet_size != len) {
        log_error("Opcode:%" PRIu64 " received from:`%s' has invalid size: %zu",
                  opcode, process->name, len);
        return -1;
    } else if (op_info->min_packet_size && op_info->min_packet_size > len) {
        log_error("Opcode:%" PRIu64 " received from:`%s' is too small: %zu",
                  opcode, process->name, len);
        return -1;
    }

    target = privsep_get_child_by_name(op_info->target);
    if (target == NULL) {
        log_error("Could not found target process for opcode:%" PRIu64 " target=`%s'",
                  opcode, op_info->target);
        return -1;
    }

    msg = msg_init(opcode, len, data);
    if (msg == NULL) {
        return -1;
    }

    msg_queue_send(target->queue, msg);

    rc = bufferevent_enable(msg_queue_get_event_buffer(target->queue), EV_WRITE);
    if (rc == -1) {
        log_error("Failed to bufferevent_enable write on queue for %s", target->name);
        return -1;
    }

    return 0;
}