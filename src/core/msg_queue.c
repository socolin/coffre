#include "msg_queue.h"

#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/param.h>
#include <string.h>

#include "compat/linux.h"
#include "utils/log.h"
#include "utils/string.h"
#include "privsep.h"

struct msg_header {
    uint64_t opcode;
    size_t len;
};

struct msg {
    struct msg_header header;
    char *data;
};

struct msg_queue {
    struct bufferevent *bev;
    struct privsep_process *process;
    struct msg *current_receiving_msg;
    size_t current_receiving_msg_offset;
    struct msg *pending_msg[MAX_QUEUE_SIZE];
    size_t pending_pos;
    size_t pending_count;
    process_msg_callback process_msg_cb;
};

struct msg_queue *
msg_queue_init(struct privsep_process *process, struct event_base *event, process_msg_callback cb)
{
    struct msg_queue *queue = NULL;
    struct bufferevent *bev = NULL;
    int rc = 0;

    queue = calloc(sizeof(*queue), 1);
    if (queue == NULL) {
        log_error("Failed to calloc msg_queue: %m");
        goto failed_calloc;
    }

#ifdef OpenBSD
    bev = bufferevent_new(process->fd, NULL, NULL, NULL, NULL);
#else
    bev = bufferevent_socket_new(event, process->fd, 0);
#endif
    if (bev == NULL) {
        log_error("Failed to allocate bufferevent for `%s` queue", process->name);
        goto failed_alloc_bufferevent;
    }

    rc = bufferevent_base_set(event, bev);
    if (rc == -1) {
        log_error("Failed to bufferevent_base_set on `%s` queue", process->name);
        goto failed_setup_bufferevent;
    }

    bufferevent_setcb(bev, msg_queue_on_read_event, msg_queue_on_write_event, msg_queue_on_error_event, queue);
    bufferevent_setwatermark(bev, EV_READ | EV_WRITE, 0, 1 * 1024 * 1024);
    rc = bufferevent_enable(bev, EV_READ);
    if (rc == -1) {
        log_error("Failed to bufferevent_enable on `%s` queue", process->name);
        goto failed_setup_bufferevent;
    }

    queue->process = process;
    queue->bev = bev;
    queue->process_msg_cb = cb;

    return queue;
failed_setup_bufferevent:
    bufferevent_free(bev);
failed_alloc_bufferevent:
    freezero(queue, sizeof(*queue));
failed_calloc:
    return NULL;
}

void
msg_queue_destroy(struct msg_queue *queue)
{
    struct msg *msg = NULL;

    if (queue == NULL) {
        return;
    }

    for (size_t i = 0; i < queue->pending_count; i++) {
        msg = queue->pending_msg[(i + queue->pending_pos) % MAX_QUEUE_SIZE];
        freezero(msg->data, msg->header.len);
        msg_destroy(msg);
    }

    bufferevent_free(queue->bev);

    freezero(queue, sizeof(*queue));
}

void
msg_queue_on_read_event(struct bufferevent *bev, void *arg)
{
    struct msg_queue *queue = (struct msg_queue *) arg;
    struct msg *msg = NULL;
    ssize_t rc;

    do {
        if (queue->current_receiving_msg == NULL) {
            if (EVBUFFER_LENGTH(EVBUFFER_INPUT(bev)) >= sizeof(struct msg_header)) {
                msg = msg_init(0, 0, NULL);
                if (msg == NULL) {
                    bufferevent_disable(bev, EV_READ | EV_WRITE);
                    close(EVENT_FD(&bev->ev_read));
                    return;
                }

                rc = bufferevent_read(bev, &msg->header, sizeof(msg->header));
                if (rc == -1) {
                    log_error("Failed to read queue header: %m");
                    bufferevent_disable(bev, EV_READ | EV_WRITE);
                    close(EVENT_FD(&bev->ev_read));
                    return;
                }
                if (rc != sizeof(msg->header)) {
                    log_error("Invalid read size queue header: %m");
                    bufferevent_disable(bev, EV_READ | EV_WRITE);
                    close(EVENT_FD(&bev->ev_read));
                    return;
                }

                msg->data = malloc(msg->header.len);
                if (msg->data == NULL) {
                    log_error("Failed to alloc data for msg: %m");
                    bufferevent_disable(bev, EV_READ | EV_WRITE);
                    close(EVENT_FD(&bev->ev_read));
                    return;
                }

                queue->current_receiving_msg = msg;
                queue->current_receiving_msg_offset = 0;
            }
        }

        if (queue->current_receiving_msg != NULL && EVBUFFER_LENGTH(EVBUFFER_INPUT(bev)) > 0) {
            msg = queue->current_receiving_msg;
            size_t to_read = MIN(EVBUFFER_LENGTH(EVBUFFER_INPUT(bev)), msg->header.len - queue->current_receiving_msg_offset);
            rc = bufferevent_read(bev, msg->data + queue->current_receiving_msg_offset, to_read);
            if (rc == -1) {
                log_error("Failed bufferevent_read");
                goto error;
            }
            if (rc != to_read) {
                log_error("Invalid read queue data size: %m");
                goto error;
            }
            queue->current_receiving_msg_offset += rc;

            if (queue->current_receiving_msg_offset == msg->header.len) {
                rc = queue->process_msg_cb(queue, msg->header.opcode, msg->header.len, msg->data);
                if (rc == -1) {
                    goto error;
                }
                msg_destroy(msg);
                queue->current_receiving_msg = NULL;
                queue->current_receiving_msg_offset = 0;
            }
        }
    } while (queue->current_receiving_msg == NULL && EVBUFFER_LENGTH(EVBUFFER_INPUT(bev)) >= sizeof(struct msg_header));

    return;
error:
    event_base_loopbreak(bev->ev_base);
}

void
msg_queue_on_write_event(struct bufferevent *bev, void *arg)
{
    struct msg_queue *queue = (struct msg_queue *) arg;
    struct msg *msg = NULL;
    ssize_t rc;

    if (queue->pending_count == 0) {
        return;
    }

    do {
        msg = queue->pending_msg[queue->pending_pos];
        queue->pending_msg[queue->pending_pos] = NULL;
        queue->pending_count--;
        queue->pending_pos++;
        if (queue->pending_pos >= MAX_QUEUE_SIZE) {
            queue->pending_pos = 0;
        }

        rc = bufferevent_write(bev, &msg->header, sizeof(msg->header));
        if (rc == -1) {
            log_error("Failed to write queue header: %m");
            goto error;
        }

        rc = bufferevent_write(bev, msg->data, msg->header.len);
        if (rc == -1) {
            log_error("Failed to write queue data: %m");
            goto error;
        }

        msg_destroy(msg);
    } while (queue->pending_count && EVBUFFER_LENGTH(EVBUFFER_INPUT(bev)) < 1 * 1024 * 1024);

    return;
error:
    event_base_loopbreak(bev->ev_base);
}

void
msg_queue_on_error_event(struct bufferevent *bev, short what, void *arg)
{
    struct msg_queue *queue = (struct msg_queue *) arg;

    if ((what & EVBUFFER_EOF) != 0) {
        log_error("EOF on msg_queue from process `%s'", queue->process->name);
    } else {
        log_error("An error occure on msg_queue %s %d", queue->process->name, what);
    }

    event_base_loopbreak(bev->ev_base);
}

struct privsep_process *
msg_queue_get_process(struct msg_queue *queue)
{
    return queue->process;
}

struct bufferevent *
msg_queue_get_event_buffer(struct msg_queue *queue)
{
    return queue->bev;
}

int
msg_queue_send(struct msg_queue *queue, struct msg *msg)
{
    if (queue->pending_count >= MAX_QUEUE_SIZE) {
        log_error("Too many message in queue, cannot send more message");
        return -1;
    }

    queue->pending_msg[(queue->pending_pos + queue->pending_count) % MAX_QUEUE_SIZE] = msg;
    queue->pending_count++;

    bufferevent_enable(queue->bev, EV_WRITE);

    return 0;
}

struct msg *
msg_init(uint64_t opcode, size_t len, void *data)
{
    struct msg *msg = NULL;

    msg = calloc(1, sizeof(*msg));
    if (msg == NULL) {
        log_error("Failed to calloc msg: %m");
        return NULL;
    }

    msg->header.opcode = opcode;
    msg->header.len = len;
    msg->data = data;

    return msg;
}

int
msg_queue_send_sinit(struct msg_queue *queue, uint64_t opcode, struct string *str)
{
    int rc = 0;

    rc = msg_queue_send_init(queue, opcode, string_length(str), string_data(str));
    if (rc == -1) {
        string_destroy(str);
        return -1;
    }

    string_destroy_keepdata(str);

    return 0;
}

int
msg_queue_send_init(struct msg_queue *queue, uint64_t opcode, size_t len, void *data)
{
    struct msg *msg = NULL;
    int rc = 0;

    msg = msg_init(opcode, len, data);
    if (msg == NULL) {
        return -1;
    }

    rc = msg_queue_send(queue, msg);
    if (rc == -1) {
        msg_destroy(msg);
        return -1;
    }

    return 0;
}

void
msg_destroy(struct msg *msg)
{
    if (msg == NULL) {
        return;
    }

    freezero(msg, sizeof(*msg));
}
