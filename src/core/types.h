#ifndef COFFRE_TYPES_H
#define COFFRE_TYPES_H

#include <inttypes.h>

#define CLIENT_ID_BYTES_LEN 8
#define CLIENT_ID_LEN (CLIENT_ID_BYTES_LEN * 2 + 1)

typedef char clientid[CLIENT_ID_LEN];

#define FILE_UID_BYTES_LEN 16
#define FILE_UID_CHAR_LEN (FILE_UID_BYTES_LEN * 2)
#define FILE_UID_LEN (FILE_UID_BYTES_LEN * 2 + 1)

typedef char fileuid[FILE_UID_LEN];

typedef char safe_name[128];

#define SHARE_KEY_TOKEN_BYTES_LEN 32
#define SHARE_KEY_TOKEN_LEN (SHARE_KEY_TOKEN_BYTES_LEN * 2 + 1)

typedef char share_token[SHARE_KEY_TOKEN_LEN];

typedef int64_t operationid;

#define NO_OPERATION_ID 0x4242424242424242L

enum safe_mode {
    SAFE_MODE_INIT,
    SAFE_MODE_CONTROL,
    SAFE_MODE_UPLOAD,
    SAFE_MODE_DOWNLOAD
};

#endif //COFFRE_TYPES_H
