#include <stdlib.h>
#include <string.h>
#include <iso646.h>
#include <stdarg.h>
#include <utils/utils.h>

#include "msg_protocol.h"
#include "utils/log.h"
#include "utils/serialize.h"

struct msg_generic *msg_generic_init(const clientid client_id, operationid operation_id)
{
    struct msg_generic *data = NULL;

    data = calloc(1, sizeof(*data));
    if (data == NULL) {
        log_error("Failed to allocate data: %m");
        return NULL;
    }

    strlcpy(data->client_id, client_id, sizeof(data->client_id));
    data->operation_id = operation_id;

    return data;
}

struct msg_generic_result *
msg_generic_result_init(const clientid client_id, operationid operation_id, bool success)
{
    struct msg_generic_result *data = NULL;

    data = calloc(1, sizeof(*data));
    if (data == NULL) {
        log_error("Failed to allocate data: %m");
        return NULL;
    }

    strlcpy(data->client_id, client_id, sizeof(data->client_id));
    data->operation_id = operation_id;
    data->success = success;

    return data;
}

struct string *
msg_error_result_init(const clientid client_id, const operationid operation_id, const char *format, va_list ap)
{
    struct string *sdata = NULL;
    struct msg_error_result *data = NULL;
    struct string *error_message = NULL;
    int rc = 0;

    sdata = string_init();
    if (sdata == NULL) {
        return NULL;
    }

    rc = string_resize(sdata, sizeof(*data));
    if (rc == -1) {
        string_destroy(sdata);
        return NULL;
    }

    error_message = string_init();
    if (error_message == NULL) {
        string_destroy(sdata);
        log_error("Failed to allocate error string: %m");
        return NULL;
    }

    data = (struct msg_error_result *) string_data(sdata);
    strlcpy(data->client_id, client_id, sizeof(data->client_id));
    data->operation_id = operation_id;
    data->success = false;

    rc = string_vfappend(error_message, format, ap);
    if (rc == -1) {
        string_destroy(error_message);
        string_destroy(sdata);
        return NULL;
    }

    rc = serialize_string(sdata, error_message);
    if (rc == -1) {
        string_destroy(error_message);
        string_destroy(sdata);
        return NULL;
    }

    string_destroy(error_message);

    data = (struct msg_error_result *) string_data(sdata);
    data->data_size = string_length(sdata) - sizeof(*data);

    return sdata;
}

struct msg_auth_result_data *
msg_auth_result_data_init(const clientid client_id, operationid operation_id, bool success, enum safe_mode mode)
{
    struct msg_auth_result_data *data = NULL;

    data = calloc(1, sizeof(*data));
    if (data == NULL) {
        log_error("Failed to allocate data: %m");
        return NULL;
    }

    strlcpy(data->client_id, client_id, sizeof(data->client_id));
    data->operation_id = operation_id;
    data->success = success;
    data->mode = mode;

    return data;
}

struct string *
msg_generic_data_result_init(const clientid client_id, operationid operation_id, bool success)
{
    struct string *sdata = NULL;
    struct msg_generic_data_result *data = NULL;
    int rc = 0;

    sdata = string_init();
    if (sdata == NULL) {
        return NULL;
    }

    rc = string_resize(sdata, sizeof(*data));
    if (rc == -1) {
        string_destroy(sdata);
        return NULL;
    }

    data = (struct msg_generic_data_result *) string_data(sdata);
    strlcpy(data->client_id, client_id, sizeof(data->client_id));
    data->operation_id = operation_id;
    data->success = success;

    return sdata;
}

struct msg_auth_data *msg_auth_data_init(const clientid client_id, operationid operation_id, struct string *name, struct string *passphrase)
{
    struct msg_auth_data *data = NULL;

    if (string_length(name) >= sizeof(data->name)) {
        log_warning("name too long");
        return NULL;
    }

    if (string_length(passphrase) >= sizeof(data->passphrase)) {
        log_warning("passphrase too long");
        return NULL;
    }

    data = calloc(1, sizeof(*data));
    if (data == NULL) {
        log_error("Failed to allocate data: %m");
        return NULL;
    }

    strlcpy(data->client_id, client_id, sizeof(data->client_id));
    strlcpy(data->name, string_data(name), sizeof(data->name));
    strlcpy(data->passphrase, string_data(passphrase), sizeof(data->passphrase));
    data->operation_id = operation_id;

    return data;
}

struct msg_auth_token_data *msg_auth_token_data_init(const clientid client_id, operationid operation_id, struct string *token)
{
    struct msg_auth_token_data *data = NULL;

    if (string_length(token) >= sizeof(data->token)) {
        log_warning("token too long");
        return NULL;
    }

    data = calloc(1, sizeof(*data));
    if (data == NULL) {
        log_error("Failed to allocate data: %m");
        return NULL;
    }

    strlcpy(data->client_id, client_id, sizeof(data->client_id));
    strlcpy(data->token, string_data(token), sizeof(data->token));
    data->operation_id = operation_id;

    return data;
}

struct msg_create_safe_data *
msg_create_safe_data_init(const clientid client_id, operationid operation_id, struct string *name, struct string *passphrase)
{
    struct msg_create_safe_data *data = NULL;

    if (string_length(name) >= sizeof(data->name)) {
        log_warning("name too long");
        return NULL;
    }

    if (string_length(passphrase) >= sizeof(data->passphrase)) {
        log_warning("passphrase too long");
        return NULL;
    }

    data = calloc(1, sizeof(*data));
    if (data == NULL) {
        log_error("Failed to allocate data: %m");
        return NULL;
    }

    strlcpy(data->client_id, client_id, sizeof(data->client_id));
    strlcpy(data->name, string_data(name), sizeof(data->name));
    strlcpy(data->passphrase, string_data(passphrase), sizeof(data->passphrase));
    data->operation_id = operation_id;

    return data;
}

struct msg_list_directory_data *
msg_list_directory_data_init(const clientid client_id, operationid operation_id, struct string *uid)
{
    struct msg_list_directory_data *data = NULL;

    if (not string_eq_cstr(uid, "root") && not is_valid_file_uid(uid)) {
        log_warning("invalid file uid `%s'", string_data(uid));
        return NULL;
    }

    data = calloc(1, sizeof(*data));
    if (data == NULL) {
        log_error("Failed to allocate data: %m");
        return NULL;
    }

    strlcpy(data->client_id, client_id, sizeof(data->client_id));
    strlcpy(data->file_uid, string_data(uid), sizeof(data->file_uid));
    data->operation_id = operation_id;

    return data;
}

struct string *
msg_list_directory_result_data_init(const clientid client_id, operationid operation_id, const fileuid file_uid, bool success)
{
    struct string *sdata = NULL;
    struct msg_list_directory_result_data data;
    int rc = 0;

    sdata = string_init_reserve(sizeof(data));
    if (sdata == NULL) {
        return NULL;
    }

    strlcpy(data.client_id, client_id, sizeof(data.client_id));
    strlcpy(data.file_uid, file_uid, sizeof(data.file_uid));
    data.operation_id = operation_id;
    data.success = success;

    rc = string_bappend(sdata, &data, sizeof(data));
    if (rc == -1) {
        string_destroy(sdata);
        return NULL;
    }

    return sdata;
}


struct string *
msg_start_upload_data_init(const clientid client_id, operationid operation_id, struct string *uid, struct string *filename)
{
    struct string *sdata = NULL;
    struct msg_start_upload_data *data = NULL;
    int rc = 0;

    sdata = string_init();
    if (sdata == NULL) {
        return NULL;
    }

    rc = string_resize(sdata, sizeof(*data));
    if (rc == -1) {
        string_destroy(sdata);
        return NULL;
    }

    data = (struct msg_start_upload_data *) string_data(sdata);
    strlcpy(data->client_id, client_id, sizeof(data->client_id));
    strlcpy(data->directory_uid, string_data(uid), sizeof(data->directory_uid));

    rc = serialize_string(sdata, filename);
    if (rc == -1) {
        string_destroy(sdata);
        return NULL;
    }

    data = (struct msg_start_upload_data *) string_data(sdata);
    data->data_size = string_length(sdata) - sizeof(*data);
    data->operation_id = operation_id;

    return sdata;
}

struct string *
msg_upload_data_init(const clientid client_id, operationid operation_id, struct string *chunk_data)
{
    struct string *sdata = NULL;
    struct msg_upload_data *data = NULL;
    int rc = 0;

    sdata = string_init();
    if (sdata == NULL) {
        return NULL;
    }

    rc = string_resize(sdata, sizeof(*data));
    if (rc == -1) {
        string_destroy(sdata);
        return NULL;
    }

    data = (struct msg_upload_data *) string_data(sdata);
    strlcpy(data->client_id, client_id, sizeof(data->client_id));

    rc = string_bappend(sdata, string_data(chunk_data), string_length(chunk_data));
    if (rc == -1) {
        string_destroy(sdata);
        return NULL;
    }

    data = (struct msg_upload_data *) string_data(sdata);
    data->data_size = string_length(chunk_data);
    data->operation_id = operation_id;

    return sdata;
}

struct msg_share_key_data *
msg_share_key_data_init(const clientid client_id, operationid operation_id, uint8_t mode)
{
    struct msg_share_key_data *data = NULL;

    if (mode != SAFE_MODE_DOWNLOAD && mode != SAFE_MODE_UPLOAD) {
        log_warning("Invalid mode: %u", mode);
        return NULL;
    }

    data = calloc(1, sizeof(*data));
    if (data == NULL) {
        log_error("Failed to allocate data: %m");
        return NULL;
    }

    strlcpy(data->client_id, client_id, sizeof(data->client_id));
    data->operation_id = operation_id;
    data->mode = mode;

    return data;
}

struct msg_share_key_result_data *
msg_share_key_result_data_init(const clientid client_id, operationid operation_id, bool success, const char *token)
{
    struct msg_share_key_result_data *data = NULL;

    data = calloc(1, sizeof(*data));
    if (data == NULL) {
        log_error("Failed to allocate data: %m");
        return NULL;
    }

    strlcpy(data->client_id, client_id, sizeof(data->client_id));
    data->operation_id = operation_id;
    data->success = success;
    strlcpy(data->token, token, sizeof(data->token));

    return data;
}

struct msg_start_download_data *
msg_start_download_data_init(const clientid client_id, operationid operation_id, const struct string *file_uid)
{

    struct msg_start_download_data *data = NULL;

    if (not is_valid_file_uid(file_uid)) {
        log_warning("invalid file_uid `%s'", string_data(file_uid));
        return NULL;
    }

    data = calloc(1, sizeof(*data));
    if (data == NULL) {
        log_error("Failed to allocate data: %m");
        return NULL;
    }

    strlcpy(data->client_id, client_id, sizeof(data->client_id));
    strlcpy(data->file_uid, string_data(file_uid), sizeof(data->file_uid));
    data->operation_id = operation_id;

    return data;
}

struct msg_delete_file_data *
msg_delete_file_data_init(const clientid client_id, operationid operation_id, const struct string *directory_uid, const struct string *file_uid)
{
    struct msg_delete_file_data *data = NULL;

    if (not is_valid_file_uid(directory_uid)) {
        log_warning("invalid directory_uid `%s'", string_data(directory_uid));
        return NULL;
    }

    if (not is_valid_file_uid(file_uid)) {
        log_warning("invalid file_uid `%s'", string_data(file_uid));
        return NULL;
    }

    data = calloc(1, sizeof(*data));
    if (data == NULL) {
        log_error("Failed to allocate data: %m");
        return NULL;
    }

    strlcpy(data->client_id, client_id, sizeof(data->client_id));
    strlcpy(data->directory_uid, string_data(directory_uid), sizeof(data->directory_uid));
    strlcpy(data->file_uid, string_data(file_uid), sizeof(data->file_uid));
    data->operation_id = operation_id;

    return data;
}
