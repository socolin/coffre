#ifndef COFFRE_PRIVSEP_MASTER_H
#define COFFRE_PRIVSEP_MASTER_H

#include "msg_queue.h"

int privsep_master_process_msg(struct msg_queue *, uint64_t, size_t, char *);

#endif //COFFRE_PRIVSEP_MASTER_H
