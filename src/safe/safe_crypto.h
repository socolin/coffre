#ifndef COFFRE_SAFE_CRYPTO_H
#define COFFRE_SAFE_CRYPTO_H

#include <stdbool.h>
#include "core/types.h"

#include "safe_impl.h"
#include "utils/string.h"

bool safe_generate_key(struct safe *, const char *);
int safe_load_key(struct safe *, const char *);

int safe_encrypt_file(struct safe *, const char *, struct string *);
int safe_encrypt_prepare(struct safe *, const char *);
int safe_encrypt_write_data(struct safe *, char *, size_t);
int safe_encrypt_write_sdata(struct safe *, struct string *);
int safe_encrypt_cancel(struct safe *);
int safe_encrypt_complete(struct safe *);

struct string *safe_decrypt_file(struct safe *, const char *);
int safe_decrypt_prepare(struct safe *, const char *);
struct string *safe_decrypt_read_data(struct safe *, size_t);
void safe_decrypt_complete(struct safe *);

#endif //COFFRE_SAFE_CRYPTO_H
