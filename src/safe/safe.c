#include "safe.h"

#include <inttypes.h>
#include <iso646.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdbool.h>
#include <openssl/evp.h>
#include <string.h>
#include <iso646.h>
#include <event.h>

#include "core/conf.h"
#include "core/msg_protocol.h"
#include "core/msg_queue.h"
#include "compat/linux.h"
#include "utils/log.h"
#include "utils/serialize.h"
#include "safe_impl.h"

#define __return_client_error(queue, clientid, operation_id, format, ...)                   \
    rc = safe_send_error(queue, clientid, operation_id, format "%s", __VA_ARGS__);          \
    if (rc == -1) {                                                                         \
        log_cerror("Failed to safe_send_error");                                            \
        return -1;                                                                          \
    }                                                                                       \
    return 0                                                                                \


#define return_client_error(queue, clientid, operation_id, ...)                             \
    __return_client_error(queue, clientid, operation_id, __VA_ARGS__, "")


static bool
safe_is_name_valid(const char *name)
{
    while (*name) {
        if ((*name >= 'a' && *name <= 'z')
            || (*name >= '0' && *name <= '9')
            || (*name >= 'A' && *name <= 'Z')
            || *name == '-' || *name == '_' || *name == '@') {
            name++;
            continue;
        }

        return false;
    }

    return true;
}

static int
safe_send_error(struct msg_queue *queue, const clientid client_id, const operationid operation_id, const char *format, ...) {
    va_list ap;
    int rc = 0;

    va_start(ap, format);
    struct string *error_msg = msg_error_result_init(client_id, operation_id, format, ap);
    va_end(ap);
    if (error_msg == NULL) {
        log_error("Failed to allocate error message");
        return -1;
    }

    rc = msg_queue_send_sinit(queue, OP_MSG_ERROR_RESULT, error_msg);
    if (rc == -1) {
        log_error("Failed to send error response in queue");
        return -1;
    }

    return 0;
}

static int
safe_process_auth_msg(struct msg_queue *queue, struct msg_auth_data *auth_data, struct dictionary *open_safes)
{
    const char *clientid = auth_data->client_id;
    struct msg_auth_result_data *response_data = NULL;
    struct safe *safe = NULL;
    int rc = 0;

    log_cinfo("Auth on safe `%s'", auth_data->name);

    if (not safe_is_name_valid(auth_data->name)) {
        return_client_error(queue, clientid, auth_data->operation_id, "Invalid safe name `%s' should only contains a-z, A-Z, 0-9, -, _ or @", auth_data->name);
    }

    safe = safe_load(auth_data->name, auth_data->passphrase);
    if (safe == NULL) {
        return_client_error(queue, clientid, auth_data->operation_id, "An error occurred while loading safe, invalid passphrase ?");
    }

    rc = dictionary_put(open_safes, clientid, safe);
    if (rc == -1) {
        safe_destroy(safe);
        log_cerror("Failed to add safe to opened safes dictionary");
        return -1;
    }

    log_cinfo("Auth ok for safe `%s'", auth_data->name);

    response_data = msg_auth_result_data_init(clientid, auth_data->operation_id, true, safe->mode);
    if (response_data == NULL) {
        log_cerror("Failed to msg_auth_result_data_init");
        return -1;
    }

    rc = msg_queue_send_init(queue, OP_MSG_AUTH_RESULT, sizeof(*response_data), response_data);
    if (rc == -1) {
        log_cerror("Failed to msg_queue_send_init");
        freezero(response_data, sizeof(*response_data));
        return -1;
    }

    return 0;
}

static int
safe_process_auth_token_msg(struct msg_queue *queue,
                            struct msg_auth_token_data *auth_data,
                            struct safe_process_data *safe_process_data)
{
    const char *clientid = auth_data->client_id;
    struct msg_auth_result_data *response_data = NULL;
    struct safe *safe = NULL;
    int rc = 0;

    log_cinfo("Auth on safe `%s'", auth_data->token);

    safe = safe_load_with_share_token(safe_process_data->safes_by_token, auth_data->token);
    if (safe == NULL) {
        return_client_error(queue, clientid, auth_data->operation_id, "An error occurred while loading safe, invalid token ?");
    }

    rc = dictionary_put(safe_process_data->open_safes, clientid, safe);
    if (rc == -1) {
        safe_destroy(safe);
        log_cerror("Failed to add safe to opened safes dictionary");
        return -1;
    }

    log_cinfo("Auth ok for safe `%s'", safe->name);

    response_data = msg_auth_result_data_init(clientid, auth_data->operation_id, true, safe->mode);
    if (response_data == NULL) {
        safe_destroy(safe);
        return -1;
    }

    rc = msg_queue_send_init(queue, OP_MSG_AUTH_RESULT, sizeof(*response_data), response_data);
    if (rc == -1) {
        safe_destroy(safe);
        freezero(response_data, sizeof(*response_data));
        return -1;
    }

    return 0;
}

static int
safe_process_deauth_msg(struct safe *safe, struct msg_generic *deauth_data, struct safe_process_data *safe_process_data)
{
    const char *clientid = deauth_data->client_id;

    log_cdebug("safe_process_deauth_msg");

    if (safe == NULL) {
        return 0;
    }

    dictionary_remove(safe_process_data->open_safes, clientid);
    safe_token_remove_all(safe, safe_process_data->safes_by_token);
    safe_destroy(safe);

    return 0;
}

static int
safe_process_create_safe_msg(struct msg_queue *queue,
                             struct msg_create_safe_data *create_safe_data,
                             struct dictionary *open_safes)
{
    const char *clientid = create_safe_data->client_id;
    struct msg_auth_result_data *response_data = NULL;
    struct safe *safe = NULL;
    int rc = 0;

    log_cinfo("Start creating new safe: `%s'", create_safe_data->name);

    if (not safe_is_name_valid(create_safe_data->name)) {
        return_client_error(queue, clientid, create_safe_data->operation_id, "Invalid safe name `%s' should only contains a-z, A-Z, 0-9, -, _ or @", create_safe_data->name);
    }

    if (safe_exists(create_safe_data->name)) {
        return_client_error(queue, clientid, create_safe_data->operation_id, "Safe name already used");
    }

    safe = safe_create_new(create_safe_data->name, create_safe_data->passphrase);
    if (safe == NULL) {
        return_client_error(queue, clientid, create_safe_data->operation_id, "An error occurred while creating safe");
    }

    rc = dictionary_put(open_safes, clientid, safe);
    if (rc == -1) {
        safe_destroy(safe);
        log_cerror("Failed to add created safe to opened safes dictionary");
        return -1;
    }

    log_cnotice("New safe created");

    response_data = msg_auth_result_data_init(create_safe_data->client_id, create_safe_data->operation_id, true, safe->mode);
    if (response_data == NULL) {
        return -1;
    }

    rc = msg_queue_send_init(queue, OP_MSG_CREATE_SAFE_RESULT, sizeof(*response_data), response_data);
    if (rc == -1) {
        freezero(response_data, sizeof(*response_data));
        return -1;
    }

    return 0;
}

static int
safe_list_directory_append_string(struct safe *safe, struct string *data, struct bson *file_metadata, const char *key)
{
    int rc = 0;
    struct string *str = NULL;

    if (not bson_get_string(file_metadata, key, &str)) {
        log_serror("Missing `%s` in a file data", key);
        return -1;
    }

    rc = serialize_string(data, str);
    if (rc == -1) {
        log_serror("Failed to append string to msg when listing directory");
        return -1;
    }

    return 0;
}

static int
safe_list_directory_append_timestamp(struct safe *safe, struct string *data, struct bson *file_metadata,
                                     const char *key)
{
    int rc = 0;
    time_t value = 0;

    if (not bson_get_timestamp(file_metadata, key, &value)) {
        log_serror("Missing `%s` in a file data", key);
        return -1;
    }

    rc = serialize_time(data, value);
    if (rc == -1) {
        log_serror("Failed to append data to msg when listing directory");
        return -1;
    }

    return 0;
}

static int
safe_list_directory_append_int32(struct safe *safe, struct string *data, struct bson *file_metadata, const char *key)
{
    int rc = 0;
    int32_t value = 0;

    if (not bson_get_int32(file_metadata, key, &value)) {
        log_serror("Missing `%s` in a file data", key);
        return -1;
    }

    rc = serialize_int32(data, value);
    if (rc == -1) {
        log_serror("Failed to append data to msg when listing directory");
        return -1;
    }

    return 0;
}

static int
safe_list_directory_append_int64(struct safe *safe, struct string *data, struct bson *file_metadata, const char *key)
{
    int rc = 0;
    int64_t value = 0;

    if (not bson_get_int64(file_metadata, key, &value)) {
        log_serror("Missing `%s` in a file data", key);
        return -1;
    }

    rc = serialize_int64(data, value);
    if (rc == -1) {
        log_serror("Failed to append data to msg when listing directory");
        return -1;
    }

    return 0;
}

static int
safe_list_directory(struct safe *safe, fileuid directory_uid, struct string *data)
{
    struct bson *file_metadata = NULL;
    struct directory *directory = NULL;
    int rc = 0;
    int32_t type = 0;

    directory = safe_load_directory(safe, directory_uid);
    if (directory == NULL) {
        log_swarning("Failed to load directory `%s'", directory_uid);
        goto error;
    }

    rc = safe_list_directory_append_string(safe, data, directory->data, "name");
    if (rc == -1) {
        log_swarning("Failed to append directory name to list directory data `%s'", directory_uid);
        goto error;
    }

    if (bson_contains_key(directory->data, "next")) {
        rc = serialize_int8(data, 1);
        if (rc == -1) {
            log_swarning("Failed to serialize `hasNext` directory: `%s'", directory_uid);
            goto error;
        }

        rc = safe_list_directory_append_string(safe, data, directory->data, "next");
        if (rc == -1) {
            log_swarning("Failed to append directory next uid to list directory data `%s'",
                         directory_uid);
            goto error;
        }
    } else {
        rc = serialize_int8(data, 0);
        if (rc == -1) {
            log_swarning("Failed to serialize `hasNext` directory: `%s'", directory_uid);
            goto error;
        }
    }

    while ((file_metadata = directory_iterate_files(directory)) != NULL) {
        rc = safe_list_directory_append_string(safe, data, file_metadata, "uid");
        if (rc == -1) {
            log_serror("Failed to append file uid to list directory data `%s'", directory_uid);
            goto error;
        }

        rc = safe_list_directory_append_string(safe, data, file_metadata, "name");
        if (rc == -1) {
            log_serror("Failed to append file name to list directory data `%s'", directory_uid);
            goto error;
        }

        rc = safe_list_directory_append_timestamp(safe, data, file_metadata, "date");
        if (rc == -1) {
            log_serror("Missing `date` in a file of directory `%s'", directory_uid);
            goto error;
        }

        rc = safe_list_directory_append_int32(safe, data, file_metadata, "type");
        if (rc == -1) {
            log_serror("Missing `type` in a file of directory `%s'", directory_uid);
            goto error;
        }
        bson_get_int32(file_metadata, "type", &type);

        if (type == FILE_TYPE_FILE) {
            rc = safe_list_directory_append_int64(safe, data, file_metadata, "length");
            if (rc == -1) {
                log_serror("Missing `length` in a file of directory `%s'", directory_uid);
                goto error;
            }
        }
    }

    directory_destroy(directory);
    return 0;
error:
    directory_destroy(directory);
    return -1;
}

static int
safe_process_list_directory_msg(struct safe *safe,
                                struct msg_queue *queue,
                                struct msg_list_directory_data *list_directory_data)
{
    const char *clientid = list_directory_data->client_id;
    struct string *response_data = NULL;
    int rc = 0;
    char file_uid[FILE_UID_LEN];
    struct string *root_uid = NULL;
    struct msg_list_directory_result_data *data = NULL;

    response_data = msg_list_directory_result_data_init(list_directory_data->client_id,
                                                        list_directory_data->operation_id,
                                                        list_directory_data->file_uid,
                                                        true);
    if (response_data == NULL) {
        log_cerror("Failed to msg_list_directory_result_data_init");
        return -1;
    }

    if (strcmp(list_directory_data->file_uid, "root") == 0) {
        if (not bson_get_string(safe->conf, "root", &root_uid)) {
            string_destroy(response_data);
            return_client_error(queue, clientid, list_directory_data->operation_id, "Corrupted config, no root directory defined");
        }

        strlcpy(file_uid, string_data(root_uid), sizeof(file_uid));
        data = (struct msg_list_directory_result_data *) string_data(response_data);
        strlcpy(data->file_uid, file_uid, sizeof(data->file_uid));
    } else {
        strlcpy(file_uid, list_directory_data->file_uid, sizeof(file_uid));
    }

    rc = safe_list_directory(safe, file_uid, response_data);
    if (rc == -1) {
        string_destroy(response_data);
        return_client_error(queue, clientid, list_directory_data->operation_id, "Failed to list requested directory");
    }

    data = (struct msg_list_directory_result_data *) string_data(response_data);
    data->data_size = string_length(response_data) - sizeof(*data);

    rc = msg_queue_send_sinit(queue, OP_MSG_LIST_DIRECTORY_RESULT, response_data);
    if (rc == -1) {
        return -1;
    }

    return 0;
}

static int
safe_process_start_upload_msg(struct safe *safe,
                              struct msg_queue *queue,
                              struct msg_start_upload_data *start_upload_data)
{
    const char *clientid = start_upload_data->client_id;
    struct string *response_data = NULL;
    struct msg_generic_data_result *data = NULL;
    int rc = 0;
    struct string *filename = NULL;
    fileuid file_uid;
    size_t offset = 0;

    response_data = msg_generic_data_result_init(start_upload_data->client_id, start_upload_data->operation_id, true);
    if (response_data == NULL) {
        log_cerror("Failed to msg_generic_data_result_init");
        return -1;
    }

    rc = deserialize_string(start_upload_data->data, start_upload_data->data_size, &offset, &filename);
    if (rc == -1) {
        string_destroy(response_data);
        log_cerror("Failed to deserialize_string");
        return -1;
    }

    bzero(file_uid, sizeof(file_uid));

    if (string_length(filename) >= MAX_DISPLAY_FILENAME_SIZE) {
        string_destroy(response_data);
        return_client_error(queue, clientid, start_upload_data->operation_id, "Filename too long");
    }

    rc = safe_prepare_save_data_file(safe, start_upload_data->directory_uid, string_data(filename), file_uid);
    if (rc == -1) {
        string_destroy(response_data);
        return_client_error(queue, clientid, start_upload_data->operation_id, "An error occurred while preparing to save file");
    }

    rc = serialize_cstring(response_data, file_uid);
    if (rc == -1) {
        safe_cancel_save_data(safe);
        string_destroy(response_data);
        log_cerror("Failed to serialize file_uid");
        return -1;
    }

    data = (struct msg_generic_data_result *) string_data(response_data);
    data->data_size = string_length(response_data) - sizeof(*data);

    string_destroy(filename);

    rc = msg_queue_send_sinit(queue, OP_MSG_START_UPLOAD_RESULT, response_data);
    if (rc == -1) {
        log_cerror("Failed to msg_queue_send_sinit");
        return -1;
    }

    return 0;
}

static int
safe_process_upload_msg(struct safe *safe, struct msg_queue *queue, struct msg_upload_data* upload_data)
{
    const char *clientid = upload_data->client_id;
    struct msg_generic_result *response_data = NULL;
    int rc = 0;

    rc = safe_continue_save_data(safe, upload_data->data, upload_data->data_size);
    if (rc == -1) {
        return_client_error(queue, clientid, upload_data->operation_id, "An error occurred while saving data");
    }

    response_data = msg_generic_result_init(clientid, upload_data->operation_id, true);
    if (response_data == NULL) {
        log_cerror("Failed to msg_generic_result_init");
        return -1;
    }

    rc = msg_queue_send_init(queue, OP_MSG_UPLOAD_RESULT, sizeof(*response_data), response_data);
    if (rc == -1) {
        log_cerror("Failed to msg_queue_send_init");
        freezero(response_data, sizeof(*response_data));
        return -1;
    }

    return 0;
}

static int
safe_process_upload_complete_msg(struct safe *safe, struct msg_queue *queue, struct msg_generic* generic_data)
{
    const char *clientid = generic_data->client_id;
    struct string *response_data = NULL;
    struct msg_generic_data_result *data = NULL;
    fileuid directory_uid;
    int rc = 0;

    bzero(directory_uid, sizeof(directory_uid));

    rc = safe_finish_save_data(safe, directory_uid);
    if (rc == -1) {
        return_client_error(queue, clientid, generic_data->operation_id, "An error occurred while finishing saving data");
    }

    response_data = msg_generic_data_result_init(clientid, generic_data->operation_id, true);
    if (response_data == NULL) {
        log_cerror("Failed to msg_generic_data_result_init");
        return -1;
    }

    rc = serialize_cstring(response_data, directory_uid);
    if (rc == -1) {
        string_destroy(response_data);
        log_cerror("Failed to serialize_cstring");
        return -1;
    }

    data = (struct msg_generic_data_result *) string_data(response_data);
    data->data_size = string_length(response_data) - sizeof(*data);

    rc = msg_queue_send_sinit(queue, OP_MSG_UPLOAD_COMPLETE_RESULT, response_data);
    if (rc == -1) {
        return -1;
    }

    return 0;
}

static int
safe_process_cancel_upload_msg(struct safe *safe, struct msg_queue *queue, struct msg_generic* generic_data)
{
    const char *clientid = generic_data->client_id;
    struct msg_generic_result *response_data = NULL;
    int rc = 0;

    rc = safe_cancel_save_data(safe);
    if (rc == -1) {
        return_client_error(queue, clientid, generic_data->operation_id, "An error occurred while canceling upload");
    }

    response_data = msg_generic_result_init(clientid, generic_data->operation_id, true);
    if (response_data == NULL) {
        log_cerror("Failed to msg_generic_result_init");
        return -1;
    }

    rc = msg_queue_send_init(queue, OP_MSG_UPLOAD_CANCEL_RESULT, sizeof(*response_data), response_data);
    if (rc == -1) {
        log_cerror("Failed to msg_queue_send_init");
        freezero(response_data, sizeof(*response_data));
        return -1;
    }

    return 0;
}

static int
safe_process_share_key_msg(struct safe *safe,
                           struct msg_queue *queue,
                           struct msg_share_key_data *share_key_data,
                           struct dictionary *safes_by_token)
{
    const char *clientid = share_key_data->client_id;
    share_token token;
    struct msg_share_key_result_data *response_data = NULL;
    int rc = 0;

    bzero(token, sizeof(token));

    rc = safe_share_key(safe, (enum safe_mode) share_key_data->mode, token, safes_by_token, time(NULL)) + 60;
    if (rc == -1) {
        return_client_error(queue, clientid, share_key_data->operation_id, "Failed to share key");
    }

    response_data = msg_share_key_result_data_init(clientid, share_key_data->operation_id, true, token);
    if (response_data == NULL) {
        log_cerror("Failed to msg_share_key_result_data_init");
        return -1;
    }

    rc = msg_queue_send_init(queue, OP_MSG_SHARE_KEY_RESULT, sizeof(*response_data), response_data);
    if (rc == -1) {
        log_cerror("Failed to msg_queue_send_init");
        freezero(response_data, sizeof(*response_data));
        return -1;
    }

    return 0;
}

static int
safe_process_start_download_msg(struct safe *safe, struct msg_queue *queue, struct msg_start_download_data* start_download_data)
{
    const char *clientid = start_download_data->client_id;
    struct msg_generic_result *response_data = NULL;
    int rc = 0;

    rc = safe_prepare_read_file(safe, start_download_data->file_uid);
    if (rc == -1) {
        return_client_error(queue, clientid, start_download_data->operation_id, "Failed to prepare download");
    }

    response_data = msg_generic_result_init(clientid, start_download_data->operation_id, true);
    if (response_data == NULL) {
        log_cerror("Failed to msg_generic_result_init");
        return -1;
    }

    rc = msg_queue_send_init(queue, OP_MSG_START_DOWNLOAD_RESULT, sizeof(*response_data), response_data);
    if (rc == -1) {
        log_cerror("Failed to msg_queue_send_init");
        freezero(response_data, sizeof(*response_data));
        return -1;
    }

    return 0;
}

static int
safe_process_download_msg(struct safe *safe, struct msg_queue *queue, struct msg_generic* msg)
{
    const char *clientid = msg->client_id;
    struct string *response_data = NULL;
    struct string *file_data = NULL;
    struct msg_generic_data_result *data = NULL;
    int rc = 0;

    rc = safe_continue_read_file(safe, &file_data, 512 * 1024);
    if (rc == -1) {
        return_client_error(queue, clientid, msg->operation_id, "An error occurred during download");
    }

    response_data = msg_generic_data_result_init(clientid, msg->operation_id, true);
    if (response_data == NULL) {
        string_destroy(file_data);
        log_cerror("Failed to msg_generic_data_result_init");
        return -1;
    }

    // TODO: Optimization: There is a full copy of the data, that could be avoided by refactoring
    // TODO: safe_continue_read_file to use the same string for the data and the msg, for now keep it simple
    rc = serialize_string(response_data, file_data);
    string_destroy(file_data);
    if (rc == -1) {
        string_destroy(response_data);
        log_cerror("Failed to serialize_string");
        return -1;
    }

    data = (struct msg_generic_data_result *) string_data(response_data);
    data->data_size = string_length(response_data) - sizeof(*data);

    rc = msg_queue_send_sinit(queue, OP_MSG_DOWNLOAD_RESULT, response_data);
    if (rc == -1) {
        log_cerror("Failed to msg_queue_send_sinit");
        return -1;
    }

    return 0;
}

static int
safe_process_download_cancel_msg(struct safe *safe, struct msg_queue *queue, struct msg_generic* msg)
{
    const char *clientid = msg->client_id;
    struct msg_generic_result *response_data = NULL;
    int rc = 0;

    safe_cancel_read_file(safe);

    response_data = msg_generic_result_init(clientid, msg->operation_id, true);
    if (response_data == NULL) {
        log_cerror("Failed to msg_generic_result_init");
        return -1;
    }

    rc = msg_queue_send_init(queue, OP_MSG_DOWNLOAD_CANCEL_RESULT, sizeof(*response_data), response_data);
    if (rc == -1) {
        log_cerror("Failed to msg_queue_send_init");
        freezero(response_data, sizeof(*response_data));
        return -1;
    }

    return 0;
}
static int
safe_process_delete_file_msg(struct safe *safe, struct msg_queue *queue, struct msg_delete_file_data* msg)
{
    const char *clientid = msg->client_id;
    struct msg_generic_result *response_data = NULL;
    int rc = 0;

    rc = safe_delete_file(safe, msg->directory_uid, msg->file_uid);
    if (rc == -1) {
        return_client_error(queue, clientid, msg->operation_id, "An error occurred while deleting file");
    }

    response_data = msg_generic_result_init(clientid, msg->operation_id, true);
    if (response_data == NULL) {
        log_cerror("Failed to msg_generic_result_init");
        return -1;
    }

    rc = msg_queue_send_init(queue, OP_MSG_DELETE_FILE_RESULT, sizeof(*response_data), response_data);
    if (rc == -1) {
        log_cerror("Failed to msg_queue_send_init");
        freezero(response_data, sizeof(*response_data));
        return -1;
    }

    return 0;
}

static bool
safe_opcode_require_open_safe(uint64_t opcode) {
    switch (opcode) {
        case OP_MSG_LIST_DIRECTORY:
        case OP_MSG_START_UPLOAD:
        case OP_MSG_UPLOAD:
        case OP_MSG_UPLOAD_COMPLETE:
        case OP_MSG_UPLOAD_CANCEL:
        case OP_MSG_SHARE_KEY:
        case OP_MSG_START_DOWNLOAD:
        case OP_MSG_DOWNLOAD:
        case OP_MSG_DOWNLOAD_CANCEL:
        case OP_MSG_DELETE_FILE:
            return true;
        default:
            return false;
    }
}

static bool
safe_opcode_require_no_open_safe(uint64_t opcode) {
    switch (opcode) {
        case OP_MSG_AUTH:
        case OP_MSG_AUTH_TOKEN:
        case OP_MSG_CREATE_SAFE:
            return true;
        default:
            return false;
    }
}

static int
safe_process_msg(struct msg_queue *queue, uint64_t opcode, size_t len, char *data)
{
    struct privsep_process *process = NULL;
    struct safe *safe = NULL;
    struct safe_process_data *safe_process_data = NULL;
    const struct msg_generic *msg = (const struct msg_generic *) data;
    const char *clientid = msg->client_id;
    int rc = 0;

    log_cdebug("Opcode: %" PRIu64, opcode);

    process = msg_queue_get_process(queue);
    safe_process_data = process->udata;

    safe = dictionary_get(safe_process_data->open_safes, clientid);
    if (safe == NULL && safe_opcode_require_open_safe(opcode)) {
        rc = safe_send_error(queue, clientid, msg->operation_id, "You need to be auth before doing this action");
    }
    else if (safe != NULL && safe_opcode_require_no_open_safe(opcode)) {
        rc = safe_send_error(queue, clientid, msg->operation_id, "You are already auth");
    }
    else {
        switch (opcode) {
            case OP_MSG_AUTH:
                rc = safe_process_auth_msg(queue, (struct msg_auth_data *) data, safe_process_data->open_safes);
                break;
            case OP_MSG_AUTH_TOKEN:
                rc = safe_process_auth_token_msg(queue, (struct msg_auth_token_data*) data, safe_process_data);
                break;
            case OP_MSG_DEAUTH:
                rc = safe_process_deauth_msg(safe, (struct msg_generic *) data, safe_process_data);
                break;
            case OP_MSG_CREATE_SAFE:
                rc = safe_process_create_safe_msg(queue, (struct msg_create_safe_data *) data, safe_process_data->open_safes);
                break;
            case OP_MSG_LIST_DIRECTORY:
                rc = safe_process_list_directory_msg(safe, queue, (struct msg_list_directory_data *) data);
                break;
            case OP_MSG_START_UPLOAD:
                rc = safe_process_start_upload_msg(safe, queue, (struct msg_start_upload_data *) data);
                break;
            case OP_MSG_UPLOAD:
                rc = safe_process_upload_msg(safe, queue, (struct msg_upload_data *) data);
                break;
            case OP_MSG_UPLOAD_COMPLETE:
                rc = safe_process_upload_complete_msg(safe, queue, (struct msg_generic *) data);
                break;
            case OP_MSG_UPLOAD_CANCEL:
                rc = safe_process_cancel_upload_msg(safe, queue, (struct msg_generic *) data);
                break;
            case OP_MSG_SHARE_KEY:
                rc = safe_process_share_key_msg(safe, queue, (struct msg_share_key_data *) data, safe_process_data->safes_by_token);
                break;
            case OP_MSG_START_DOWNLOAD:
                rc = safe_process_start_download_msg(safe, queue, (struct msg_start_download_data *) data);
                break;
            case OP_MSG_DOWNLOAD:
                rc = safe_process_download_msg(safe, queue, (struct msg_generic*) data);
                break;
            case OP_MSG_DOWNLOAD_CANCEL:
                rc = safe_process_download_cancel_msg(safe, queue, (struct msg_generic*) data);
                break;
            case OP_MSG_DELETE_FILE:
                rc = safe_process_delete_file_msg(safe, queue, (struct msg_delete_file_data*) data);
                break;
            default:
                log_cerror("Unknown opcode %" PRIu64, opcode);
                rc = -1;
                break;
        }
    }

    log_cdebug("done %" PRIu64, opcode);

    freezero(data, len);
    return rc;
}

struct safe_process_data *
safe_process_data_init()
{
    struct safe_process_data *data = NULL;

    data = calloc(1, sizeof(*data));
    if (data == NULL) {
        log_critical("Failed to calloc struct safe_process_data: %m");
        return NULL;
    }

    data->open_safes = dictionary_init();
    if (data->open_safes == NULL) {
        free(data);
        return NULL;
    }

    data->safes_by_token= dictionary_init();
    if (data->safes_by_token == NULL) {
        free(data);
        return NULL;
    }

    return data;
}

void
safe_process_data_destroy(struct safe_process_data *data)
{
    if (data == NULL) {
        return;
    }

    // FIXME: iterate over all open safes and destroy them

    dictionary_destroy(data->open_safes);
    dictionary_destroy(data->safes_by_token);
    freezero(data, sizeof(*data));
}

int
start_safe(struct privsep_process *parent)
{
    struct event_base *event = NULL;
    struct safe_process_data *safe_process_data = NULL;
    struct bufferevent *bev_queue_master = NULL;
    struct stat sb;
    int rc = 0;

    // 1)

    event = event_base_new();
    if (event == NULL) {
        log_error("Failed to allocate event_base");
        return -1;
    }

    // 2)

    parent->queue = msg_queue_init(parent, event, safe_process_msg);
    if (parent->queue == NULL) {
        log_error("Failed to init `master` queue");
        return -1;
    }

    safe_process_data = safe_process_data_init();
    if (safe_process_data == NULL) {
        return -1;
    }

    parent->udata = safe_process_data;

    // 3)

    rc = chroot(g_config.root);
    if (rc == -1) {
        log_error("chroot to `%s' failed: %m", g_config.root);
        return -1;
    }

    log_info("chrooted to `%s'", g_config.root);

    rc = chdir("/");
    if (rc == -1) {
        log_error("chdir to / failed: %m");
        return -1;
    }

    rc = setgid(g_config.groupid);
    if (rc == -1) {
        log_error("Failed to setgid to %d(%s): %m", g_config.groupid, g_config.groupname);
        return -1;
    }

    rc = setuid(g_config.userid);
    if (rc == -1) {
        log_error("Failed to setuid to %d(%s): %m", g_config.userid, g_config.username);
        return -1;
    }

    // 4)

    rc = pledge("stdio rpath wpath cpath", NULL);
    if (rc == -1) {
        log_error("pledge failed: %m");
        return -1;
    }

    rc = stat(SAFES_DIRECTORY, &sb);
    if (rc == -1) {
        if (errno != ENOENT) {
            log_error("Failed to stat `%s': %m", SAFES_DIRECTORY);
            return -1;
        }
        rc = mkdir(SAFES_DIRECTORY, 0700);
        if (rc == -1) {
            log_error("Failed to mkdir `%s': %m", SAFES_DIRECTORY);
            return -1;
        }
    } else {
        if (!S_ISDIR(sb.st_mode)) {
            log_error("Safes directory should be a directory: `%s'", SAFES_DIRECTORY);
            return -1;
        }

        if (sb.st_uid != g_config.userid) {
            log_error("Bad owner for safe directory: `%s' current=%d wanted=%d", SAFES_DIRECTORY, sb.st_uid,
                      g_config.userid);
            return -1;
        }

        if ((sb.st_mode & 0777u) != 0700u) {
            log_error("Bad permission for safe directory: `%s' should be 700", SAFES_DIRECTORY);
            return -1;
        }
    }

    // 5)

    OpenSSL_add_all_algorithms();

    // 6)

    event_base_dispatch(event);

    // 7)

    safe_process_data_destroy(safe_process_data);
    msg_queue_destroy(parent->queue);
    bufferevent_free(bev_queue_master);
    event_base_free(event);

    return 0;
}
