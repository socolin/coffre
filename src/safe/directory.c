#include "directory.h"

#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdio.h>
#include <iso646.h>

#include "utils/log.h"
#include "utils/string.h"
#include "compat/linux.h"

struct directory *
directory_init(const fileuid file_uid, struct bson *bson)
{
    struct directory *directory = NULL;

    directory = calloc(1, sizeof(*directory));
    if (directory == NULL) {
        log_error("Failed to calloc directory struct: %m");
        return NULL;
    }

    strlcpy(directory->file_uid, file_uid, sizeof(directory->file_uid));
    directory->data = bson;

    return directory;
}

void
directory_destroy(struct directory *directory)
{
    if (directory == NULL) {
        return;
    }

    bson_destroy(directory->data);
    freezero(directory, sizeof(*directory));
}

struct bson *
directory_init_file(const fileuid file_uid, const char *filename, enum file_type type)
{
    struct bson *bson = NULL;
    int rc = 0;

    bson = bson_init(BSON_TYPE_DOCUMENT);
    if (bson == NULL) {
        log_error("Failed to create file data, cannot allocate bson");
        goto failed_init_bson;
    }

    rc = bson_set_cstring(bson, "uid", file_uid);
    if (rc == -1) {
        log_error("Failed to set bson cstring `uid`");
        goto failed_set_uid;
    }

    rc = bson_set_cstring(bson, "name", filename);
    if (rc == -1) {
        log_error("Failed to set bson cstring `name`");
        goto failed_set_name;
    }

    rc = bson_set_timestamp(bson, "date", time(NULL));
    if (rc == -1) {
        log_error("Failed to set bson timestamp `date`");
        goto failed_set_date;
    }

    rc = bson_set_int32(bson, "type", type);
    if (rc == -1) {
        log_error("Failed to set bson int `type`");
        goto failed_set_type;
    }

    return bson;
failed_set_type:
failed_set_date:
failed_set_uid:
failed_set_name:
    bson_destroy(bson);
failed_init_bson:
    return NULL;
}

struct directory *
directory_create(const fileuid file_uid, const fileuid parent_uid, const char *name, const fileuid previous_uid)
{
    struct directory *directory = NULL;
    struct bson *bson = NULL;
    struct bson *files = NULL;
    int rc = 0;

    bson = directory_init_file(file_uid, name, FILE_TYPE_DIRECTORY);
    if (bson == NULL) {
        log_error("Failed to create directory, cannot allocate bson");
        goto failed_init_bson;
    }

    directory = directory_init(file_uid, bson);
    if (directory == NULL) {
        log_error("Failed to create directory, cannot allocate directory");
        goto failed_init_directory;
    }

    if (parent_uid) {
        rc = bson_set_cstring(bson, "parent_uid", parent_uid);
        if (rc == -1) {
            log_error("Failed to set `parent_uid` in bson data directory");
            goto failed_set_parent;
        }
    }

    if (previous_uid) {
        rc = bson_set_cstring(bson, "previous", previous_uid);
        if (rc == -1) {
            log_error("Failed to set `previous` in bson data directory");
            goto failed_set_parent;
        }
    }

    files = bson_set_empty_array(bson, "files");
    if (files == NULL) {
        log_error("Failed to init array `files` in bson data directory");
        goto failed_set_files;
    }

    return directory;

failed_set_files:
failed_set_parent:
    directory_destroy(directory);
failed_init_directory:
    bson_destroy(bson);
failed_init_bson:
    return NULL;
}

static bool
find_node_by_uid(struct bson *node, const void *arg) {
    struct string *uid = NULL;

    if (bson_get_string(node, "uid", &uid)) {
        if (string_eq_cstr(uid, arg)) {
            return true;
        }
    }

    return false;
}

struct bson *
directory_get_file(struct directory *directory, const fileuid file_uid, char *out_next_file_uid)
{
    struct bson *files = NULL;
    struct bson *file = NULL;
    struct string *next = NULL;

    bzero(out_next_file_uid, FILE_UID_LEN);

    files = bson_get(directory->data, "files");
    if (files == NULL) {
        log_error("Corrupted directory, no `files' key");
        return NULL;
    }

    file = bson_find_in_array(files, find_node_by_uid, file_uid);
    if (file != NULL) {
        return file;
    }

    if (bson_get_string(directory->data, "next", &next)) {
        strlcpy(out_next_file_uid, string_data(next), FILE_UID_LEN);
    }

    return NULL;
}

int
directory_add_file(struct directory *directory, struct bson *file_data, char *out_next_file_uid)
{
    struct bson *last_file = NULL;
    struct bson *files = NULL;
    struct bson *file = NULL;
    struct string *next = NULL;
    size_t count = 0;

    files = bson_get(directory->data, "files");

    file = files->v_child;
    while (file != NULL) {
        count++;
        last_file = file;
        file = file->next;
    }

    if (count < MAX_FILE_PER_DIRECTORY) {
        if (last_file == NULL) {
            files->v_child = file_data;
        } else {
            last_file->next = file_data;
        }
    } else {
        if (bson_get_string(directory->data, "next", &next)) {
            strlcpy(out_next_file_uid, string_data(next), FILE_UID_LEN);
        } else {
            bzero(out_next_file_uid, FILE_UID_LEN);
        }
        return 1;
    }

    return 0;
}

struct bson *
directory_iterate_files(struct directory *directory)
{
    if (directory->file_iterator == NULL) {
        struct bson *files = NULL;
        files = bson_get(directory->data, "files");
        if (files == NULL) {
            log_error("Missing files in directory `%s'", directory->file_uid);
            return NULL;
        }
        directory->file_iterator = files->v_child;
    } else {
        directory->file_iterator = directory->file_iterator->next;
    }

    return directory->file_iterator;
}

int
directory_remove_file(struct directory *directory, struct bson *file_metadata)
{
    struct bson *files = NULL;

    files = bson_get(directory->data, "files");
    if (files == NULL) {
        log_error("Corrupted directory: Missing `files' in directory `%s'", directory->file_uid);
        return -1;
    }

    if (not bson_remove_from_array(files, file_metadata)) {
        log_error("Failed to remove file from directory `%s'", directory->file_uid);
        return -1;
    }

    return 0;
}
