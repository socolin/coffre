#ifndef COFFRE_SAFE_H
#define COFFRE_SAFE_H

#include "core/privsep.h"
#include "utils/dictionary.h"

struct safe_process_data {
    struct dictionary *open_safes;
    struct dictionary *safes_by_token;
};

int start_safe(struct privsep_process *);

#endif //COFFRE_SAFE_H
