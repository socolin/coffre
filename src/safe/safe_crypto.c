#include "safe_crypto.h"

#include <iso646.h>
#include <openssl/bn.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <limits.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/param.h>

#include "safe_types.h"
#include "compat/linux.h"
#include "utils/log.h"
#include "safe_impl.h"

#define MAX_IN_MEMORY_FILE_SIZE (512 * 1024)

#ifdef SNOW_ENABLED
#define KEY_SIZE 1024
#else
#define KEY_SIZE 4096
#endif

bool
safe_generate_key(struct safe *safe, const char *passphrase)
{
    char path[PATH_MAX];
    int bits = KEY_SIZE;
    unsigned long e = RSA_F4;

    BIO *bp_public = NULL;
    BIO *bp_private = NULL;
    RSA *rsa = NULL;
    BIGNUM *bne = NULL;
    const EVP_CIPHER *cipher = NULL;

    int rc = 0;
    bool success = false;

    // 1)

    cipher = EVP_get_cipherbyname("aes128");
    if (cipher == NULL) {
        log_serror("Failed to find enc 'aes128'");
        goto free_all;
    }

    bne = BN_new();
    if (bne == NULL) {
        log_serror("Failed to allocate BIGNUM: %s", ERR_error_string(ERR_get_error(), NULL));
        goto free_all;
    }

    rc = BN_set_word(bne, e);
    if (rc != 1) {
        log_serror("Failed to set BIGNUM word: %s", ERR_error_string(ERR_get_error(), NULL));
        goto free_all;
    }

    rsa = RSA_new();
    if (rsa == NULL) {
        log_serror("Failed to allocate RSA: %s", ERR_error_string(ERR_get_error(), NULL));
        goto free_all;
    }

    log_sinfo("Start generating rsa key");
    rc = RSA_generate_key_ex(rsa, bits, bne, NULL);
    if (not rc) {
        log_serror("Failed to generate RSA key: %s", ERR_error_string(ERR_get_error(), NULL));
        goto free_all;
    }
    log_sinfo("Rsa key generated");

    // 2)

    rc = snprintf(path, sizeof(path), "%s/%s/keys", SAFES_DIRECTORY, safe->name);
    if (rc >= sizeof(path)) {
        log_serror("Failed to generate path for keys, too long ?");
        goto free_all;
    }

    rc = mkdir(path, 0700);
    if (rc >= sizeof(path)) {
        log_serror("Failed to mkdir keys path: %m");
        goto free_all;
    }

    // 3)

    rc = snprintf(path, sizeof(path), "%s/%s/keys/public.pem", SAFES_DIRECTORY, safe->name);
    if (rc >= sizeof(path)) {
        log_serror("Failed to generate path for public key, too long ?");
        goto free_all;
    }

    bp_public = BIO_new_file(path, "w+");
    if (bp_public == NULL) {
        log_serror("Failed to BIO_new_file public.pem: %s", ERR_error_string(ERR_get_error(), NULL));
        goto free_all;
    }

    rc = PEM_write_bio_RSA_PUBKEY(bp_public, rsa);
    if (not rc) {
        log_serror("Failed to PEM_write_bio_RSAPublicKey: %s", ERR_error_string(ERR_get_error(), NULL));
        goto free_all;
    }

    // 4)

    rc = snprintf(path, sizeof(path), "%s/%s/keys/private.pem", SAFES_DIRECTORY, safe->name);
    if (rc >= sizeof(path)) {
        log_serror("Failed to generate path for public key, too long ?");
        goto free_all;
    }

    bp_private = BIO_new_file(path, "w+");
    if (bp_private == NULL) {
        log_serror("Failed to BIO_new_file private.pem: %s", ERR_error_string(ERR_get_error(), NULL));
        goto free_all;
    }

    rc = PEM_write_bio_RSAPrivateKey(bp_private, rsa, cipher, NULL, 0, NULL, (void *) passphrase);
    if (not rc) {
        log_serror("Failed to PEM_write_bio_RSAPrivateKey: %s", ERR_error_string(ERR_get_error(), NULL));
        goto free_all;
    }

    // 5)

    success = true;

free_all:

    BIO_free_all(bp_public);
    BIO_free_all(bp_private);
    RSA_free(rsa);
    BN_free(bne);

    if (not success) {
        snprintf(path, sizeof(path), "%s/%s/keys/private.pem", SAFES_DIRECTORY, safe->name);
        unlink(path);
        snprintf(path, sizeof(path), "%s/%s/keys/public.pem", SAFES_DIRECTORY, safe->name);
        unlink(path);
        snprintf(path, sizeof(path), "%s/%s/keys", SAFES_DIRECTORY, safe->name);
        rc = rmdir(path);
        if (rc == -1 && errno != ENOENT) {
            log_salert("Failed to delete safe directory `%s': %m", path);
        }
    }

    return success;
}

int
safe_load_key(struct safe *safe, const char *passphrase)
{
    char path[PATH_MAX];
    BIO *bp_public = NULL;
    BIO *bp_private = NULL;
    RSA *rsa = NULL;
    int rc = 0;

    // 1)

    rc = snprintf(path, sizeof(path), "%s/%s/keys/public.pem", SAFES_DIRECTORY, safe->name);
    if (rc >= sizeof(path)) {
        log_serror("Failed to generate path for public key, too long ?");
        goto free_all;
    }

    bp_public = BIO_new_file(path, "r");
    if (bp_public == NULL) {
        log_serror("Failed to BIO_new_file `%s': %s", path, ERR_error_string(ERR_get_error(), NULL));
        goto free_all;
    }

    rsa = PEM_read_bio_RSA_PUBKEY(bp_public, &rsa, NULL, NULL);
    if (rsa == NULL) {
        log_serror("Failed to PEM_read_bio_RSA_PUBKEY: %s", ERR_error_string(ERR_get_error(), NULL));
        goto free_all;
    }

    BIO_free_all(bp_public);
    bp_public = NULL;

    // 2)

    rc = snprintf(path, sizeof(path), "%s/%s/keys/private.pem", SAFES_DIRECTORY, safe->name);
    if (rc >= sizeof(path)) {
        log_serror("Failed to generate path for public key, too long ?");
        goto free_all;
    }

    bp_private = BIO_new_file(path, "r");
    if (bp_private == NULL) {
        log_serror("Failed to BIO_new_file `%s': %s", path, ERR_error_string(ERR_get_error(), NULL));
        goto free_all;
    }

    rsa = PEM_read_bio_RSAPrivateKey(bp_private, &rsa, NULL, (void *) passphrase);
    if (rsa == NULL) {
        log_serror("Failed to PEM_read_bio_RSAPrivateKey: %s", ERR_error_string(ERR_get_error(), NULL));
        goto free_all;
    }

    BIO_free_all(bp_private);
    bp_private = NULL;

    safe->key = safe_key_init(rsa);
    if (safe->key == NULL) {
        log_serror("Failed to safe_key_init");
        goto free_all;
    }

    return 0;

free_all:
    BIO_free_all(bp_private);
    BIO_free_all(bp_public);
    RSA_free(rsa);
    return -1;
}

int
safe_load_file_key(struct safe *safe, const char *file_path, unsigned char *out_file_key)
{
    ssize_t read_count = 0;
    int fd = -1;
    int rsa_key_size = 0;
    unsigned char *encrypted_key = NULL;
    char path[PATH_MAX];
    int key_len = 0;
    int rc = 0;
    unsigned char *file_key = NULL;

    // 1)

    rsa_key_size = RSA_size(safe->key->rsa);

    encrypted_key = calloc(1, (size_t) rsa_key_size);
    if (encrypted_key == NULL) {
        goto failed_alloc_encrypted_key;
    }

    file_key = calloc(1, (size_t) rsa_key_size);
    if (file_key == NULL) {
        goto failed_alloc_encrypted_key;
    }

    // 2)

    rc = snprintf(path, sizeof(path), "%s.key", file_path);
    if (rc >= sizeof(path)) {
        log_serror("Failed to generate path for file key, too long ?");
        goto failed_key_path;
    }

    fd = open(path, O_RDONLY);
    if (fd == -1) {
        log_serror("Failed to open key file `%s': %m", path);
        goto failed_open_key_file;
    }

    bzero(path, sizeof(path));

    read_count = read(fd, encrypted_key, (size_t) rsa_key_size);
    if (read_count == -1) {
        log_serror("Failed to write key file: %m");
        goto failed_read_encrypted_key;
    }
    if (read_count != rsa_key_size) {
        log_serror("Key not fully written to file");
        goto failed_read_encrypted_key;
    }

    close(fd);
    fd = -1;

    // 3)

    key_len = RSA_private_decrypt(rsa_key_size, encrypted_key, file_key, safe->key->rsa, RSA_PKCS1_PADDING);
    if (key_len == -1) {
        log_serror("Failed to decrypt file key with safe public key: %s", ERR_error_string(ERR_get_error(), NULL));
        goto failed_decrypt_key;
    }
    if (key_len != FILE_KEY_LEN) {
        log_serror("Decrypted key does not match expected file key size %d instead of %d", key_len, FILE_KEY_LEN);
        goto failed_decrypt_key;
    }

    memmove(out_file_key, file_key, FILE_KEY_LEN);

    freezero(encrypted_key, (size_t) rsa_key_size);
    freezero(file_key, (size_t) rsa_key_size);

    return 0;

failed_decrypt_key:
failed_read_encrypted_key:
    close(fd);
failed_open_key_file:
failed_key_path:
failed_alloc_encrypted_key:
    freezero(encrypted_key, (size_t) rsa_key_size);
    freezero(file_key, (size_t) rsa_key_size);
    explicit_bzero(path, sizeof(path));
    return -1;
}


// 1) Generate random key for this file
// 2) Encrypt key with public key of safe
// 3) Save key to file
int
safe_create_file_key(struct safe *safe, const char *file_path, unsigned char *out_file_key)
{
    unsigned char file_key[FILE_KEY_LEN];
    char path[PATH_MAX];
    int rsa_key_size = 0;
    unsigned char *encrypted_key = NULL;
    int encrypted_key_len = 0;
    ssize_t written = 0;
    int fd = -1;
    int rc = 0;

    // 1)

    arc4random_buf(file_key, sizeof(file_key));

    // 2)

    rsa_key_size = RSA_size(safe->key->rsa);
    encrypted_key = calloc((size_t) rsa_key_size, sizeof(char));
    if (encrypted_key == NULL) {
        goto failed_calloc_key_encrypted;
    }

    encrypted_key_len = RSA_public_encrypt(sizeof(file_key), file_key, encrypted_key, safe->key->rsa, RSA_PKCS1_PADDING);
    if (encrypted_key_len == -1) {
        log_serror("Failed to encrypt file key with safe public key: %s", ERR_error_string(ERR_get_error(), NULL));
        goto failed_encrypt_key;
    }

    // 3)

    rc = snprintf(path, sizeof(path), "%s.key", file_path);
    if (rc >= sizeof(path)) {
        log_serror("Failed to generate path for file key, too long ?");
        goto failed_key_path;
    }

    fd = open(path, O_WRONLY | O_CREAT, 0600);
    if (fd == -1) {
        log_serror("Failed to create new key file `%s': %m", path);
        goto failed_save_key;
    }

    bzero(path, sizeof(path));

    written = write(fd, encrypted_key, (size_t) encrypted_key_len);
    if (written == -1) {
        log_serror("Failed to write key file: %m");
        goto failed_write_key;
    }
    if (written != encrypted_key_len) {
        log_serror("Key not fully written to file");
        goto failed_write_key;
    }

    close(fd);
    fd = -1;

    memmove(out_file_key, file_key, sizeof(file_key));

    freezero(encrypted_key, (size_t) rsa_key_size);

    return 0;
failed_write_key:
    close(fd);
    snprintf(path, sizeof(path), "%s.key", file_path);
    unlink(path);
failed_save_key:
    freezero(encrypted_key, (size_t) rsa_key_size);
failed_calloc_key_encrypted:
failed_encrypt_key:
failed_key_path:
    explicit_bzero(file_key, sizeof(file_key));
    return -1;
}


int
safe_encrypt_prepare(struct safe *safe, const char *file_path)
{
    bool new_file = false;
    struct stat sb;
    static const char magic[] = "Salted__";
    char path[PATH_MAX];
    unsigned char file_key[FILE_KEY_LEN];
    int rc = 0;
    BIO *bio_enc = NULL;
    const EVP_CIPHER *cipher = NULL;
    const EVP_MD *dgst = NULL;
    unsigned char salt[PKCS5_SALT_LEN];
    unsigned char key[EVP_MAX_KEY_LENGTH];
    unsigned char iv[EVP_MAX_IV_LENGTH];
    EVP_CIPHER_CTX *ctx = NULL;
    BIO *bio_cipher = NULL;
    struct string *state_file_path = NULL;

    bzero(salt, sizeof(salt));
    bzero(key, sizeof(key));
    bzero(iv, sizeof(iv));
    bzero(path, sizeof(path));
    bzero(file_key, sizeof(file_key));

    // 1)

    if (safe->state != SAFE_STATE_NONE) {
        log_serror("Failed to prepare encrypt file `%s' invalid safe state: %d", file_path, safe->state);
        return -1;
    }

    // 2)

    rc = stat(file_path, &sb);
    if (rc == -1) {
        if (errno != ENOENT) {
            log_serror("Failed to stat file `%s' cannot check if file exists: %m", file_path);
            return -1;
        }
        new_file = true;
    }

    // 3)

    if (new_file) {
        rc = safe_create_file_key(safe, file_path, file_key);
        if (rc == -1) {
            log_serror("Failed to create file key `%s'", file_path);
            goto failed_init_key;
        }
    } else {
        rc = safe_load_file_key(safe, file_path, file_key);
        if (rc == -1) {
            log_serror("Failed to load file key `%s'", file_path);
            goto failed_init_key;
        }
    }

    // 4)

    rc = snprintf(path, sizeof(path), "%s.enc.tmp", file_path);
    if (rc >= sizeof(path)) {
        goto failed_enc_path;
    }

    bio_enc = BIO_new_file(path, "w+");
    if (bio_enc == NULL) {
        log_serror("Failed to BIO_new_file `%s': %s", path, ERR_error_string(ERR_get_error(), NULL));
        goto failed_create_enc_file;
    }

    bzero(path, sizeof(path));

    // 5)

    cipher = EVP_get_cipherbyname("aes-256-cbc");
    if (cipher == NULL) {
        log_serror("Failed to find enc 'aes-256-cbc'");
        goto failed_find_cipher;
    }

    dgst = EVP_get_digestbyname("sha1");
    if (dgst == NULL) {
        log_serror("Failed to find digest 'sha1'");
        goto failed_find_digest;
    }

    arc4random_buf(salt, sizeof(salt));

    rc = BIO_write(bio_enc, magic, sizeof(magic) - 1);
    if (rc != sizeof(magic) - 1) {
        log_serror("Failed to write magic to tmp enc file: %s", ERR_error_string(ERR_get_error(), NULL));
        goto failed_write_magic;
    }

    rc = BIO_write(bio_enc, (char *) salt, sizeof(salt));
    if (rc != sizeof(salt)) {
        log_serror("Failed to write salt to tmp enc file: %s", ERR_error_string(ERR_get_error(), NULL));
        goto failed_write_magic;
    }

    EVP_BytesToKey(cipher, dgst, salt, file_key, sizeof(file_key), 1, key, iv);
    explicit_bzero(file_key, sizeof(file_key));

    // 6)

    bio_cipher = BIO_new(BIO_f_cipher());
    if (bio_cipher == NULL) {
        log_serror("Failed to BIO_new BIO_f_cipher: %s", ERR_error_string(ERR_get_error(), NULL));
        goto failed_bio_cipher;
    }

    BIO_get_cipher_ctx(bio_cipher, &ctx);

    if (!EVP_CipherInit_ex(ctx, cipher, NULL, NULL, NULL, 1)) {
        log_serror("Failed to EVP_CipherInit_ex 1: %s", ERR_error_string(ERR_get_error(), NULL));
        goto failed_cipher_init1;
    }

    if (!EVP_CipherInit_ex(ctx, NULL, NULL, key, iv, 1)) {
        log_serror("Failed to EVP_CipherInit_ex 2: %s", ERR_error_string(ERR_get_error(), NULL));
        goto failed_cipher_init2;
    }

    bio_enc = BIO_push(bio_cipher, bio_enc);

    // 7)

    state_file_path = string_from_cstr(file_path);
    if (state_file_path == NULL) {
        goto failed_alloc_string_file_path;
    }

    safe->state = SAFE_STATE_WRITING;
    bzero(&safe->state_data, sizeof(safe->state_data));
    safe->state_data.writing.bio_enc = bio_enc;
    safe->state_data.writing.file_path = state_file_path;

    explicit_bzero(salt, sizeof(salt));
    explicit_bzero(iv, sizeof(iv));
    explicit_bzero(key, sizeof(key));
    explicit_bzero(file_key, sizeof(file_key));

    return 0;
failed_alloc_string_file_path:
    bio_enc = NULL; // Will be free by first free_all due to chaining
failed_cipher_init2:
failed_cipher_init1:
    BIO_free_all(bio_cipher);
failed_bio_cipher:
    explicit_bzero(iv, sizeof(iv));
    explicit_bzero(key, sizeof(key));
failed_write_magic:
    explicit_bzero(salt, sizeof(salt));
failed_find_digest:
failed_find_cipher:
    BIO_free_all(bio_enc);
failed_create_enc_file:
    snprintf(path, sizeof(path), "%s.enc.tmp", file_path);
    unlink(path);
    bzero(path, sizeof(path));
failed_enc_path:
failed_init_key:
    explicit_bzero(file_key, sizeof(file_key));
    return -1;
}

int
safe_encrypt_write_data(struct safe *safe, char *data, size_t data_size)
{
    int rc = 0;

    rc = BIO_write(safe->state_data.writing.bio_enc, data, (int) data_size);
    if (rc != data_size) {
        log_serror("Failed to write encrypt: %s", ERR_error_string(ERR_get_error(), NULL));
        return -1;
    }

    return rc;
}

int
safe_encrypt_write_sdata(struct safe *safe, struct string *data)
{
    int size = (int) string_length(data);
    int rc = 0;

    rc = BIO_write(safe->state_data.writing.bio_enc, string_data(data), size);
    if (rc != size) {
        log_serror("Failed to write encrypt: %s", ERR_error_string(ERR_get_error(), NULL));
        return -1;
    }

    return 0;
}

int
safe_encrypt_cancel(struct safe *safe)
{
    char path[PATH_MAX];
    int rc = 0;

    rc = snprintf(path, sizeof(path), "%s.enc.tmp", string_data(safe->state_data.writing.file_path));
    safe_state_destroy(safe);
    if (rc >= sizeof(path)) {
        log_serror("Failed to generate path for public key, too long ?");
        return -1;
    }

    rc = unlink(path);
    if (rc == -1) {
        log_serror("Failed to unlink tmp enc file: `%s': %m", path);
        return -1;
    }

    return 0;
}

int
safe_encrypt_complete(struct safe *safe)
{
    char path1[PATH_MAX];
    char path2[PATH_MAX];
    int rc = 0;

    if (!BIO_flush(safe->state_data.writing.bio_enc)) {
        log_serror("Failed to flush encrypt: %s", ERR_error_string(ERR_get_error(), NULL));
        return -1;
    }

    rc = snprintf(path1, sizeof(path1), "%s.enc.tmp", string_data(safe->state_data.writing.file_path));
    if (rc >= sizeof(path1)) {
        log_serror("Failed to generate path for public key, too long ?");
        return -1;
    }

    rc = snprintf(path2, sizeof(path2), "%s.enc", string_data(safe->state_data.writing.file_path));
    if (rc >= sizeof(path2)) {
        log_serror("Failed to generate path for public key, too long ?");
        return -1;
    }

    rc = rename(path1, path2);
    if (rc == -1) {
        log_serror("Failed to rename tmp enc file to final version: `%s'->`%s':%m", path1, path2);
        return -1;
    }

    safe_state_destroy(safe);

    return 0;
}

int
safe_encrypt_file(struct safe *safe, const char *file_path, struct string *data)
{
    int rc = 0;

    if (string_length(data) >= MAX_IN_MEMORY_FILE_SIZE) {
        log_serror("Error in safe `%s': File `%s' is too big: %zu bytes", safe->name, file_path, string_length(data));
        return -1;
    }

    rc = safe_encrypt_prepare(safe, file_path);
    if (rc == -1) {
        return -1;
    }

    rc = safe_encrypt_write_sdata(safe, data);
    if (rc == -1) {
        safe_encrypt_cancel(safe);
        return -1;
    }

    rc = safe_encrypt_complete(safe);
    if (rc == -1) {
        safe_encrypt_cancel(safe);
        return -1;
    }

    return 0;
}

int
safe_decrypt_prepare(struct safe *safe, const char *file_path)
{
    static const char magic[] = "Salted__";
    char magic_buf[sizeof(magic) - 1];
    char path[PATH_MAX];
    unsigned char file_key[FILE_KEY_LEN];
    int rc = 0;
    BIO *bio_enc = NULL;
    const EVP_CIPHER *cipher = NULL;
    const EVP_MD *dgst = NULL;
    EVP_CIPHER_CTX *ctx = NULL;
    BIO *bio_cipher = NULL;
    unsigned char salt[PKCS5_SALT_LEN];
    unsigned char key[EVP_MAX_KEY_LENGTH];
    unsigned char iv[EVP_MAX_IV_LENGTH];

    bzero(key, sizeof(key));
    bzero(iv, sizeof(iv));
    bzero(salt, sizeof(salt));

    // 1)

    if (safe->state != SAFE_STATE_NONE) {
        log_serror("Failed to prepare decrypt file `%s' invalid safe state: %d", file_path, safe->state);
        return -1;
    }

    // 2)

    rc = safe_load_file_key(safe, file_path, file_key);
    if (rc == -1) {
        log_serror("Failed to load file key `%s'", file_path);
        goto failed_load_key;
    }

    // 3)

    rc = snprintf(path, sizeof(path), "%s.enc", file_path);
    if (rc >= sizeof(path)) {
        goto failed_enc_path;
    }

    bio_enc = BIO_new_file(path, "r");
    if (bio_enc == NULL) {
        log_serror("Failed to BIO_new_file `%s' r: %s", path, ERR_error_string(ERR_get_error(), NULL));
        goto failed_open_enc_file;
    }

    bzero(path, sizeof(path));

    // 4)

    cipher = EVP_get_cipherbyname("aes-256-cbc");
    if (cipher == NULL) {
        log_serror("Failed to find enc 'aes-256-cbc'");
        goto failed_find_cipher;
    }

    dgst = EVP_get_digestbyname("sha1");
    if (dgst == NULL) {
        log_serror("Failed to find digest 'sha1'");
        goto failed_find_digest;
    }

    rc = BIO_read(bio_enc, magic_buf, sizeof magic_buf);
    if (rc != sizeof magic_buf) {
        log_serror("Failed to read magic: %s", ERR_error_string(ERR_get_error(), NULL));
        goto failed_read_magic;
    }

    rc = BIO_read(bio_enc, (unsigned char *) salt, sizeof salt);
    if (rc != sizeof salt) {
        log_serror("Failed to read salt: %s", ERR_error_string(ERR_get_error(), NULL));
        goto failed_read_salt;
    }

    if (memcmp(magic_buf, magic, sizeof magic - 1) != 0) {
        log_serror("Invalid magic: `%s'", magic_buf);
        goto failed_read_salt;
    }

    EVP_BytesToKey(cipher, dgst, salt, file_key, sizeof(file_key), 1, key, iv);
    explicit_bzero(file_key, sizeof(file_key));
    explicit_bzero(salt, sizeof(salt));

    // 5)

    bio_cipher = BIO_new(BIO_f_cipher());
    if (bio_cipher == NULL) {
        log_serror("Failed to BIO_new BIO_f_cipher: %s", ERR_error_string(ERR_get_error(), NULL));
        goto failed_bio_cipher;
    }

    BIO_get_cipher_ctx(bio_cipher, &ctx);

    if (!EVP_CipherInit_ex(ctx, cipher, NULL, NULL, NULL, 0)) {
        log_serror("Failed to EVP_CipherInit_ex 1: %s", ERR_error_string(ERR_get_error(), NULL));
        goto failed_cipher_init1;
    }

    if (!EVP_CipherInit_ex(ctx, NULL, NULL, key, iv, 0)) {
        log_serror("Failed to EVP_CipherInit_ex 2: %s", ERR_error_string(ERR_get_error(), NULL));
        goto failed_cipher_init2;
    }

    bio_enc = BIO_push(bio_cipher, bio_enc);

    // 6)

    safe->state = SAFE_STATE_READING;
    bzero(&safe->state_data, sizeof(safe->state_data));
    safe->state_data.reading.bio_enc = bio_enc;

    return 0;

failed_cipher_init2:
failed_cipher_init1:
    BIO_free_all(bio_cipher);
failed_bio_cipher:
failed_read_salt:
    explicit_bzero(salt, sizeof(salt));
failed_read_magic:
failed_find_digest:
failed_find_cipher:
    BIO_free_all(bio_enc);
failed_open_enc_file:
failed_enc_path:
    explicit_bzero(file_key, sizeof(file_key));
failed_load_key:
    explicit_bzero(path, sizeof(path));
    return -1;
}

struct string *
safe_decrypt_read_data(struct safe *safe, size_t count)
{
    struct string *data = NULL;
    ssize_t read_count = 0;
    size_t total_read = 0;
    char buffer[BUFSIZ];
    int rc = 0;

    if (safe->state != SAFE_STATE_READING) {
        log_serror("Failed to decrypt read data, invalid safe state: %d", safe->state);
        return NULL;
    }

    data = string_init();
    if (data == NULL) {
        return NULL;
    }

    int to_read = MIN(sizeof(buffer), count - total_read);
    while (to_read) {
        read_count = BIO_read(safe->state_data.reading.bio_enc, (char *) buffer, to_read);
        if (read_count < 0) {
            log_serror("Failed to BIO_read data: %s", ERR_error_string(ERR_get_error(), NULL));
            return NULL;
        }
        if (read_count == 0) {
            break;
        }

        rc = string_nappend(data, buffer, (size_t) read_count);
        if (rc == -1) {
            return NULL;
        }
        total_read += read_count;
        to_read = MIN(sizeof(buffer), count - total_read);
    }

    return data;
}

void
safe_decrypt_complete(struct safe *safe)
{
    BIO_free_all(safe->state_data.reading.bio_enc);
    safe->state = SAFE_STATE_NONE;
    bzero(&safe->state_data, sizeof(safe->state_data));
}

struct string *
safe_decrypt_file(struct safe *safe, const char *file_path)
{
    int rc = 0;
    struct string *data = NULL;

    rc = safe_decrypt_prepare(safe, file_path);
    if (rc == -1) {
        return NULL;
    }

    // Limit max size to 512KB, no file to be decrypted and used locally should be greater than this
    data = safe_decrypt_read_data(safe, MAX_IN_MEMORY_FILE_SIZE);
    if (data == NULL) {
        safe_decrypt_complete(safe);
        return NULL;
    }

    safe_decrypt_complete(safe);

    return data;
}
