#ifndef COFFRE_SAFE_IMPL_H
#define COFFRE_SAFE_IMPL_H

#include <stdbool.h>
#include <openssl/rsa.h>

#include "core/msg_protocol.h"
#include "core/types.h"
#include "utils/bson.h"
#include "safe_types.h"
#include "directory.h"
#include "utils/dictionary.h"

#ifdef SNOW_ENABLED
#define SAFES_DIRECTORY "test_safes"
#else
#define SAFES_DIRECTORY "/safes"
#endif

enum safe_state
{
    SAFE_STATE_NONE,
    SAFE_STATE_WRITING,
    SAFE_STATE_READING,
};

struct safe_key {
    size_t ref_cnt;
    RSA *rsa;
};

struct safe_share_token {
    share_token token;
    enum safe_mode mode;
    time_t expire;
};

#define MAX_SHARE_TOKENS 5

struct safe {
    safe_name name;
    struct safe_key *key;
    enum safe_state state;
    enum safe_mode mode;
    struct bson *conf;
    union {
        struct {
            BIO *bio_enc;
            fileuid directory_uid;
            char filename[MAX_DISPLAY_FILENAME_SIZE + 1];
            fileuid file_uid;
            size_t written;
            struct string *file_path;
        } writing;
        struct {
            BIO *bio_enc;
        } reading;
    } state_data;
    size_t token_count;
    struct safe_share_token tokens[MAX_SHARE_TOKENS];
};

void safe_gen_file_uid(char *file_uid);

struct safe_key *safe_key_init(RSA *rsa);
void safe_key_incr_ref(struct safe_key *);
void safe_key_decr_ref(struct safe_key *);

struct safe *safe_init(const safe_name);
void safe_destroy(struct safe *);
void safe_state_destroy(struct safe *);
int safe_delete(struct safe *);
bool safe_exists(const safe_name);
struct safe *safe_create_new(safe_name, const char *);
struct safe *safe_load(safe_name, const char *);
struct safe *safe_load_with_share_token(struct dictionary*, const share_token);

int safe_save_directory(struct safe *, const struct directory *);
struct directory *safe_load_directory(struct safe *, const char *);
int safe_prepare_save_data_file(struct safe *, const char *, const char *, char *);
int safe_continue_save_data(struct safe *, char *, size_t);
int safe_continue_save_sdata(struct safe *, struct string *);
int safe_finish_save_data(struct safe *, fileuid);
int safe_cancel_save_data(struct safe *);

int safe_prepare_read_file(struct safe *, const fileuid);
int safe_continue_read_file(struct safe *, struct string **, size_t);
void safe_cancel_read_file(struct safe *);

int safe_delete_file(struct safe *, const fileuid, const fileuid);

int safe_token_add(struct safe *, enum safe_mode, const share_token, struct dictionary *, time_t);
void safe_token_remove(struct safe *, const share_token, struct dictionary *);
enum safe_mode safe_token_contains(const struct safe *, const share_token);
int safe_share_key(struct safe *, enum safe_mode, share_token, struct dictionary *, time_t);
void safe_token_remove_all(struct safe *, struct dictionary *);

#endif //COFFRE_SAFE_IMPL_H
