#ifndef COFFRE_DIRECTORY_H
#define COFFRE_DIRECTORY_H

#include "core/types.h"
#include "utils/bson.h"
#include "safe_types.h"

enum file_type {
    FILE_TYPE_DIRECTORY = 0,
    FILE_TYPE_FILE = 1,
    FILE_TYPE_LINK = 2
};

struct directory {
    char file_uid[FILE_UID_LEN];
    struct bson *data;
    struct bson *file_iterator;
};

struct directory *directory_init(const fileuid, struct bson *);
void directory_destroy(struct directory *);

struct directory *directory_create(const fileuid , const fileuid , const char *, const fileuid);
struct bson *directory_get_file(struct directory *, const fileuid, char *);
int directory_add_file(struct directory *, struct bson*, char *);
struct bson *directory_init_file(const fileuid, const char *, enum file_type);
struct bson *directory_iterate_files(struct directory *);
int directory_remove_file(struct directory *, struct bson *);

#endif //COFFRE_DIRECTORY_H
