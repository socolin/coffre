#include "safe_impl.h"

#include <stdlib.h>
#include <string.h>
#include <iso646.h>
#include <limits.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <errno.h>
#include <unistd.h>
#include <stdbool.h>
#include <fcntl.h>

#include "compat/linux.h"
#include "utils/fs.h"
#include "utils/log.h"
#include "safe_crypto.h"
#include "directory.h"

struct safe_key *
safe_key_init(RSA *rsa)
{
    struct safe_key *safe_key = NULL;

    safe_key = calloc(1, sizeof(*safe_key));
    if (safe_key == NULL) {
        log_error("Failed to calloc safe_key struct: %m");
        return NULL;
    }
    safe_key->rsa = rsa;
    safe_key->ref_cnt = 1;

    return safe_key;
}

void
safe_key_incr_ref(struct safe_key *safe_key)
{
    safe_key->ref_cnt++;
}

void
safe_key_decr_ref(struct safe_key *safe_key)
{
    safe_key->ref_cnt--;
    if (safe_key->ref_cnt == 0) {
        RSA_free(safe_key->rsa);
        freezero(safe_key, sizeof(*safe_key));
    }
}

struct safe *
safe_init(const safe_name name)
{
    struct safe *safe = NULL;

    safe = calloc(1, sizeof(*safe));
    if (safe == NULL) {
        log_error("Failed to calloc safe struct for safe `%s': %m", name);
        return NULL;
    }

    strlcpy(safe->name, name, sizeof(safe->name));

    return safe;
}

void
safe_state_destroy(struct safe *safe)
{
    switch (safe->state) {
        case SAFE_STATE_WRITING:
            string_destroy(safe->state_data.writing.file_path);
            BIO_free_all(safe->state_data.writing.bio_enc);
            break;
        case SAFE_STATE_READING:
            BIO_free_all(safe->state_data.reading.bio_enc);
            break;
        default:
            break;
    }
    safe->state = SAFE_STATE_NONE;
}

void
safe_destroy(struct safe *safe)
{
    if (safe == NULL) {
        return;
    }
    if (safe->state == SAFE_STATE_WRITING) {
        safe_encrypt_cancel(safe);
    }
    safe_state_destroy(safe);
    if (safe->key != NULL) {
        safe_key_decr_ref(safe->key);
    }
    freezero(safe, sizeof(*safe));
}

bool
safe_exists(const safe_name name)
{
    char safe_path[PATH_MAX];
    struct stat sb;
    int rc;

    snprintf(safe_path, sizeof(safe_path), "%s/%s", SAFES_DIRECTORY, name);

    rc = stat(safe_path, &sb);
    if (rc == -1) {
        if (errno != ENOENT) {
            log_error("Failed to stat safe directory `%s' to test if it exists: %m", safe_path);
        }
        return false;
    }

    if (!S_ISDIR(sb.st_mode)) {
        log_error("Error when testing if safe exists: `%s' should be a directory", safe_path);
        return false;
    }

    return true;
}

int
safe_delete(struct safe *safe)
{
    char safe_path[PATH_MAX];
    int rc = 0;

    snprintf(safe_path, sizeof(safe_path), "%s/%s", SAFES_DIRECTORY, safe->name);

    rc = fs_rmdir_recursive(safe_path);
    if (rc == -1) {
        log_error("Failed to delete safe directory `%s': %m", safe_path);

        return -1;
    }

    return 0;
}

int
safe_conf_save(struct safe *safe)
{
    char conf_path[PATH_MAX];
    struct string *conf_data = NULL;
    int rc = 0;

    conf_data = bson_encode(safe->conf);
    if (conf_data == NULL) {
        log_serror("Failed to encode bson conf of safe");
        return -1;
    }

    snprintf(conf_path, sizeof(conf_path), "%s/%s/conf", SAFES_DIRECTORY, safe->name);

    rc = safe_encrypt_file(safe, conf_path, conf_data);
    bzero(conf_path, sizeof(conf_path));
    if (rc == -1) {
        return -1;
    }

    return 0;
}

int
safe_conf_init(struct safe *safe)
{
    int rc = 0;
    struct directory *root = NULL;
    char file_uid[FILE_UID_LEN];

    bzero(file_uid, sizeof(file_uid));

    safe->conf = bson_init(BSON_TYPE_DOCUMENT);
    if (safe->conf == NULL) {
        log_serror("Failed to alloc conf of safe");
        goto failed_init_conf;
    }

    safe_gen_file_uid(file_uid);

    rc = bson_set_cstring(safe->conf, "root", file_uid);
    if (rc == -1) {
        log_serror("Failed to set root file uid in conf of safe");
        goto failed_save_conf;
    }

    root = directory_create(file_uid, NULL, "root", NULL);
    if (root == NULL) {
        log_serror("Failed to create root directory");
        goto failed_create_directory;
    }

    rc = safe_save_directory(safe, root);
    if (rc == -1) {
        log_serror("Failed to save root directory");
        goto failed_save_directory;
    }

    rc = safe_conf_save(safe);
    if (rc == -1) {
        log_serror("Failed to init conf of safe");
        goto failed_save_conf;
    }

    return 0;
failed_save_conf:
failed_save_directory:
    directory_destroy(root);
failed_create_directory:
    bson_destroy(safe->conf);
failed_init_conf:
    return -1;
}

struct safe *
safe_create_new(safe_name name, const char *passphrase)
{
    char safe_path[PATH_MAX];
    struct safe *safe = NULL;
    int rc = 0;

    if (safe_exists(name)) {
        log_warning("Safe `%s' already exists", name);
        return NULL;
    }

    snprintf(safe_path, sizeof(safe_path), "%s/%s", SAFES_DIRECTORY, name);

    rc = mkdir(safe_path, 0700);
    if (rc == -1) {
        log_error("Failed to mkdir safe directory `%s': %m", safe_path);
        goto mkdir_failed;
    }

    snprintf(safe_path, sizeof(safe_path), "%s/%s/data", SAFES_DIRECTORY, name);

    rc = mkdir(safe_path, 0700);
    if (rc == -1) {
        log_error("Failed to mkdir safe data directory `%s': %m", safe_path);
        goto mkdir_data_failed;
    }

    bzero(safe_path, sizeof(safe_path));

    safe = safe_init(name);
    if (safe == NULL) {
        goto alloc_safe_failed;
    }
    safe->mode = SAFE_MODE_CONTROL;

    if (not safe_generate_key(safe, passphrase)) {
        goto key_gen_failed;
    }

    rc = safe_load_key(safe, passphrase);
    if (rc == -1) {
        log_serror("Failed to load key of safe `%s'", name);
        goto safe_conf_init_failed;
    }

    rc = safe_conf_init(safe);
    if (rc == -1) {
        log_serror("Failed to init configuration of safe `%s'", name);
        goto safe_conf_init_failed;
    }

    return safe;
safe_conf_init_failed:
    safe_delete(safe);
key_gen_failed:
    safe_destroy(safe);
alloc_safe_failed:
    snprintf(safe_path, sizeof(safe_path), "%s/%s/data", SAFES_DIRECTORY, name);
    rmdir(safe_path);
mkdir_data_failed:
    snprintf(safe_path, sizeof(safe_path), "%s/%s", SAFES_DIRECTORY, name);
    rmdir(safe_path);
mkdir_failed:
    bzero(safe_path, sizeof(safe_path));
    return NULL;
}

struct safe *
safe_load(safe_name name, const char *passphrase)
{
    char conf_path[PATH_MAX];
    struct safe *safe = NULL;
    struct string *conf_data = NULL;
    int rc;

    explicit_bzero(conf_path, sizeof(conf_path));

    if (not safe_exists(name)) {
        return NULL;
    }

    safe = safe_init(name);
    if (safe == NULL) {
        log_error("Failed to alloc safe");
        goto failed_init;
    }
    safe->mode = SAFE_MODE_CONTROL;

    rc = safe_load_key(safe, passphrase);
    if (rc == -1) {
        // FIXME: invalid passphrase ?
        log_serror("Failed load to key of safe `%s'", name);
        goto failed_load_key;
    }

    snprintf(conf_path, sizeof(conf_path), "%s/%s/conf", SAFES_DIRECTORY, safe->name);
    conf_data = safe_decrypt_file(safe, conf_path);
    if (conf_data == NULL) {
        log_serror("Failed to decrypt conf file of safe `%s'", name);
        goto failed_load_safe;
    }

    safe->conf = bson_decode(string_data(conf_data), string_length(conf_data));
    if (safe->conf == NULL) {
        goto failed_decode_conf;
    }

    string_destroy(conf_data);
    explicit_bzero(conf_path, sizeof(conf_path));

    return safe;
failed_decode_conf:
    string_destroy(conf_data);
failed_load_safe:
    explicit_bzero(conf_path, sizeof(conf_path));
failed_load_key:
    safe_destroy(safe);
failed_init:
    return NULL;
}

struct safe *
safe_load_with_share_token(struct dictionary *safes_by_token, const share_token token)
{
    char conf_path[PATH_MAX];
    struct safe *safe = NULL;
    struct string *conf_data = NULL;
    struct safe *src_safe = NULL;
    enum safe_mode mode;

    explicit_bzero(conf_path, sizeof(conf_path));

    src_safe = dictionary_get(safes_by_token, token);
    if (src_safe == NULL) {
        log_warning("Failed to find token `%s' in safes_by_token", token);
        return NULL;
    }

    if (not safe_exists(src_safe->name)) {
        return NULL;
    }

    mode = safe_token_contains(src_safe, token);
    if (mode == SAFE_MODE_INIT) {
        log_warning("Failed to find token `%s' in safe `%s'", token, src_safe->name);
        return NULL;
    }

    safe = safe_init(src_safe->name);
    if (safe == NULL) {
        log_error("Failed to alloc safe");
        goto failed_init;
    }

    safe_key_incr_ref(src_safe->key);
    safe->key = src_safe->key;
    safe->mode = mode;

    snprintf(conf_path, sizeof(conf_path), "%s/%s/conf", SAFES_DIRECTORY, safe->name);
    conf_data = safe_decrypt_file(safe, conf_path);
    if (conf_data == NULL) {
        log_serror("Failed to decrypt conf file of safe `%s'", safe->name);
        goto failed_load_safe;
    }

    safe->conf = bson_decode(string_data(conf_data), string_length(conf_data));
    if (safe->conf == NULL) {
        goto failed_decode_conf;
    }

    string_destroy(conf_data);
    explicit_bzero(conf_path, sizeof(conf_path));
    safe_token_remove(src_safe, token, safes_by_token);

    return safe;
failed_decode_conf:
    string_destroy(conf_data);
failed_load_safe:
    explicit_bzero(conf_path, sizeof(conf_path));
    safe_destroy(safe);
failed_init:
    return NULL;
}

void
safe_gen_file_uid(char *file_uid)
{
    size_t i = 0;
    uint8_t rand[FILE_UID_BYTES_LEN];

    arc4random_buf(rand, sizeof(rand));
    for (; i < sizeof(rand); i++) {
        snprintf(file_uid + (i * 2), 3, "%0.2x", rand[i]);
    }
}

struct directory *
safe_load_directory(struct safe *safe, const char *file_uid)
{
    struct string *directory_data = NULL;
    char path[PATH_MAX];
    struct bson *bson = NULL;

    snprintf(path, sizeof(path), "%s/%s/data/%.2s/%s", SAFES_DIRECTORY, safe->name, file_uid, file_uid);
    directory_data = safe_decrypt_file(safe, path);
    if (directory_data == NULL) {
        log_serror("Failed to load directory data `%s'", path);
        goto failed_load_data;
    }

    bson = bson_decode(string_data(directory_data), string_length(directory_data));
    if (bson == NULL) {
        log_serror("Failed to decode directory data `%s'", path);
        goto fail_decode_data;
    }
    string_destroy(directory_data);
    directory_data = NULL;

    struct directory *directory = directory_init(file_uid, bson);
    if (directory == NULL) {
        log_serror("Failed to init directory structure");
        goto failed_init_directory;
    }

    return directory;

failed_init_directory:
    bson_destroy(bson);
fail_decode_data:
    string_destroy(directory_data);
failed_load_data:
    return NULL;
}

int
safe_save_directory(struct safe *safe, const struct directory *directory)
{
    char path[PATH_MAX];
    struct string *data = NULL;
    int rc = 0;

    // 1)

    data = bson_encode(directory->data);
    if (data == NULL) {
        log_error("Failed to encode directory data `%s'", directory->file_uid);
        goto failed_encode_data;
    }

    // 2)

    snprintf(path, sizeof(path), "%s/%s/data/%.2s", SAFES_DIRECTORY, safe->name, directory->file_uid);
    rc = mkdir(path, 0700);
    if (rc == -1) {
        if (errno != EEXIST) {
            log_error("Failed to mkdir safe data directory `%s': %m", path);
            goto failed_mkdir;
        }
    }

    // 3)

    snprintf(path, sizeof(path), "%s/%s/data/%.2s/%s", SAFES_DIRECTORY, safe->name, directory->file_uid, directory->file_uid);
    rc = safe_encrypt_file(safe, path, data);
    if (rc == -1) {
        log_error("Failed to encrypt directory file `%s'", directory->file_uid);
        goto failed_encrypt;
    }

    string_destroy(data);

    return 0;
failed_encrypt:
failed_mkdir:
    string_destroy(data);
failed_encode_data:
    return -1;
}

static int
safe_directory_get_file(struct safe *safe, const char *parent_uid, const char *file_uid,
                        struct bson **out_file_metadata, struct directory **out_directory)
{
    char directory_uid[FILE_UID_LEN];
    struct directory *directory = NULL;

    strlcpy(directory_uid, parent_uid, sizeof(directory_uid));
    do {
        directory_destroy(directory);
        directory = safe_load_directory(safe, directory_uid);
        if (directory == NULL) {
            log_serror("Failed to load directory data: `%s'", directory_uid);
            return -1;
        }

        *out_file_metadata = directory_get_file(directory, file_uid, directory_uid);
        *out_directory = directory;
    } while (*out_file_metadata == NULL && *directory_uid);

    return 0;
}

static int
safe_directory_grow(struct safe *safe, const struct directory *previous, struct bson *file_metadata,
                    fileuid out_directory_uid)
{
    char directory_uid[FILE_UID_LEN];
    struct directory *directory = NULL;
    int rc = 0;

    safe_gen_file_uid(directory_uid);
    // FIXME: check not used

    directory = directory_create(directory_uid, NULL, "", previous->file_uid);
    if (directory == NULL) {
        log_serror("Failed to create new directory to grow directory: `%s'", previous->file_uid);
        goto failed_create_directory;
    }

    rc = directory_add_file(directory, file_metadata, directory_uid);
    if (rc == -1) {
        log_serror("Failed to add file to directory: `%s'", directory_uid);
        goto failed_add_file;
    }

    rc = bson_set_cstring(previous->data, "next", directory_uid);
    if (rc == -1) {
        log_serror("Failed to save next directory uid in previous one: `%s'", previous->file_uid);
        goto failed_set_next_in_previous;
    }

    rc = safe_save_directory(safe, directory);
    if (rc == -1) {
        log_serror("Failed to save directory: `%s'", directory_uid);
        goto failed_save_directory;
    }

    rc = safe_save_directory(safe, previous);
    if (rc == -1) {
        log_serror("Failed to save directory: `%s'", previous->file_uid);
        goto failed_save_previous_directory;
    }

    strlcpy(out_directory_uid, directory_uid, sizeof(fileuid));

    return 0;
failed_save_previous_directory:
failed_save_directory:
    // FIXME: safe_delete_file(directory_uid);
failed_set_next_in_previous:
failed_add_file:
    directory_destroy(directory);
failed_create_directory:
    return -1;
}

static int
safe_directory_add_file(struct safe *safe, const char *parent_uid, struct bson *file_metadata,
                        fileuid out_directory_uid)
{
    char directory_uid[FILE_UID_LEN];
    char previous_uid[FILE_UID_LEN];
    struct directory *directory = NULL;
    int rc = 0;

    strlcpy(directory_uid, parent_uid, sizeof(directory_uid));
    strlcpy(previous_uid, directory_uid, sizeof(previous_uid));

    do {
        directory_destroy(directory);
        directory = safe_load_directory(safe, directory_uid);
        if (directory == NULL) {
            log_serror("Failed to load directory data: `%s'", directory_uid);
            return -1;
        }

        rc = directory_add_file(directory, file_metadata, directory_uid);
        if (rc == -1) {
            directory_destroy(directory);
            log_serror("Failed to add file to directory: `%s'", directory_uid);
            return -1;
        }

        strlcpy(previous_uid, directory_uid, sizeof(previous_uid));
    } while (rc == 1 && *directory_uid != '\0');

    // No space available in directories file, need to add one more
    if (rc == 1) {
        rc = safe_directory_grow(safe, directory, file_metadata, directory_uid);
        if (rc == -1) {
            directory_destroy(directory);
            log_serror("Failed to grow directory: `%s'", parent_uid);
            return -1;
        }
    } else {
        rc = safe_save_directory(safe, directory);
        if (rc == -1) {
            directory_destroy(directory);
            log_serror("Failed to save directory: `%s'", directory_uid);
            return -1;
        }
    }

    strlcpy(out_directory_uid, directory_uid, sizeof(fileuid));

    directory_destroy(directory);
    return 0;
}
int
safe_prepare_save_data_file(struct safe *safe, const char *parent_uid, const char *filename,
                            char *file_uid)
{
    struct directory *directory = NULL;
    char path[PATH_MAX];
    struct bson *file_metadata = NULL;
    int rc = 0;

    if (strlen(filename) >= MAX_DISPLAY_FILENAME_SIZE) {
        log_serror("filename too long");
        return -1;
    }

    // 1)

    if (*file_uid == '\0') {
        safe_gen_file_uid(file_uid);
        // FIXME: check if not already used
    }

    // 2)

    rc = safe_directory_get_file(safe, parent_uid, file_uid, &file_metadata, &directory);
    directory_destroy(directory);
    if (rc == -1) {
        goto failed_load_parent_dir;
    }

    // 3)

    snprintf(path, sizeof(path), "%s/%s/data/%.2s", SAFES_DIRECTORY, safe->name, file_uid);
    rc = mkdir(path, 0700);
    if (rc == -1) {
        if (errno != EEXIST) {
            log_serror("Failed to mkdir safe data directory `%s': %m", path);
            goto failed_mkdir;
        }
    }

    // 4)

    snprintf(path, sizeof(path), "%s/%s/data/%.2s/%s", SAFES_DIRECTORY, safe->name, file_uid, file_uid);
    rc = safe_encrypt_prepare(safe, path);
    if (rc == -1) {
        log_serror("Failed to safe_encrypt_prepare to save file `%s'", path);
        goto failed_prepare_encrypt;
    }

    strlcpy(safe->state_data.writing.directory_uid, parent_uid, sizeof(safe->state_data.writing.directory_uid));
    strlcpy(safe->state_data.writing.file_uid, file_uid, sizeof(safe->state_data.writing.file_uid));
    strlcpy(safe->state_data.writing.filename, filename, sizeof(safe->state_data.writing.filename));
    safe->state_data.writing.written = 0;

    return 0;
failed_prepare_encrypt:
failed_mkdir:
failed_load_parent_dir:
    return -1;
}

int
safe_continue_save_data(struct safe *safe, char *data, size_t data_size)
{
    int rc = 0;

    if (safe->state != SAFE_STATE_WRITING) {
        log_serror("Invalid safe state, try to write data but state is: %d", safe->state);
        return -1;
    }

    rc = safe_encrypt_write_data(safe, data, data_size);
    if (rc == -1) {
        log_serror("Failed to encrypt data");
        return -1;
    }

    safe->state_data.writing.written += rc;

    return 0;
}

int
safe_add_file_to_directory(struct safe *safe, const char *directory_uid, const char *file_uid, const char *filename,
                           size_t length, fileuid out_directory_uid)
{
    struct directory *directory = NULL;
    struct bson *file_metadata = NULL;
    int rc = 0;

    log_sinfo("Adding file `%s' (%s) to directory `%s'", filename, file_uid, directory_uid);

    rc = safe_directory_get_file(safe, directory_uid, file_uid, &file_metadata, &directory);
    directory_destroy(directory);
    if (rc == -1) {
        goto failed_load_find_directory;
    }

    if (file_metadata == NULL) {
        file_metadata = directory_init_file(file_uid, filename, FILE_TYPE_FILE);
        if (file_metadata == NULL) {
            log_serror("Failed to init file metadata");
            goto failed_add_file_to_folder;
        }
        rc = bson_set_int64(file_metadata, "length", (int64_t) length);
        if (rc == -1) {
            bson_destroy(file_metadata);
            log_serror("Failed to set length to file metadata");
            goto failed_add_file_to_folder;
        }
        rc = safe_directory_add_file(safe, directory_uid, file_metadata, out_directory_uid);
        if (rc == -1) {
            bson_destroy(file_metadata);
            goto failed_add_file_to_folder;
        }
    }

    return 0;

failed_add_file_to_folder:
failed_load_find_directory:
    return -1;
}


int
safe_finish_save_data(struct safe *safe, fileuid out_directory_uid)
{
    int rc = 0;
    fileuid file_uid;
    fileuid directory_uid;
    size_t length = 0;
    char filename[MAX_DISPLAY_FILENAME_SIZE];

    strlcpy(file_uid, safe->state_data.writing.file_uid, sizeof(file_uid));
    strlcpy(directory_uid, safe->state_data.writing.directory_uid, sizeof(directory_uid));
    strlcpy(filename, safe->state_data.writing.filename, sizeof(filename));
    length = safe->state_data.writing.written;

    if (safe->state != SAFE_STATE_WRITING) {
        log_serror("Invalid safe state, try to write data but state is: %d", safe->state);
        return -1;
    }

    rc = safe_encrypt_complete(safe);
    if (rc == -1) {
        log_serror("Failed to complete encrypt");
        return -1;
    }

    if (*file_uid != '\0') {
        rc = safe_add_file_to_directory(safe, directory_uid, file_uid, filename, length, out_directory_uid);
        if (rc == -1) {
            // FIXME: Delete file
            log_serror("Failed to add file `%s' to directory `%s'", file_uid, directory_uid);
            return -1;
        }
    }

    return 0;
}

int
safe_cancel_save_data(struct safe *safe)
{
    int rc = 0;

    if (safe->state != SAFE_STATE_WRITING) {
        log_serror("Invalid safe state, try to write data but state is: %d", safe->state);
        return -1;
    }

    rc = safe_encrypt_cancel(safe);
    if (rc == -1) {
        log_serror("Failed to safe_encrypt_cancel");
        return -1;
    }

    return 0;
}

int
safe_prepare_read_file(struct safe *safe, const fileuid file_uid)
{
    char safe_path[PATH_MAX];
    int rc = 0;

    snprintf(safe_path, sizeof(safe_path), "%s/%s/data/%.2s/%s", SAFES_DIRECTORY, safe->name, file_uid, file_uid);

    rc = safe_decrypt_prepare(safe, safe_path);
    if (rc == -1) {
        log_serror("Failed to safe_decrypt_prepare");
        return -1;
    }

    return 0;
}

int
safe_continue_read_file(struct safe *safe, struct string **out_data, size_t count)
{
    *out_data = safe_decrypt_read_data(safe, count);
    if (*out_data == NULL) {
        log_serror("Failed to safe_decrypt_read_data");
        return -1;
    }

    if (string_length(*out_data) == 0) {
        safe_decrypt_complete(safe);
        return 1;
    }

    return 0;
}

void
safe_cancel_read_file(struct safe *safe)
{
    safe_decrypt_complete(safe);
}

int
safe_wipe_key(struct safe *safe, const fileuid file_uid)
{
    char path[PATH_MAX];
    char wipe_path[PATH_MAX];
    char buffer[128];
    ssize_t written = 0;
    size_t total_written = 0;
    size_t to_write = 0;
    struct stat sb;
    int rc = 0;
    int fd = 0;

    snprintf(path, sizeof(path), "%s/%s/data/%.2s/%s.key", SAFES_DIRECTORY, safe->name, file_uid, file_uid);

    fd = open(path, O_WRONLY);
    if (fd == -1) {
        if (errno == ENOENT) {
            return 0;
        }
        log_error("Failed to open key file `%s' to wipe: %m", wipe_path);
        goto fail_open;
    }

    rc = fstat(fd, &sb);
    if (rc == -1) {
        log_error("Failed to fstat key file: %m");
        goto failed_fstat;
    }

    do {
        to_write = MIN(sb.st_size - total_written, sizeof(buffer));
        arc4random_buf(buffer, to_write);

        written = write(fd, buffer, to_write);
        if (written == -1) {
            log_error("Failed to fstat key file: %m");
            goto failed_write;
        }

        total_written += written;
    }
    while(total_written < sb.st_size);

    close(fd);

    rc = unlink(path);
    if (rc == -1) {
        log_error("Failed to unlink key file `%s':  %m", wipe_path);
        return -1;
    }

    return 0;
failed_write:
failed_fstat:
    close(fd);
fail_open:
    return -1;
}

int
safe_delete_file(struct safe *safe, const fileuid directory_uid, const fileuid file_uid) {
    struct directory *directory = NULL;
    struct bson *file_metadata = NULL;
    int rc = 0;
    int32_t file_type = 0;
    char path[PATH_MAX];

    if (safe->state != SAFE_STATE_NONE) {
        log_serror("Invalid safe state, try to write data but state is: %d", safe->state);
        return -1;
    }

    rc = safe_directory_get_file(safe, directory_uid, file_uid, &file_metadata, &directory);
    if (rc == -1) {
        directory_destroy(directory);
        log_serror("Failed to get file `%s' from directory: `%s'", file_uid, directory_uid);
        return -1;
    }

    if (file_metadata == NULL) {
        log_swarning("File `%s' does not exists in directory `%s'", file_uid, directory_uid);
        return 0;
    }

    bson_get_int32(file_metadata, "type", &file_type);
    if (file_type != FILE_TYPE_FILE) {
        directory_destroy(directory);
        bson_destroy(file_metadata);
        log_serror("Invalid file type `%s': `%d'", file_uid, file_type);
        return -1;
    }

    rc = directory_remove_file(directory, file_metadata);
    if (rc == -1) {
        directory_destroy(directory);
        bson_destroy(file_metadata);
        log_serror("Failed to remove file `%s' from directory `%s'", file_uid, directory_uid);
        return -1;
    }

    bson_destroy(file_metadata);

    rc = safe_wipe_key(safe, file_uid);
    if (rc == -1) {
        log_serror("Failed to safe_wipe_key `%s'", file_uid);
        directory_destroy(directory);
        return -1;
    }

    rc = safe_save_directory(safe, directory);
    directory_destroy(directory);
    if (rc == -1) {
        log_serror("Failed to save directory `%s'", directory_uid);
        return -1;
    }

    snprintf(path, sizeof(path), "%s/%s/data/%.2s/%s.enc", SAFES_DIRECTORY, safe->name, file_uid, file_uid);
    rc = unlink(path);
    if (rc == -1 && errno != ENOENT) {
        log_serror("Failed to unlink file '%s`: %m", path);
        return -1;
    }

    log_sinfo("Successfully delete file `%s'", file_uid);

    return rc;
}

static void
safe_generate_token(struct safe *safe, share_token out_token, struct dictionary *safes_by_token)
{
    uint8_t rand[SHARE_KEY_TOKEN_BYTES_LEN];

    do {
        arc4random_buf(rand, sizeof(rand));
        for (size_t i = 0; i < sizeof(rand); i++) {
            snprintf(out_token + (i * 2), 3, "%0.2x", rand[i]);
        }
    } while (safe_token_contains(safe, out_token) || dictionary_get(safes_by_token, out_token) != NULL);
}


int
safe_token_add(struct safe *safe, enum safe_mode mode, const share_token token, struct dictionary *safes_by_token,
               time_t expire)
{
    int rc = 0;
    size_t max_tokens = sizeof(safe->tokens) / sizeof(*safe->tokens);

    if (safe->token_count == max_tokens) {
        safe_token_remove(safe, safe->tokens[0].token, safes_by_token);
    }

    safe->tokens[safe->token_count].mode = mode;
    safe->tokens[safe->token_count].expire = expire;
    strlcpy(safe->tokens[safe->token_count].token, token, sizeof(safe->tokens[safe->token_count].token));

    safe->token_count++;

    rc = dictionary_put(safes_by_token, token, safe);
    if (rc == -1) {
        log_serror("Failed to add token to safes_by_token");
        safe->token_count--;
        return -1;
    }

    return 0;
}

void
safe_token_remove(struct safe *safe, const share_token token, struct dictionary *safes_by_token) {
    share_token deleted_token;

    strlcpy(deleted_token, token, sizeof(deleted_token));

    for (size_t i = 0; i < safe->token_count; i++) {
        if (strncmp(safe->tokens[i].token, token, sizeof(safe->tokens[i].token)) == 0) {
            memmove(safe->tokens + i, safe->tokens + i + 1, sizeof(*safe->tokens) * (safe->token_count - i - 1));
            safe->token_count--;
            break;
        }
    }
    dictionary_remove(safes_by_token, deleted_token);
}

enum safe_mode
safe_token_contains(const struct safe *safe, const share_token token)
{
    for (size_t i = 0; i < safe->token_count; i++) {
        if (strncmp(safe->tokens[i].token, token, sizeof(safe->tokens[i].token)) == 0) {
            return safe->tokens[i].mode;
        }
    }
    return SAFE_MODE_INIT;
}

int
safe_share_key(struct safe *safe, enum safe_mode mode, share_token out_token, struct dictionary *safes_by_token,
               time_t expire)
{
    int rc = 0;

    safe_generate_token(safe, out_token, safes_by_token);

    rc = safe_token_add(safe, mode, out_token, safes_by_token, expire);
    if (rc == -1) {
        return -1;
    }

    return 0;
}

void
safe_token_remove_all(struct safe *safe, struct dictionary *safes_by_token)
{
    for (size_t i = 0; i < safe->token_count; i++) {
        dictionary_remove(safes_by_token, safe->tokens[i].token);
    }
    safe->token_count = 0;
}


#include <snow/snow.h>
#include <openssl/evp.h>

describe(safe, {
    safe_name name = "test_safe";
    safe_name fake_name = "fake";
    const char *passphrase = "abcdef";
    const char *bad_passphrase = "abcdefgg";
    it("init libssl", {
        OpenSSL_add_all_algorithms();
    });
    subdesc(create_safe, {
        it("Create a new safe", {
            struct safe *safe = NULL;
            safe = safe_create_new(name, passphrase);
            assertneq(safe, NULL);
            safe_destroy(safe);
        });
    });
    subdesc(exists, {
        it("Check if safe exists", {
            asserteq(safe_exists(name), true);
        });
        it("Check if safe does not exists", {
            asserteq(safe_exists(fake_name), false);
        });
    });
    subdesc(load_safe, {
        it("Load a safe using primary passphrase", {
            struct safe *safe = NULL;
            safe = safe_load(name, passphrase);
            assertneq(safe, NULL);
            safe_destroy(safe);
        });
        it("Failed to load a safe using a bad passphrase", {
            struct safe *safe = NULL;
            safe = safe_load(name, bad_passphrase);
            asserteq(safe, NULL);
        });
    });
    subdesc(add_file, {
        it("Add a new file in safe, in root directory", {
            struct safe *safe = NULL;
            struct string *root_uid = NULL;
            char path_enc[PATH_MAX];
            char path_key[PATH_MAX];
            struct stat sb;
            struct bson *file_metadata = NULL;
            struct directory *directory = NULL;
            fileuid file_uid;
            bzero(file_uid, sizeof(file_uid));

            safe = safe_load(name, passphrase);
            assertneq(safe, NULL);
            asserteq(bson_get_string(safe->conf, "root", &root_uid), true);

            asserteq(safe_prepare_save_data_file(safe, string_data(root_uid), "test.txt", file_uid), 0);
            asserteq(safe_continue_save_data(safe, "Hello World !", 13), 0);
            asserteq(safe_finish_save_data(safe, string_data(root_uid)), 0);

            snprintf(path_enc, sizeof(path_enc), "%s/%s/data/%.2s/%s.enc", SAFES_DIRECTORY, name, file_uid, file_uid);
            asserteq(stat(path_enc, &sb), 0);
            snprintf(path_key, sizeof(path_key), "%s/%s/data/%.2s/%s.key", SAFES_DIRECTORY, name, file_uid, file_uid);
            asserteq(stat(path_key, &sb), 0);

            asserteq(safe_directory_get_file(safe, string_data(root_uid), file_uid, &file_metadata, &directory), 0);
            assertneq(file_metadata, NULL);
            assertneq(directory, NULL);
            directory_destroy(directory);

            safe_destroy(safe);
        });
    });
    subdesc(share_key, {
        it("Share safe key with another safe, without reusing passphrase", {
            share_token token;
            share_token invalid_token;
            time_t expire = 42;
            struct safe *safe = NULL;
            struct safe *safe2 = NULL;
            struct dictionary *safes_by_token = NULL;
            bzero(token, sizeof(token));
            bzero(invalid_token, sizeof(invalid_token));
            safes_by_token = dictionary_init();
            assertneq(safes_by_token, NULL);

            safe = safe_load(name, passphrase);
            assertneq(safe, NULL);

            asserteq(safe_share_key(safe, SAFE_MODE_UPLOAD, token, safes_by_token, expire), 0);
            asserteq(safe->token_count, 1);
            asserteq(safe->key->ref_cnt, 1);
            assertneq(dictionary_get(safes_by_token, token), NULL);

            safe_generate_token(safe, invalid_token, safes_by_token);

            safe2 = safe_load_with_share_token(safes_by_token, invalid_token);
            asserteq(safe2, NULL);

            safe2 = safe_load_with_share_token(safes_by_token, token);
            assertneq(safe2, NULL);
            asserteq(safe->token_count, 0);
            assertneq(safe->key, NULL);
            assertneq(safe2->key, NULL);
            asserteq(safe2->key->ref_cnt, 2);
            asserteq(safe->key, safe2->key);
            asserteq(dictionary_get(safes_by_token, token), NULL);

            safe_destroy(safe);
            asserteq(safe2->key->ref_cnt, 1);

            safe_destroy(safe2);
        });
    it("Generate more than maximum share key available and check if only the last ones are still valid", {
            share_token token;
            time_t expire = 42;
            struct safe *safe = NULL;
            struct dictionary *safes_by_token = NULL;
            bzero(token, sizeof(token));

            safes_by_token = dictionary_init();
            assertneq(safes_by_token, NULL);

            safe = safe_load(name, passphrase);
            assertneq(safe, NULL);

            share_token tokens[MAX_SHARE_TOKENS * 2];

            size_t max_tokens = sizeof(safe->tokens) / sizeof(*safe->tokens);
            for (size_t i = 0; i < max_tokens * 2; i++) {
                asserteq(safe_share_key(safe, SAFE_MODE_UPLOAD, token, safes_by_token, expire), 0);
                asserteq(safe->token_count, MIN(max_tokens, i + 1));
                assertneq(dictionary_get(safes_by_token, token), NULL);
                strlcpy(tokens[i], token, sizeof(tokens[i]));
            }
            for (size_t i = 0; i < max_tokens; i++) {
                asserteq(dictionary_get(safes_by_token, tokens[i]), NULL);
                asserteq(safe_token_contains(safe, tokens[i]), 0);
            }
            for (size_t i = max_tokens; i < max_tokens * 2; i++) {
                assertneq(dictionary_get(safes_by_token, tokens[i]), NULL);
                assertneq(safe_token_contains(safe, tokens[i]), 0);
            }
            asserteq(safe->token_count, max_tokens);

            safe_destroy(safe);
        });
        it("Generate multiple share key and then destroy safe to check all key are removed from dictionary", {
            share_token token;
            time_t expire = 42;
            struct safe *safe = NULL;
            struct dictionary *safes_by_token = NULL;
            bzero(token, sizeof(token));

            safes_by_token = dictionary_init();
            assertneq(safes_by_token, NULL);

            safe = safe_load(name, passphrase);
            assertneq(safe, NULL);

            share_token tokens[MAX_SHARE_TOKENS];

            size_t max_tokens = sizeof(safe->tokens) / sizeof(*safe->tokens);
            for (size_t i = 0; i < max_tokens; i++) {
                asserteq(safe_share_key(safe, SAFE_MODE_UPLOAD, token, safes_by_token, expire), 0);
                asserteq(safe->token_count, MIN(max_tokens, i + 1));
                assertneq(dictionary_get(safes_by_token, token), NULL);
                strlcpy(tokens[i], token, sizeof(tokens[i]));
            }

            asserteq(safe->token_count, max_tokens);
            for (size_t i = 0; i < max_tokens; i++) {
                assertneq(dictionary_get(safes_by_token, tokens[i]), NULL);
                assertneq(safe_token_contains(safe, tokens[i]), 0);
            }

            safe_token_remove_all(safe, safes_by_token);
            asserteq(safe->token_count, 0);
            for (size_t i = 0; i < max_tokens; i++) {
                asserteq(dictionary_get(safes_by_token, tokens[i]), NULL);
                asserteq(safe_token_contains(safe, tokens[i]), 0);
            }

            safe_destroy(safe);
        });
    });
    subdesc(delete_safe, {
        it("Delete a safe", {
            int rc = 0;
            struct safe *safe = NULL;
            safe = safe_init(name);
            assertneq(safe, NULL);
            asserteq(safe_exists(name), true);
            rc = safe_delete(safe);
            asserteq(safe_exists(name), false);
            asserteq(rc, 0);
            safe_destroy(safe);
        });
    });
});
