#ifndef COFFRE_CLIENT_H
#define COFFRE_CLIENT_H

#include <event.h>

#include "core/msg_protocol.h"
#include "websocket.h"
#include "utils/ring_buffer.h"
#include "utils/dictionary.h"
#include "utils/string.h"
#include "utils/bson.h"

struct server;
struct client {
    clientid client_id;
    enum safe_mode mode;
    int fd;
    int eof;
    int close;
    struct tls *tls_ctx;
    struct server *server;
    int tls_pollin;
    int tls_pollout;
    struct event ev_read;
    struct event ev_write;
    struct ring_buf *read_buf;
    struct ring_buf *write_buf;
    struct websocket_client_data *ws;
    bool authentified;
    size_t max_frame_without_privsep_response;
    size_t frame_without_privsep_response_count;
};

void client_init_id(struct client *);
int client_init(struct client *, int);
void on_tls_client_event(int, short, void *);
ssize_t client_send_data(struct client *, char *, size_t);
int client_send_frame(struct client *, enum ws_opcode, struct string *, ws_frame_sent_callback, operationid);
int client_send_bson_frame(struct client *, struct bson *);
int client_send_bson_frame_with_cb(struct client *, struct bson *, ws_frame_sent_callback, operationid);
int client_process_packet(struct client *, struct bson *);

#endif //COFFRE_CLIENT_H
