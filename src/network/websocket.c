#include "websocket.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <resolv.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <openssl/sha.h>
#include <limits.h>
#include <endian.h>

#include "utils/log.h"
#include "utils/bson.h"
#include "client.h"

struct websocket_client_data *
ws_init()
{
    struct websocket_client_data *ws;

    ws = calloc(1, sizeof(*ws));
    if (ws == NULL) {
        log_error("Failed to calloc ws structure: %m");
        return NULL;
    }

    ws->headers = dictionary_init();

    return ws;
}

void
ws_destroy(struct websocket_client_data *ws)
{
    if (ws == NULL) {
        return;
    }
    dictionary_destroy(ws->headers);
    string_destroy(ws->state_data.request.cur_line);
    string_destroy(ws->state_data.response.response);
    string_destroy(ws->path);
}

void
ws_set_state_response(struct websocket_client_data *ws, enum client_state state, int code, const char *str,
                      const char *err)
{
    struct ws_response_state_data *response_data = &ws->state_data.response;
    ws->state = state;
    response_data->status_code = code;
    response_data->status_str = str;
    response_data->error_msg = err;
}

static int
ws_parse_http_request_line(struct websocket_client_data *ws, struct string *line, struct string **path)
{
    struct string **splitted = NULL;
    ssize_t count = string_split(line, ' ', &splitted, 4);
    if (count == -1) {
        log_error("Error while splitting string `%s' on ' ' ", string_data(line));
        return -1;
    }

    if (count != 3) {
        goto invalid_request;
    }
    if (!string_eq_cstr(splitted[0], "GET")) {
        goto invalid_request;
    }
    if (!string_eq_cstr(splitted[2], "HTTP/1.1")) {
        goto invalid_request;
    }

    string_destroy(splitted[0]);
    *path = splitted[1];
    string_destroy(splitted[2]);
    return 0;
invalid_request:
    ws_set_state_response(ws, RESPONSE_HANDSHAKE, 400, "Bad Request", "HTTP protocol error");
    for (size_t i = 0; i < count; i++) {
        string_destroy(splitted[i]);
    }
    return 0;
}

int
ws_read_handshake_request(struct websocket_client_data *ws, struct ring_buf *buf)
{
    struct ws_request_state_data *request_data = &ws->state_data.request;
    char *buffer = NULL;
    struct string *cur_line = NULL;
    ssize_t eol_index = 0;
    size_t read_count = 0;
    int rc = 0;

    cur_line = request_data->cur_line;
    do {
        if (cur_line == NULL) {
            cur_line = string_init();
            if (cur_line == NULL) {
                log_error("Failed to init string");
                return -1;
            }
        }
        eol_index = ringbuf_indexof(buf, '\n');
        if (eol_index == -1) {
            read_count = ringbuf_available_data(buf);
        } else {
            read_count = (size_t) (eol_index + 1);
        }
        read_count = ringbuf_get_buffer_for_reading(buf, &buffer, read_count);
        rc = string_nappend(cur_line, buffer, read_count);
        if (rc == -1) {
            return -1;
        }

        if (string_length(cur_line) > MAX_HEADER_LINE_SIZE) {
            log_error("Invalid client header, line too long");
            return -1;
        }

        if (eol_index != -1) {
            string_chomp(cur_line);
            rc = ws_parse_handshake_header(ws, cur_line);
            if (rc == -1) {
                return -1;
            }
            cur_line = NULL;
        }
    } while (eol_index != -1 || ringbuf_available_data(buf) > 0);

    request_data->cur_line = cur_line;

    return 0;
}

static struct string *
ws_frame_header(enum ws_opcode opcode, size_t length, int fin)
{
    struct string *header = string_init();
    if (header == NULL) {
        return NULL;
    }

    uint8_t op = 0;
    uint8_t len = 0;

    if (fin) {
        op |= 0x80;
    }
    op |= opcode;

    if (length > USHRT_MAX) {
        len = 127;
    } else if (length > 125) {
        len = 126;
    } else{
        len = (uint8_t) length;
    }

    string_nappend(header, (const char *) &op, 1);
    string_nappend(header, (const char *) &len, 1);
    if (len == 126) {
        uint16_t len_u16 = (uint16_t) htons(length);
        string_nappend(header, (const char *) &len_u16, 2);
    }
    if (len == 127) {
        uint64_t len_u64 = be64toh(length);
        string_nappend(header, (const char *) &len_u64, 8);
    }

    return header;
}

/**
 *
 * @param ws
 * @param client
 * @return -1 on error
 *         0 if frame was not entirely processed
 *         1 if it can process the next one
 *         2 if it can process the next one, but do not free payload
 */
static int
ws_process_frame(struct websocket_client_data *ws, struct client *client)
{
    struct ws_communicating_state_data *communicating_data = &ws->state_data.communicating;
    struct ws_frame *frame = &communicating_data->rcv_frame;
    struct string *payload = frame->payload;
    struct bson *bson;
    char *clientid = client->client_id;
    int rc = 0;

    log_cdebug("ws frame received: op: %d size: %zu", frame->opcode, string_length(frame->payload));

    switch (frame->opcode) {
        case WS_OP_BINARY: {
            if (client->frame_without_privsep_response_count >= client->max_frame_without_privsep_response) {
                log_cwarning("client sent data while he was supposed to wait response");
                return -1;
            }

            bson = bson_decode(string_data(frame->payload), string_length(frame->payload));
            if (bson == NULL) {
                return -1;
            }

            rc = client_process_packet(client, bson);
            bson_destroy(bson);
            if (rc == -1) {
                return -1;
            }

            break;
        }
        case WS_OP_CONTINUE: {
            log_cerror("Not implemented frame opcode: WS_OP_CONTINUE");
            return -1;
        }
        case WS_OP_PING: {
            client_send_frame(client, WS_OP_PONG, payload, NULL, NO_OPERATION_ID);
            return 2;
        }
        case WS_OP_CLOSE:
            ws->state = END;
            break;
        default:
            log_cerror("Not supported frame opcode: %d", frame->opcode);
            return -1;
    }

    frame->process_count++;

    return 1;
}

int
ws_read(struct websocket_client_data *ws, struct ring_buf *buf, struct client *client)
{
    struct ws_communicating_state_data *com_data = &ws->state_data.communicating;
    char *clientid = client->client_id;
    int parsing = 1;
    int rc = 0;

    do {
        switch (com_data->frame_state) {
            case WS_FRAME_STATE_HEADER: {
                if (ringbuf_available_data(buf) >= 1) {
                    char op;
                    ringbuf_read(buf, &op, 1);
                    com_data->rcv_frame.fin = (uint8_t) (op & 0x80);
                    com_data->rcv_frame.opcode = (uint8_t) (op & 0x7f);
                    com_data->frame_state = WS_FRAME_STATE_SIZE;
                } else {
                    parsing = 0;
                }
                break;
            }
            case WS_FRAME_STATE_SIZE: {
                if (com_data->rcv_frame.payload_length == 0) {
                    if (ringbuf_available_data(buf) >= 1) {
                        char op;
                        ringbuf_read(buf, &op, 1);
                        com_data->rcv_frame.use_mask = (uint8_t) (op & 0x80);
                        com_data->rcv_frame.payload_length = (uint8_t) (op & 0x7f);
                        if (com_data->rcv_frame.payload_length < 126) {
                            com_data->frame_state = WS_FRAME_STATE_MASK;
                        }
                    } else {
                        parsing = 0;
                    }
                } else {
                    if (com_data->rcv_frame.payload_length == 126) {
                        if (ringbuf_available_data(buf) >= sizeof(uint16_t)) {
                            uint16_t size;
                            ringbuf_read(buf, (char *) &size, sizeof(size));
                            com_data->rcv_frame.payload_length = ntohs(size);

                            com_data->frame_state = WS_FRAME_STATE_MASK;
                        } else {
                            parsing = 0;
                        }
                    } else if (com_data->rcv_frame.payload_length == 127) {
                        if (ringbuf_available_data(buf) >= sizeof(uint64_t)) {
                            uint64_t size;
                            ringbuf_read(buf, (char *) &size, sizeof(size));
                            com_data->rcv_frame.payload_length = be64toh(size);
                            com_data->frame_state = WS_FRAME_STATE_MASK;
                        } else {
                            parsing = 0;
                        }
                    }
                }
                if (com_data->rcv_frame.payload_length + com_data->rcv_previous_frames_len > MAX_FRAME_SIZE) {
                    string_destroy(com_data->rcv_frame.payload);
                    log_cwarning("Client try to send frame too big");
                    return -1;
                }
                break;
            }
            case WS_FRAME_STATE_MASK: {
                if (com_data->rcv_frame.use_mask) {
                    if (ringbuf_available_data(buf) >= sizeof(uint32_t)) {
                        uint32_t mask;
                        ringbuf_read(buf, (char *) &mask, sizeof(mask));
                        com_data->rcv_frame.mask = mask;
                        com_data->frame_state = WS_FRAME_STATE_PAYLOAD;
                    } else {
                        parsing = 0;
                    }
                } else {
                    com_data->frame_state = WS_FRAME_STATE_PAYLOAD;
                }
                break;
            }
            case WS_FRAME_STATE_PAYLOAD: {
                char *buffer = NULL;
                size_t read_count = 0;
                struct string *payload = com_data->rcv_frame.payload;
                if (payload == NULL) {
                    payload = string_init();
                    com_data->rcv_frame.payload = payload;
                }

                read_count = ringbuf_get_buffer_for_reading(buf, &buffer, com_data->rcv_frame.payload_length -
                        (string_length(payload) - com_data->rcv_previous_frames_len));
                rc = string_nappend(payload, buffer, read_count);
                if (rc == -1) {
                    log_cerror("Error while reading payload");
                    return -1;
                }

                if ((string_length(payload) - com_data->rcv_previous_frames_len) == com_data->rcv_frame.payload_length) {
                    com_data->frame_state = WS_FRAME_STATE_READY;
                } else {
                    parsing = 0;
                }
                break;
            }
            case WS_FRAME_STATE_READY: {
                if (com_data->rcv_frame.use_mask) {
                    com_data->rcv_frame.use_mask = 0;
                    uint8_t *data = (uint8_t *) string_data(com_data->rcv_frame.payload);
                    uint8_t *mask = (uint8_t *) &com_data->rcv_frame.mask;
                    for (size_t i = 0; i < com_data->rcv_frame.payload_length; i++) {
                        data[i + com_data->rcv_previous_frames_len] = data[i + com_data->rcv_previous_frames_len] ^ mask[i % 4];
                    }
                }

                if (com_data->rcv_frame.fin) {
                    if (com_data->rcv_previous_frames_opcode != 0) {
                        com_data->rcv_frame.opcode = com_data->rcv_previous_frames_opcode;
                    }
                    rc = ws_process_frame(ws, client);
                    if (rc == -1) {
                        log_cerror("Failed to process ws frame");
                        return -1;
                    }
                    if (rc > 0) {
                        if (rc == 1) {
                            string_destroy(com_data->rcv_frame.payload);
                        }
                        bzero(&com_data->rcv_frame, sizeof(com_data->rcv_frame));
                        com_data->frame_state = WS_FRAME_STATE_HEADER;
                    } else {
                        parsing = 0;
                    }
                    com_data->rcv_previous_frames_len = 0;
                    com_data->rcv_previous_frames_opcode = 0;
                } else {
                    struct string *payload = com_data->rcv_frame.payload;
                    if (com_data->rcv_previous_frames_opcode == 0) {
                        com_data->rcv_previous_frames_opcode = com_data->rcv_frame.opcode;
                    }
                    com_data->frame_state = WS_FRAME_STATE_HEADER;
                    com_data->rcv_previous_frames_len = string_length(payload);
                    bzero(&com_data->rcv_frame, sizeof(com_data->rcv_frame));
                    com_data->rcv_frame.payload = payload;
                }
                break;
            }
        }
    } while (parsing);

    return 0;
}

int
ws_write(struct websocket_client_data *ws, struct ring_buf *buf, struct client *client)
{
    struct ws_communicating_state_data *com_data = &ws->state_data.communicating;
    struct string *header = NULL;
    struct string *payload = NULL;
    size_t written = 0;
    ssize_t sent = 0;

    if (com_data->snd_pending_count == 0) {
        return 0;
    }

    do {
        header = com_data->snd_pending_frames[com_data->snd_pending_pos].header;
        payload = com_data->snd_pending_frames[com_data->snd_pending_pos].payload;
        written = com_data->snd_pending_frames[com_data->snd_pending_pos].written;

        if (header != NULL) {
            sent = client_send_data(client, string_data(header) + written, string_length(header) - written);
            if (sent == -1) {
                return -1;
            }
            written += sent;
            if (written == string_length(header)) {
                string_destroy(header);
                com_data->snd_pending_frames[com_data->snd_pending_pos].header = NULL;
                written = 0;
            }
            com_data->snd_pending_frames[com_data->snd_pending_pos].written = written;
        }

        if (header == NULL) {
            sent = client_send_data(client, string_data(payload) + written, string_length(payload) - written);
            if (sent == -1) {
                return -1;
            }
            written += sent;
            if (written == string_length(payload)) {
                string_destroy(payload);
                com_data->snd_pending_frames[com_data->snd_pending_pos].payload = NULL;
                if (com_data->snd_pending_frames[com_data->snd_pending_pos].sent_callback != NULL) {
                    operationid operation_id = com_data->snd_pending_frames[com_data->snd_pending_pos].operation_id;
                    com_data->snd_pending_frames[com_data->snd_pending_pos].sent_callback(client, operation_id);
                }
                com_data->snd_pending_pos = (com_data->snd_pending_pos + 1) % MAX_SND_PENDING_FRAME;
                com_data->snd_pending_count--;
                written = 0;
            }
            com_data->snd_pending_frames[com_data->snd_pending_pos].written = written;
        }

    } while (written == 0 && com_data->snd_pending_count);

    return 0;
}

int
ws_parse_handshake_header(struct websocket_client_data *ws, struct string *line)
{
    int rc = 0;

    if (ws->path == NULL) {
        if (ws_parse_http_request_line(ws, line, &ws->path) == -1) {
            log_error("Failed to parse request line: `%s'", string_data(line));
            return -1;
        }
    } else {
        struct string **splitted = NULL;
        ssize_t count = 0;

        if (string_eq_cstr(line, "")) {
            struct string *client_sec_key = dictionary_get(ws->headers, WS_HEADER_SEC_WEBSOCKET_KEY);
            if (client_sec_key == NULL) {
                ws_set_state_response(ws, RESPONSE_HANDSHAKE, 400, "Bad Request",
                                      "Missing header: " WS_HEADER_SEC_WEBSOCKET_KEY);
                return 0;
            }
            struct string *client_sec_version = dictionary_get(ws->headers, WS_HEADER_SEC_WEBSOCKET_VERSION);
            if (client_sec_version == NULL) {
                ws_set_state_response(ws, RESPONSE_HANDSHAKE, 400, "Bad Request",
                                      "Missing header: " WS_HEADER_SEC_WEBSOCKET_VERSION);
                return 0;
            }
            if (!string_eq_cstr(client_sec_version, "13")) {
                log_warning("Invalid version: %s", string_data(client_sec_version));
                ws_set_state_response(ws, RESPONSE_HANDSHAKE, 400, "Bad Request", "Bad version");
                return 0;
            }
            ws_set_state_response(ws, RESPONSE_HANDSHAKE, 101, "Switching Protocols", NULL);
            return 0;
        }

        count = string_split(line, ':', &splitted, 2);
        if (count == -1) {
            return -1;
        }
        if (count < 2) {
            log_warning("Invalid http header: `%s'", string_data(line));
            ws_set_state_response(ws, RESPONSE_HANDSHAKE, 400, "Bad Request", "Bad header");
            string_destroy(splitted[0]);
            return 0;
        }

        string_trim_end(splitted[0], " ");
        string_trim_start(splitted[1], " ");

        rc = dictionary_put(ws->headers, string_data(splitted[0]), splitted[1]);
        if (rc == -1) {
            return -1;
        }
        return 0;
    }

    return 0;
}

struct string *
ws_http_response(struct websocket_client_data *ws)
{
    struct ws_response_state_data *response_data = &ws->state_data.response;
    char buffer[256];
    char base64_key_buf[32];
    struct string *str;
    int rc;


    str = string_init();
    if (str == NULL) {
        return str;
    }

    rc = snprintf(buffer, sizeof(buffer), "HTTP/1.1 %d %s\r\n", response_data->status_code, response_data->status_str);
    if (rc >= sizeof(buffer)) {
        log_error("snprintf failed for request line");
        goto error;
    }
    rc = string_append(str, buffer);
    if (rc == -1) {
        goto error;
    }
    rc = string_append(str, "Upgrade: websocket\r\n");
    if (rc == -1) {
        goto error;
    }
    rc = string_append(str, "Connection: Upgrade\r\n");
    if (rc == -1) {
        goto error;
    }
    if (response_data->status_code == 101) {
        struct string *client_sec_key = dictionary_get(ws->headers, WS_HEADER_SEC_WEBSOCKET_KEY);
        if (client_sec_key == NULL) {
            goto error;
        }

        u_int8_t digest[SHA_DIGEST_LENGTH];

        SHA_CTX sha1;
        SHA1_Init(&sha1);

        string_append(client_sec_key, WS_MAGIC_KEY);

        SHA1_Update(&sha1, (const u_int8_t *) string_data(client_sec_key), string_length(client_sec_key));
        SHA1_Final(digest, &sha1);

        bzero(base64_key_buf, sizeof(base64_key_buf));
        b64_ntop((const u_char *)digest, SHA_DIGEST_LENGTH, base64_key_buf, sizeof(base64_key_buf));
        snprintf(buffer, sizeof(buffer), "Sec-WebSocket-Accept: %s\r\n", base64_key_buf);
        rc = string_append(str, buffer);
        if (rc == -1) {
            goto error;
        }
    } else {
        rc = string_append(str, "Sec-WebSocket-Version: 13\r\n");
        if (rc == -1) {
            goto error;
        }
    }

    if (response_data->error_msg != NULL) {
        rc = snprintf(buffer, sizeof(buffer), "Content-Length: %zu\r\n", strlen(response_data->error_msg) + 2);
        if (rc >= sizeof(buffer)) {
            log_error("snprintf failed for request line");
            goto error;
        }

        rc = string_append(str, buffer);
        if (rc == -1) {
            goto error;
        }
    }

    rc = string_append(str, "\r\n");
    if (rc == -1) {
        goto error;
    }

    if (response_data->error_msg != NULL) {
        rc = string_append(str, response_data->error_msg);
        if (rc == -1) {
            goto error;
        }

        rc = string_append(str, "\r\n");
        if (rc == -1) {
            goto error;
        }
    }

    return str;
error:
    string_destroy(str);
    return NULL;
}


int ws_send_frame(struct websocket_client_data *ws, enum ws_opcode opcode, struct string *payload, ws_frame_sent_callback sent_callback, operationid operation_id)
{
    struct ws_communicating_state_data *com_data = &ws->state_data.communicating;
    struct string *header = NULL;

    if (com_data->snd_pending_count >= MAX_SND_PENDING_FRAME) {
        log_error("Too many sending frame pending: %zu, cannot send frame", com_data->snd_pending_count);
        return -1;
    }
    if (com_data->snd_pending_count > 2) {
        log_warning("Too many sending frame pending, only one at the time + potential pong should be pendings");
    }

    header = ws_frame_header(opcode, string_length(payload), 1);
    if (header == NULL) {
        string_destroy(payload);
        return -1;
    }

    size_t i = (com_data->snd_pending_pos + com_data->snd_pending_count++) % MAX_SND_PENDING_FRAME;
    com_data->snd_pending_frames[i].header = header;
    com_data->snd_pending_frames[i].payload = payload;
    com_data->snd_pending_frames[i].written = 0;
    com_data->snd_pending_frames[i].sent_callback = sent_callback;
    com_data->snd_pending_frames[i].operation_id = operation_id;

    return 0;
}
