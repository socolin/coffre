#include <unistd.h>
#include <stdlib.h>
#include <iso646.h>
#include <tls.h>
#include <stdio.h>
#include <string.h>
#include <event.h>

#include "compat/linux.h"
#include "utils/bson.h"
#include "core/msg_queue.h"
#include "core/msg_protocol.h"
#include "utils/log.h"
#include "client.h"
#include "network.h"
#include "websocket.h"

#ifdef DEBUG
#define BUF_SIZE 16
#else
#define BUF_SIZE 16384
#endif

void
client_init_id(struct client *client)
{
    uint8_t rand[CLIENT_ID_BYTES_LEN];

    arc4random_buf(rand, sizeof(rand));
    for (size_t i = 0; i < sizeof(rand); i++) {
        snprintf(client->client_id + (i * 2), 3, "%0.2x", rand[i]);
    }
}

int
client_init(struct client *client, int fd)
{
    client->fd = fd;
    client->read_buf = ringbuf_init(BUF_SIZE);
    if (client->read_buf == NULL) {
        goto error;
    }
    client->write_buf = ringbuf_init(BUF_SIZE);
    if (client->write_buf == NULL) {
        goto error;
    }
    client->ws = ws_init();
    if (client->ws == NULL) {
        goto error;
    }
    client->max_frame_without_privsep_response = 1;

    return 0;
error:
    ringbuf_destroy(client->read_buf);
    ringbuf_destroy(client->write_buf);
    ws_destroy(client->ws);
    return -1;
}

void
client_destroy(struct client *client)
{
    if (client == NULL) {
        return;
    }
    ringbuf_destroy(client->read_buf);
    ringbuf_destroy(client->write_buf);
    ws_destroy(client->ws);
    freezero(client, sizeof(*client));
}

int
client_flush_write_buffer(struct client *client)
{
    char *buffer = NULL;
    ssize_t written = 0;
    size_t to_write = 0;

    do {
        to_write = 0;
        if (!client->tls_pollout) {
            to_write = ringbuf_get_buffer_for_reading(client->write_buf, &buffer, 0);
        }

        if (to_write == 0) {
            break;
        }

        written = tls_write(client->tls_ctx, buffer, to_write);
        if (written == -1) {
            log_error("tls_write failed to write %zu bytes: %s", to_write, tls_error(client->tls_ctx));
            return -1;
        } else if (written == TLS_WANT_POLLIN) {
            client->tls_pollin = 1;
            ringbuf_rewind_read(client->write_buf, to_write);
            break;
        } else if (written == TLS_WANT_POLLOUT) {
            client->tls_pollout = 1;
            ringbuf_rewind_read(client->write_buf, to_write);
            break;
        } else {
            if (written < to_write) {
                ringbuf_rewind_read(client->write_buf, to_write - written);
            }
        }
    } while (true);

    return 0;
}

ssize_t
client_send_data(struct client *client, char *data, size_t len)
{
    ssize_t rc = 0;

    ssize_t written = 0;
    do {
        written += ringbuf_write(client->write_buf, data + written, len - written);
        rc = client_flush_write_buffer(client);
        if (rc == -1) {
            return -1;
        }
        if (ringbuf_available_space(client->write_buf) == 0) {
            break;
        }
    } while (len - written > 0 && !client->tls_pollout);

    return written;
}

int
client_do_work(struct client *client)
{
    int rc = 0;

    if (client->ws->state == READ_HANDSHAKE_REQUEST) {
        rc = ws_read_handshake_request(client->ws, client->read_buf);
        if (rc == -1) {
            ws_set_state_response(client->ws, RESPONSE_HANDSHAKE, 500, "Internal server error", NULL);
        }
    }

    if (client->ws->state == RESPONSE_HANDSHAKE) {
        struct ws_response_state_data *response_data = &client->ws->state_data.response;
        struct string *response = response_data->response;
        if (response == NULL) {
            response = ws_http_response(client->ws);
            if (response == NULL) {
                return -1;
            }
            response_data->response = response;
        }

        ssize_t written = client_send_data(client, string_data(response) + response_data->written,
                                           string_length(response) - response_data->written);
        if (written == -1) {
            return -1;
        }

        response_data->written += written;
        if (response_data->written == string_length(response)) {
            string_destroy(response);
            response_data->response = NULL;
            client->ws->state = response_data->status_code == 101 ? COMMUNICATING : END;
        }
    }

    if (client->ws->state == COMMUNICATING) {
        rc = ws_read(client->ws, client->read_buf, client);
        if (rc == -1) {
            return -1;
        }
        rc = ws_write(client->ws, client->write_buf, client);
        if (rc == -1) {
            return -1;
        }
    }

    if (client->ws->state == END) {
        if (ringbuf_available_data(client->write_buf) == 0) {
            log_info("WS Connection terminated");
            client->close = 1;
        }
    }
    return 0;
}

static int
client_notify_deauth(struct client *client)
{
    struct msg* msg = NULL;
    struct msg_generic *deauth_data = NULL;
    int rc = 0;

    deauth_data = msg_generic_init(client->client_id, NO_OPERATION_ID);
    if (deauth_data == NULL) {
        return -1;
    }

    msg = msg_init(OP_MSG_DEAUTH, sizeof(*deauth_data), deauth_data);
    if (msg == NULL) {
        return -1;
    }

    rc = msg_queue_send(client->server->priv_queue, msg);
    if (rc == -1) {
        msg_destroy(msg);
        return -1;
    }

    return 0;
}

void
on_tls_client_event(int fd, short what, void *arg)
{
    struct client *client = (struct client*)arg;
    char *clientid = client->client_id;
    char *buffer = NULL;
    int rc = 0;

    // 1)

    if (what == EV_WRITE) {
        client->tls_pollout = 0;
    } else if (what == EV_READ) {
        client->tls_pollin = 0;
    }

    // 2)

    while (!client->close && !client->eof) {
        ssize_t max_read_count = 0;
        max_read_count = ringbuf_get_buffer_for_writing(client->read_buf, &buffer, SIZE_MAX);
        if (max_read_count > 0) {
            ssize_t read_count = 0;
            read_count = tls_read(client->tls_ctx, buffer, (size_t) max_read_count);
            if (read_count == -1) {
                client->close = 1;
                log_cerror("Failed to read from socket: %s", tls_error(client->tls_ctx));
                goto end;
            } else if (read_count == TLS_WANT_POLLOUT) {
                client->tls_pollout = 1;
                ringbuf_rewind_write(client->read_buf, (size_t) max_read_count);
            } else if (read_count == TLS_WANT_POLLIN) {
                client->tls_pollin = 1;
                ringbuf_rewind_write(client->read_buf, (size_t) max_read_count);
            } else if (read_count == 0) {
                client->eof = 1;
            } else {
                if (read_count < max_read_count) {
                    ringbuf_rewind_write(client->read_buf, (size_t) (max_read_count - read_count));
                }
            }
        }

        rc = client_flush_write_buffer(client);
        if (rc == -1) {
            client->close = 1;
            goto end;
        }

        rc = client_do_work(client);
        if (rc == -1) {
            client->close = 1;
            log_cerror("An error occure while working on client");
            goto end;
        }

        if (client->tls_pollin && ringbuf_available_data(client->read_buf) == 0) {
            break;
        }
        if (client->tls_pollout && ringbuf_available_space(client->write_buf) == 0) {
            break;
        }
    }

end:
    if (client->close || (client->eof && ringbuf_available_data(client->write_buf) == 0)) {
        event_del(&client->ev_write);
        event_del(&client->ev_read);
        close(client->fd);
        remove_client(client->client_id);
        if (client->authentified) {
            client_notify_deauth(client);
        }
        client_destroy(client);
    } else {
        if (client->tls_pollout) {
            rc = event_add(&client->ev_write, NULL);
            if (rc == -1) {
                log_error("Failed to event_add client write event");
            }
        }
        if (client->tls_pollin) {
            rc = event_add(&client->ev_read, NULL);
            if (rc == -1) {
                log_error("Failed to event_add client read event");
            }
        }
    }
}

static int
client_do_action_auth(struct client *client, operationid operation_id, struct bson *data)
{
    struct string *name = NULL;
    struct string *passphrase = NULL;
    struct msg *msg = NULL;
    char *clientid = client->client_id;
    int rc = 0;

    if (not bson_get_string(data, "name", &name)) {
        log_cerror("Malformed packet, missing `data`.`name` key");
        return -1;
    }

    if (not bson_get_string(data, "passphrase", &passphrase)) {
        log_cerror("Malformed packet, missing `data`.`passphrase` key");
        return -1;
    }

    struct msg_auth_data *auth_data = msg_auth_data_init(client->client_id, operation_id, name, passphrase);
    if (auth_data == NULL) {
        return -1;
    }

    msg = msg_init(OP_MSG_AUTH, sizeof(*auth_data), auth_data);
    if (msg == NULL) {
        return -1;
    }

    client->authentified = true;

    log_cdebug("client_do_action_auth");

    rc = msg_queue_send(client->server->priv_queue, msg);
    if (rc == -1) {
        msg_destroy(msg);
        return -1;
    }

    client->max_frame_without_privsep_response = 1;
    client->frame_without_privsep_response_count++;

    return 0;
}

static int
client_do_action_auth_token(struct client *client, operationid operation_id, struct bson *data)
{
    struct string *token = NULL;
    struct msg *msg = NULL;
    char *clientid = client->client_id;
    int rc = 0;

    if (not bson_get_string(data, "token", &token)) {
        log_cerror("Malformed packet, missing `data`.`name` key");
        return -1;
    }

    struct msg_auth_token_data *auth_data = msg_auth_token_data_init(client->client_id, operation_id, token);
    if (auth_data == NULL) {
        return -1;
    }

    msg = msg_init(OP_MSG_AUTH_TOKEN, sizeof(*auth_data), auth_data);
    if (msg == NULL) {
        return -1;
    }

    client->authentified = true;

    log_cdebug("client_do_action_auth_token");

    rc = msg_queue_send(client->server->priv_queue, msg);
    if (rc == -1) {
        msg_destroy(msg);
        return -1;
    }

    client->max_frame_without_privsep_response = 1;
    client->frame_without_privsep_response_count++;

    return 0;
}

static int
client_do_action_create(struct client *client, operationid operation_id, struct bson *data)
{
    struct string *name = NULL;
    struct string *passphrase = NULL;
    struct msg *msg = NULL;
    char *clientid = client->client_id;
    int rc = 0;

    if (not bson_get_string(data, "name", &name)) {
        log_cerror("Malformed packet, missing `data`.`name` key");
        return -1;
    }

    if (not bson_get_string(data, "passphrase", &passphrase)) {
        log_cerror("Malformed packet, missing `data`.`passphrase` key");
        return -1;
    }

    struct msg_create_safe_data *create_data = msg_create_safe_data_init(client->client_id, operation_id, name, passphrase);
    if (create_data == NULL) {
        return -1;
    }

    msg = msg_init(OP_MSG_CREATE_SAFE, sizeof(*create_data), create_data);
    if (msg == NULL) {
        return -1;
    }

    log_cdebug("client_do_action_create");

    rc = msg_queue_send(client->server->priv_queue, msg);
    if (rc == -1) {
        msg_destroy(msg);
        return -1;
    }

    client->max_frame_without_privsep_response = 1;
    client->frame_without_privsep_response_count++;

    return 0;
}

static int
client_do_action_list_directory(struct client *client, operationid operation_id, struct bson *data)
{
    struct string *uid = NULL;
    struct msg *msg = NULL;
    char *clientid = client->client_id;
    int rc = 0;

    if (not bson_get_string(data, "uid", &uid)) {
        log_cerror("Malformed packet, missing `data`.`uid` key");
        return -1;
    }

    struct msg_list_directory_data *list_directory_data = msg_list_directory_data_init(client->client_id, operation_id, uid);
    if (list_directory_data == NULL) {
        return -1;
    }

    msg = msg_init(OP_MSG_LIST_DIRECTORY, sizeof(*list_directory_data), list_directory_data);
    if (msg == NULL) {
        return -1;
    }

    log_cdebug("client_do_action_list_directory");

    rc = msg_queue_send(client->server->priv_queue, msg);
    if (rc == -1) {
        msg_destroy(msg);
        return -1;
    }

    client->max_frame_without_privsep_response = 1;
    client->frame_without_privsep_response_count++;

    return 0;
}

static int
client_do_action_share_key(struct client *client, operationid operation_id, struct bson *data)
{
    int32_t mode = 0;
    struct msg *msg = NULL;
    char *clientid = client->client_id;
    struct msg_share_key_data *share_key_data = NULL;
    int rc = 0;

    if (not bson_get_int32(data, "mode", &mode)) {
        log_cerror("Malformed packet, missing `data`.`mode` key");
        return -1;
    }

    share_key_data = msg_share_key_data_init(client->client_id, operation_id, (uint8_t) mode);
    if (share_key_data == NULL) {
        return -1;
    }

    msg = msg_init(OP_MSG_SHARE_KEY, sizeof(*share_key_data), share_key_data);
    if (msg == NULL) {
        return -1;
    }

    log_cdebug("client_do_action_share_key");

    rc = msg_queue_send(client->server->priv_queue, msg);
    if (rc == -1) {
        msg_destroy(msg);
        return -1;
    }

    client->max_frame_without_privsep_response = 1;
    client->frame_without_privsep_response_count++;

    return 0;
}

static int
client_do_action_delete_file(struct client *client, operationid operation_id, struct bson *data)
{
    struct msg *msg = NULL;
    char *clientid = client->client_id;
    struct msg_delete_file_data *delete_file_data = NULL;
    struct string *file_uid = NULL;
    struct string *directory_uid = NULL;
    int rc = 0;

    if (not bson_get_string(data, "directoryUid", &directory_uid)) {
        log_cerror("Malformed packet, missing `data`.`directoryUid` key");
        return -1;
    }

    if (not bson_get_string(data, "fileUid", &file_uid)) {
        log_cerror("Malformed packet, missing `data`.`fileUid` key");
        return -1;
    }

    delete_file_data = msg_delete_file_data_init(client->client_id, operation_id, directory_uid, file_uid);
    if (delete_file_data == NULL) {
        return -1;
    }

    msg = msg_init(OP_MSG_DELETE_FILE, sizeof(*delete_file_data), delete_file_data);
    if (msg == NULL) {
        return -1;
    }

    log_cdebug("client_do_action_delete_file");

    rc = msg_queue_send(client->server->priv_queue, msg);
    if (rc == -1) {
        msg_destroy(msg);
        return -1;
    }

    client->max_frame_without_privsep_response = 1;
    client->frame_without_privsep_response_count++;

    return 0;
}

static int
client_do_action_start_download(struct client *client, operationid operation_id, struct bson *data)
{
    struct string *uid = NULL;
    struct msg *msg = NULL;
    const char *clientid = client->client_id;
    int rc = 0;

    if (not bson_get_string(data, "uid", &uid)) {
        log_cerror("Malformed packet, missing `data`.`uid` key");
        return -1;
    }

    struct msg_start_download_data *start_download_data = msg_start_download_data_init(client->client_id, operation_id, uid);
    if (start_download_data == NULL) {
        return -1;
    }

    msg = msg_init(OP_MSG_START_DOWNLOAD, sizeof(*start_download_data), start_download_data);
    if (msg == NULL) {
        return -1;
    }

    rc = msg_queue_send(client->server->priv_queue, msg);
    if (rc == -1) {
        msg_destroy(msg);
        return -1;
    }

    client->max_frame_without_privsep_response = 1;
    client->frame_without_privsep_response_count++;

    return 0;
}

static int
client_do_action_start_upload(struct client *client, operationid operation_id, struct bson *data)
{
    struct string *filename = NULL;
    struct string *directory_uid = NULL;
    char *clientid = client->client_id;
    struct string *start_upload_data = NULL;
    int rc = 0;

    if (not bson_get_string(data, "filename", &filename)) {
        log_cerror("Malformed packet, missing `data`.`filename` key");
        return -1;
    }

    if (not bson_get_string(data, "directory", &directory_uid)) {
        log_cerror("Malformed packet, missing `data`.`directory` key");
        return -1;
    }

    start_upload_data = msg_start_upload_data_init(client->client_id, operation_id, directory_uid, filename);
    if (start_upload_data == NULL) {
        return -1;
    }

    log_cdebug("client_do_action_start_upload");

    rc = msg_queue_send_sinit(client->server->priv_queue, OP_MSG_START_UPLOAD, start_upload_data);
    if (rc == -1) {
        return -1;
    }

    client->max_frame_without_privsep_response = 1;
    client->frame_without_privsep_response_count++;

    return 0;
}

static int
client_do_action_upload(struct client *client, operationid operation_id, struct bson *data)
{
    struct string *chunk_data = NULL;
    char *clientid = client->client_id;
    struct string *upload_data = NULL;
    int rc = 0;

    if (not bson_get_binary(data, "data", &chunk_data)) {
        log_cerror("Malformed packet, missing `data`.`data` key");
        return -1;
    }

    upload_data = msg_upload_data_init(client->client_id, operation_id, chunk_data);
    if (upload_data == NULL) {
        return -1;
    }

    log_cdebug("client_do_action_upload");

    rc = msg_queue_send_sinit(client->server->priv_queue, OP_MSG_UPLOAD, upload_data);
    if (rc == -1) {
        return -1;
    }

    client->max_frame_without_privsep_response = 5;
    client->frame_without_privsep_response_count++;

    return 0;
}

static int
client_do_action_upload_complete(struct client *client, operationid operation_id)
{
    char *clientid = client->client_id;
    struct msg_generic *msg_data = NULL;
    int rc = 0;

    msg_data = msg_generic_init(client->client_id, operation_id);
    if (msg_data == NULL) {
        return -1;
    }

    log_cdebug("client_do_action_upload_complete");

    rc = msg_queue_send_init(client->server->priv_queue, OP_MSG_UPLOAD_COMPLETE, sizeof(*msg_data), msg_data);
    if (rc == -1) {
        return -1;
    }

    client->max_frame_without_privsep_response = 1;
    client->frame_without_privsep_response_count++;

    return 0;
}

static int
client_do_action_cancel_upload(struct client *client, operationid operation_id)
{
    char *clientid = client->client_id;
    struct msg_generic *msg_data = NULL;
    int rc = 0;

    msg_data = msg_generic_init(client->client_id, operation_id);
    if (msg_data == NULL) {
        return -1;
    }

    log_cdebug("client_do_action_cancel_upload");

    rc = msg_queue_send_init(client->server->priv_queue, OP_MSG_UPLOAD_CANCEL, sizeof(*msg_data), msg_data);
    if (rc == -1) {
        return -1;
    }

    client->max_frame_without_privsep_response = 1;
    client->frame_without_privsep_response_count++;

    return 0;
}

static int
client_do_action(struct client *client, struct string *action, operationid operation_id, struct bson *data)
{
    char *clientid = client->client_id;

#ifdef DEBUG
    if (string_eq_cstr(action, "debug")) {
        log_cdebug("debug");
        return 0;
    }
#endif

    switch (client->mode) {
        case SAFE_MODE_INIT: {
            if (string_eq_cstr(action, "auth")) {
                return client_do_action_auth(client, operation_id, data);
            }
            if (string_eq_cstr(action, "auth-token")) {
                return client_do_action_auth_token(client, operation_id, data);
            }
            if (string_eq_cstr(action, "create")) {
                return client_do_action_create(client, operation_id, data);
            }

            break;
        }
        case SAFE_MODE_CONTROL: {
            if (string_eq_cstr(action, "list-directory")) {
                return client_do_action_list_directory(client, operation_id, data);
            }
            if (string_eq_cstr(action, "share-key")) {
                return client_do_action_share_key(client, operation_id, data);
            }
            if (string_eq_cstr(action, "delete-file")) {
                return client_do_action_delete_file(client, operation_id, data);
            }

            break;
        }
        case SAFE_MODE_DOWNLOAD: {
            if (string_eq_cstr(action, "download")) {
                return client_do_action_start_download(client, operation_id, data);
            }
            // FIXME cancel-download

            break;
        }
        case SAFE_MODE_UPLOAD: {
            if (string_eq_cstr(action, "start-upload")) {
                return client_do_action_start_upload(client, operation_id, data);
            }
            if (string_eq_cstr(action, "upload")) {
                return client_do_action_upload(client, operation_id, data);
            }
            if (string_eq_cstr(action, "upload-complete")) {
                return client_do_action_upload_complete(client, operation_id);
            }
            if (string_eq_cstr(action, "cancel-upload")) {
                return client_do_action_cancel_upload(client, operation_id);
            }
            break;
        }
        default: {
            log_cwarning("Invalid client mode: %d", client->mode);
            return -1;
        }
    }
    log_cwarning("Invalid action: %s", string_data(action));
    return -1;
}

int
client_process_packet(struct client *client, struct bson *bson)
{
    struct string *action = NULL;
    struct bson *data = NULL;
    char *clientid = client->client_id;
    operationid operation_id = 0;

    if (not bson_get_string(bson, "action", &action)) {
        log_cerror("Malformed packet, missing `action` key");
        return -1;
    }

    if (not bson_get_document(bson, "data", &data)) {
        log_cerror("Malformed packet, missing `data` key");
        return -1;
    }

    if (not bson_get_int64(bson, "operationId", &operation_id)) {
        log_cerror("Malformed packet, missing `operationId` key");
        return -1;
    }

    return client_do_action(client, action, operation_id, data);
}

int
client_send_frame(struct client *client, enum ws_opcode opcode, struct string *payload,
                  ws_frame_sent_callback sent_callback, operationid operation_id)
{
    const char *clientid = client->client_id;
    int rc = 0;

    rc = ws_send_frame(client->ws, opcode, payload, sent_callback, operation_id);
    if (rc == -1) {
        log_cerror("Failed to send ws frame");
        return -1;
    }

    rc = event_add(&client->ev_write, NULL);
    if (rc == -1) {
        log_cerror("Failed to event_add write client event");
        return -1;
    }

    return 0;
}

int
client_send_bson_frame(struct client *client, struct bson *bson)
{
    return client_send_bson_frame_with_cb(client, bson, NULL, NO_OPERATION_ID);
}


int
client_send_bson_frame_with_cb(struct client *client, struct bson *bson,
                       ws_frame_sent_callback sent_callback, operationid operation_id)
{
    struct string *response_data = NULL;

    response_data = bson_encode(bson);
    if (response_data == NULL) {
        log_error("Failed to encode bson data to send frame");
        return -1;
    }

    return client_send_frame(client, WS_OP_BINARY, response_data, sent_callback, operation_id);
}
