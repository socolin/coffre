#ifndef COFFRE_NETWORK_H
#define COFFRE_NETWORK_H

#include <event.h>

#include "core/privsep.h"
#include "utils/dictionary.h"

struct server {
    int fd;
    struct tls_config *tls_conf;
    struct tls *tls_ctx;
    struct msg_queue *priv_queue;
    struct event_base *event;
};

int start_network(struct privsep_process *);
void remove_client(const char *);

#endif //COFFRE_NETWORK_H
