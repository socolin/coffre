#include "network.h"

#include <sys/socket.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <tls.h>
#include <paths.h>
#include <stdlib.h>
#include <iso646.h>
#include <signal.h>

#include "core/conf.h"
#include "core/msg_queue.h"
#include "core/types.h"
#include "utils/bson.h"
#include "compat/linux.h"
#include "utils/log.h"
#include "utils/serialize.h"
#include "safe/directory.h"

#include "client.h"

struct dictionary *g_clients = NULL;

int
start_server(struct server *server)
{
    int sfd = 0;
    int rc = 0;
    int yes = 1;
    uint32_t protocols = 0;

    signal(SIGPIPE, SIG_IGN);

    sfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sfd == -1) {
        log_error("Failed to create server socket: %m");
        return -1;
    }

    rc = setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &yes, 4);
    if (rc == -1) {
        log_error("Failed to setsockopt SO_REUSEADDR on server socket: %m");
        return -1;
    }

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(g_config.port);

    rc = bind(sfd, (const struct sockaddr *) &addr, sizeof(addr));
    if (rc == -1) {
        log_error("Failed to bind server socket (port: %u): %s", g_config.port, strerror(errno));
        return -1;
    }

    rc = listen(sfd, 5);
    if (rc == -1) {
        log_error("Failed to start listen on server socket: %m");
        return -1;
    }

    log_info("Listening on %hu", g_config.port);

    server->tls_conf = tls_config_new();
    if (server->tls_conf == NULL) {
        log_error("Failed tls_config_new: %m");
        goto error;
    }
    server->tls_ctx = tls_server();
    if (server->tls_ctx == NULL) {
        log_error("Failed tls_config_new: %m");
        goto error;
    }

    if (tls_config_parse_protocols(&protocols, g_config.ssl_protocols) < 0) {
        log_error("Failed tls_config_parse_protocols: `%s'", g_config.ssl_protocols);
        goto error;
    }

    rc = tls_config_set_protocols(server->tls_conf, protocols);
    if (rc == -1) {
        log_error("Failed tls_config_set_protocols: %s", tls_config_error(server->tls_conf));
        goto error;
    }

    rc = tls_config_set_cert_file(server->tls_conf, g_config.ssl_cert);
    if (rc == -1) {
        log_error("Failed tls_config_set_cert_file: %s", tls_config_error(server->tls_conf));
        goto error;
    }

    rc = tls_config_set_key_file(server->tls_conf, g_config.ssl_key);
    if (rc == -1) {
        log_error("Failed tls_config_set_key_file: %s", tls_config_error(server->tls_conf));
        goto error;
    }

    rc = tls_configure(server->tls_ctx, server->tls_conf);
    if (rc == -1) {
        log_error("Failed tls_configure: %s", tls_error(server->tls_ctx));
        goto error;
    }

    server->fd = sfd;

    return 0;
error:
    tls_free(server->tls_ctx);
    tls_config_free(server->tls_conf);
    close(sfd);
    return -1;
}

static void
on_server_event(int fd, short what, void *arg)
{
    int client_fd = 0;
    int rc = 0;
    struct sockaddr_storage addr;
    struct client *client = NULL;
    char *clientid = NULL;
    socklen_t addrlen = sizeof(addr);
    struct server *server = (struct server*)arg;
    int count_try_id = 0;

    log_debug("New incoming connection");

    client_fd = accept4(server->fd, (struct sockaddr *) &addr, &addrlen, SOCK_NONBLOCK);
    if (client_fd == -1) {
        log_error("Failed to accept(2) client: %m");
        return;
    }

    struct sockaddr_in *addr_in = ((struct sockaddr_in *) &addr);
    if (addr.ss_family == AF_INET) {
        log_info("Accept new connection %s:%u", inet_ntoa(addr_in->sin_addr),
                 ntohs(addr_in->sin_port));
    } else {
        log_error("Unknown new client fd sa_famility %d %u", addr.ss_family, addrlen);
        close(client_fd);
        goto invalid_socket_family;
    }

    client = calloc(1, sizeof(*client));
    if (client == NULL) {
        log_error("Failed to allocate client structure: %m");
        goto failed_allocate_client;
    }

    rc = client_init(client, client_fd);
    if (rc == -1) {
        log_error("Failed to init client");
        goto failed_client_init;
    }

    client->server = server;

    do {
        client_init_id(client);
        count_try_id++;
        if (count_try_id > 5) {
            log_error("Failed to find unique client id after 5 try");
            goto failed_find_free_client_id;
        }
    } while (dictionary_get(g_clients, client->client_id) != NULL);

    rc = dictionary_put(g_clients, client->client_id, client);
    if (rc == -1) {
        goto failed_add_client_to_client_list;
    }

    clientid = client->client_id;
    log_cinfo("New client: %s:%u", inet_ntoa(addr_in->sin_addr), ntohs(addr_in->sin_port));

    rc = tls_accept_socket(server->tls_ctx, &client->tls_ctx, client_fd);
    if (rc == -1) {
        log_error("Failed to tls_accept_socket: %s", tls_error(server->tls_ctx));
        goto failed_tls_accept;
    }

    event_set(&client->ev_read, client_fd, EV_READ, on_tls_client_event, client);
    event_base_set(server->event, &client->ev_read);
    rc = event_add(&client->ev_read, NULL);
    if (rc == -1) {
        log_error("Failed to event_add");
        goto failed_event_add;
    }

    event_set(&client->ev_write, client_fd, EV_WRITE, on_tls_client_event, client);
    event_base_set(server->event, &client->ev_write);

    return;

failed_event_add:
    tls_free(client->tls_ctx);
failed_tls_accept:
failed_add_client_to_client_list:
failed_find_free_client_id:
failed_client_init:
    freezero(client, sizeof(*client));
failed_allocate_client:
invalid_socket_family:
    close(client_fd);
}

static struct client *
get_client(clientid client_id)
{
    return dictionary_get(g_clients, client_id);
}

void remove_client(const char *client_id)
{
    dictionary_remove(g_clients, client_id);
}

static struct bson *
network_response(struct client *client, const char *action, operationid operation_id, bool success, bool completed)
{
    const char *clientid = client->client_id;
    struct bson *response = NULL;
    int rc = 0;

    response = bson_init(BSON_TYPE_DOCUMENT);
    if (response == NULL) {
        log_cerror("Failed to init bson for response");
        return NULL;
    }

    rc = bson_set_cstring(response, "action", action);
    if (rc == -1) {
        log_cerror("Failed to set `action` in response");
        goto error;
    }

    rc = bson_set_int64(response, "operationId", operation_id);
    if (rc == -1) {
        log_cerror("Failed to set `operationId` in response");
        goto error;
    }

    rc = bson_set_bool(response, "success", success);
    if (rc == -1) {
        log_cerror("Failed to set `success` in response");
        goto error;
    }

    rc = bson_set_bool(response, "completed", completed);
    if (rc == -1) {
        log_cerror("Failed to set `completed` in response");
        goto error;
    }

    return response;
error:
    bson_destroy(response);
    return NULL;
}

int
network_deserialize_list_directory(struct bson *files, char *data, size_t data_size, size_t *offset)
{
    struct bson *file = NULL;
    struct bson *prev_file = NULL;
    struct string *file_uid = NULL;
    struct string *filename = NULL;
    time_t date = 0;
    int32_t type = 0;
    int64_t length = 0;

    int rc = 0;

    while (*offset < data_size) {
        rc = deserialize_string(data, data_size, offset, &file_uid);
        if (rc == -1) {
            log_error("Failed to deserialize `file_uid`");
            goto error;
        }
        rc = deserialize_string(data, data_size, offset, &filename);
        if (rc == -1) {
            log_error("Failed to deserialize `filename`");
            goto error;
        }
        rc = deserialize_time(data, data_size, offset, &date);
        if (rc == -1) {
            log_error("Failed to deserialize `date`");
            goto error;
        }
        rc = deserialize_int32(data, data_size, offset, &type);
        if (rc == -1) {
            log_error("Failed to deserialize `type`");
            goto error;
        }
        if (type == FILE_TYPE_FILE) {
            rc = deserialize_int64(data, data_size, offset, &length);
            if (rc == -1) {
                log_error("Failed to deserialize `length`");
                goto error;
            }
        }

        file = bson_init(BSON_TYPE_DOCUMENT);
        if (file == NULL) {
            goto error;
        }

        rc = bson_set_string(file, "uid", file_uid);
        if (rc == -1) {
            goto error;
        }
        file_uid = NULL;

        rc = bson_set_string(file, "name", filename);
        if (rc == -1) {
            goto error;
        }
        filename = NULL;

        rc = bson_set_timestamp(file, "date", date);
        if (rc == -1) {
            goto error;
        }
        rc = bson_set_int32(file, "type", type);
        if (rc == -1) {
            goto error;
        }
        rc = bson_set_int64(file, "length", length);
        if (rc == -1) {
            goto error;
        }

        if (prev_file != NULL) {
            prev_file->next = file;
        } else {
            files->v_child = file;
        }

        prev_file = file;
        file = NULL;
    }

    return 0;
error:
    string_destroy(filename);
    string_destroy(file_uid);
    bson_destroy(file);
    return -1;
}

int
network_process_list_directory_result_msg(struct client *client, struct msg_list_directory_result_data *data)
{
    const char *clientid = client->client_id;
    struct bson *response = NULL;
    struct bson *files = NULL;
    struct string *name = NULL;
    struct string *next_uid = NULL;
    int8_t hasNext = 0;
    size_t offset = 0;

    int rc = 0;

    log_cdebug("result: %d", data->success);

    response = network_response(client, "list-directory", data->operation_id, data->success, true);
    if (response == NULL) {
        log_cerror("Failed to init bson for response");
        return -1;
    }
    if (not data->success) {
        goto response;
    }

    files = bson_set_empty_array(response, "files");
    if (files == NULL) {
        log_cerror("Failed to set `files` in response");
        rc = -1;
        goto end;
    }

    rc = bson_set_cstring(response, "uid", data->file_uid);
    if (rc == -1) {
        log_cerror("Failed to set `uid` in response");
        goto end;
    }

    rc = deserialize_string(data->data, data->data_size, &offset, &name);
    if (rc == -1) {
        log_cerror("Failed to deserialize `name` from msg data");
        goto end;
    }

    rc = bson_set_string(response, "name", name);
    if (rc == -1) {
        string_destroy(name);
        log_cerror("Failed to set `name` in response");
        goto end;
    }

    rc = deserialize_int8(data->data, data->data_size, &offset, &hasNext);
    if (rc == -1) {
        log_cerror("Failed to deserialize `name` from msg data");
        goto end;
    }

    if (hasNext) {

        rc = deserialize_string(data->data, data->data_size, &offset, &next_uid);
        if (rc == -1) {
            log_cerror("Failed to deserialize `next_uid` from msg data");
            goto end;
        }

        rc = bson_set_string(response, "next_uid", next_uid);
        if (rc == -1) {
            string_destroy(next_uid);
            log_cerror("Failed to set `next_uid` in response");
            goto end;
        }
    }

    rc = network_deserialize_list_directory(files, data->data, data->data_size, &offset);
    if (rc == -1) {
        log_cerror("Failed to deserialized msg data");
        goto end;
    }

response:
    rc = client_send_bson_frame(client, response);
    if (rc == -1) {
        log_cerror("Failed to send bson frame");
        goto end;
    }

end:
    bson_destroy(response);
    return rc;
}

int
network_process_auth_result_msg(struct client *client, const char *action, struct msg_auth_result_data *data)
{
    const char *clientid = client->client_id;
    struct bson *response = NULL;
    int rc = 0;

    log_cdebug("action: %s result: %d %d", action, data->success, data->mode);

    response = network_response(client, action, data->operation_id, data->success, true);
    if (response == NULL) {
        log_cerror("Failed to init bson for response action: %s", action);
        return -1;
    }

    rc = bson_set_int32(response, "max_file_per_directory", MAX_FILE_PER_DIRECTORY);
    if (rc == -1) {
        log_cerror("Failed to set `max_file_per_directory`");
        return -1;
    }

    rc = client_send_bson_frame(client, response);
    if (rc == -1) {
        log_cerror("Failed to send bson frame");
    }

    if (data->success) {
        client->mode = data->mode;
    }

    bson_destroy(response);
    return rc;
}

static int
network_process_generic_result_msg_with_cb(struct client *client, const char *action, struct msg_generic_result *data, ws_frame_sent_callback callback)
{
    const char *clientid = client->client_id;
    struct bson *response = NULL;
    int rc = 0;

    log_cdebug("action: %s result: %d", action, data->success);

    response = network_response(client, action, data->operation_id, data->success, callback == NULL);
    if (response == NULL) {
        log_cerror("Failed to init bson for response action: %s", action);
        return -1;
    }

    if (!data->success) {
        callback = NULL;
    }

    rc = client_send_bson_frame_with_cb(client, response, callback, data->operation_id);
    if (rc == -1) {
        log_cerror("Failed to send bson frame");
    }

    bson_destroy(response);
    return rc;
}

static int
network_process_generic_result_msg(struct client *client, const char *action, struct msg_generic_result *data)
{
    return network_process_generic_result_msg_with_cb(client, action, data, NULL);
}

static int
network_process_error(struct client *client, struct msg_error_result *data)
{
    const char *action = "error";
    const char *clientid = client->client_id;
    struct bson *response = NULL;
    struct string *response_message = NULL;
    size_t offset = 0;
    int rc = 0;

    log_cdebug("Response error");

    response = network_response(client, "error", data->operation_id, data->success, true);
    if (response == NULL) {
        log_cerror("Failed to init bson for response action: %s", action);
        goto end;
    }

    rc = deserialize_string(data->data, data->data_size, &offset, &response_message);
    if (rc == -1) {
        log_cerror("Failed to set `errorMessage` in bson");
        goto end;
    }

    rc = bson_set_string(response, "errorMessage", response_message);
    if (rc == -1) {
        string_destroy(response_message);
        log_cerror("Failed to set `errorMessage` in bson");
        goto end;
    }

    rc = client_send_bson_frame(client, response);
    if (rc == -1) {
        string_destroy(response_message);
        log_cerror("Failed to send bson frame");
        goto end;
    }

end:
    bson_destroy(response);
    return rc;
}

int
network_process_start_upload_result_msg(struct client *client, struct msg_generic_data_result *data)
{
    const char *clientid = client->client_id;
    struct bson *response = NULL;
    struct string *file_uid = NULL;
    size_t offset = 0;
    int rc = 0;

    log_cdebug("result: %d", data->success);

    response = network_response(client, "start-upload", data->operation_id, data->success, true);
    if (response == NULL) {
        log_cerror("Failed to init bson for response");
        return -1;
    }

    if (!data->success) {
        goto response;
    }

    rc = deserialize_string(data->data, data->data_size, &offset, &file_uid);
    if (rc == -1) {
        log_cerror("Failed to deserialize `file_uid` from msg data");
        goto end;
    }

    rc = bson_set_string(response, "file_uid", file_uid);
    if (rc == -1) {
        string_destroy(file_uid);
        log_cerror("Failed to set `file_uid` in response");
        goto end;
    }

response:
    rc = client_send_bson_frame(client, response);
    if (rc == -1) {
        log_cerror("Failed to send bson frame");
        goto end;
    }

end:
    bson_destroy(response);
    return rc;
}

int
network_process_upload_complete_result_msg(struct client *client, struct msg_generic_data_result *data)
{
    const char *clientid = client->client_id;
    struct bson *response = NULL;
    struct string *directory_uid = NULL;
    size_t offset = 0;
    int rc = 0;

    log_cdebug("result: %d", data->success);

    response = network_response(client, "upload-complete", data->operation_id, data->success, true);
    if (response == NULL) {
        log_cerror("Failed to init bson for response");
        return -1;
    }

    if (!data->success) {
        goto response;
    }

    rc = deserialize_string(data->data, data->data_size, &offset, &directory_uid);
    if (rc == -1) {
        log_cerror("Failed to deserialize `directory_uid` from msg data");
        goto end;
    }

    rc = bson_set_string(response, "directory_uid", directory_uid);
    if (rc == -1) {
        string_destroy(directory_uid);
        log_cerror("Failed to set `directory_uid` in response");
        goto end;
    }

response:
    rc = client_send_bson_frame(client, response);
    if (rc == -1) {
        log_cerror("Failed to send bson frame");
        goto end;
    }

end:
    bson_destroy(response);
    return rc;
}

int
network_process_share_key_result_msg(struct client *client, struct msg_share_key_result_data *data)
{
    const char *clientid = client->client_id;
    struct bson *response = NULL;
    int rc = 0;

    log_cdebug("result: %d", data->success);

    response = network_response(client, "share-key", data->operation_id, data->success, true);
    if (response == NULL) {
        log_cerror("Failed to init bson for response");
        return -1;
    }

    if (!data->success) {
        goto response;
    }

    rc = bson_set_cstring(response, "token", data->token);
    if (rc == -1) {
        log_cerror("Failed to set `directory_uid` in response");
        goto end;
    }

response:
    rc = client_send_bson_frame(client, response);
    if (rc == -1) {
        log_cerror("Failed to send bson frame");
        goto end;
    }

end:
    bson_destroy(response);
    return rc;
}

static void
on_network_sent_download_data(struct client *client, operationid operation_id)
{
    const char *clientid = client->client_id;
    struct msg *msg = NULL;
    int rc = 0;

    struct msg_generic *msg_download_data = msg_generic_init(client->client_id, operation_id);
    if (msg_download_data == NULL) {\
        log_cerror("Failed to msg_generic_init");
        return;
    }

    msg = msg_init(OP_MSG_DOWNLOAD, sizeof(*msg_download_data), msg_download_data);
    if (msg == NULL) {
        log_cerror("Failed to msg_init");
        return;
    }

    rc = msg_queue_send(client->server->priv_queue, msg);
    if (rc == -1) {
        log_cerror("Failed to msg_queue_send");
        msg_destroy(msg);
        return;
    }

    client->frame_without_privsep_response_count++;
}

int
network_process_download_result_msg(struct client *client, struct msg_generic_data_result *data)
{
    const char *clientid = client->client_id;
    struct bson *response = NULL;
    int rc = 0;
    struct string *file_data = NULL;
    size_t offset = 0;
    ws_frame_sent_callback callback = NULL;
    bool completed = true;

    log_cdebug("result: %d", data->success);

    response = network_response(client, "download", data->operation_id, data->success, completed);
    if (response == NULL) {
        log_cerror("Failed to init bson for response");
        return -1;
    }

    if (!data->success) {
        goto response;
    }

    rc = deserialize_string(data->data, data->data_size, &offset, &file_data);
    if (rc == -1) {
        log_cerror("Failed to deserialize string file_data");
        goto end;
    }

    completed = string_length(file_data) == 0;

    rc = bson_set_bool(response, "completed", completed);
    if (rc == -1) {
        log_cerror("Failed to set `completed` in response");
        goto end;
    }

    rc = bson_set_binary(response, "data", file_data);
    if (rc == -1) {
        string_destroy(file_data);
        log_cerror("Failed to set `directory_uid` in response");
        goto end;
    }

    if (not completed) {
        callback = on_network_sent_download_data;
    }

response:
    rc = client_send_bson_frame_with_cb(client, response, callback, data->operation_id);
    if (rc == -1) {
        log_cerror("Failed to send bson frame");
        goto end;
    }

end:
    bson_destroy(response);
    return rc;
}

static int
network_process_msg(struct msg_queue *queue, uint64_t opcode, size_t len, char *data)
{
    int rc = 0;
    struct client *client = NULL;

    client = get_client(((struct msg_generic *) data)->client_id);
    if (client == NULL) {
        // May happen if client disconnect while waiting response from other process
        goto end;
    }

    log_debug("Opcode: %" PRIu64, opcode);
    switch (opcode) {
        case OP_MSG_AUTH_RESULT:
            rc = network_process_auth_result_msg(client, "auth", (struct msg_auth_result_data *) data);
            break;
        case OP_MSG_CREATE_SAFE_RESULT:
            rc = network_process_auth_result_msg(client, "create-safe", (struct msg_auth_result_data *) data);
            break;
        case OP_MSG_LIST_DIRECTORY_RESULT:
            rc = network_process_list_directory_result_msg(client, (struct msg_list_directory_result_data *) data);
            break;
        case OP_MSG_START_UPLOAD_RESULT:
            rc = network_process_start_upload_result_msg(client, (struct msg_generic_data_result *) data);
            break;
        case OP_MSG_UPLOAD_RESULT:
            rc = network_process_generic_result_msg(client, "upload", (struct msg_generic_result *) data);
            break;
        case OP_MSG_UPLOAD_COMPLETE_RESULT:
            rc = network_process_upload_complete_result_msg(client, (struct msg_generic_data_result *) data);
            break;
        case OP_MSG_UPLOAD_CANCEL_RESULT:
            rc = network_process_generic_result_msg(client, "cancel-upload", (struct msg_generic_result *) data);
            break;
        case OP_MSG_SHARE_KEY_RESULT:
            rc = network_process_share_key_result_msg(client, (struct msg_share_key_result_data *) data);
            break;
        case OP_MSG_START_DOWNLOAD_RESULT:
            rc = network_process_generic_result_msg_with_cb(client, "start-download", (struct msg_generic_result *) data, on_network_sent_download_data);
            break;
        case OP_MSG_DOWNLOAD_RESULT:
            rc = network_process_download_result_msg(client, (struct msg_generic_data_result *) data);
            break;
        case OP_MSG_DOWNLOAD_CANCEL_RESULT:
            rc = network_process_generic_result_msg(client, "cancel-download", (struct msg_generic_result *) data);
            break;
        case OP_MSG_DELETE_FILE_RESULT:
            rc = network_process_generic_result_msg(client, "delete-file", (struct msg_generic_result *) data);
            break;
        case OP_MSG_ERROR_RESULT:
            rc = network_process_error(client, (struct msg_error_result *) data);
            break;
        default:
            log_error("Unknown opcode %" PRIu64, opcode);
            rc = -1;
            break;
    }

    log_debug("%" PRIu64 " done", opcode);

    if (rc == -1) {
        log_error("Failed to process msg %" PRIu64, opcode);
    }

    client->frame_without_privsep_response_count--;

end:
    freezero(data, len);
    return rc;
}

int
start_network(struct privsep_process *parent)
{
    struct event_base *event = NULL;
    struct server *server = NULL;
    struct event server_socket_event;
    int rc = 0;

    rc = tls_init();
    if (rc == -1) {
        log_error("Failed to tls_init");
        return -1;
    }

    rc = pledge("stdio rpath inet", NULL);
    if (rc == -1) {
        log_error("pledge failed: %m");
        return -1;
    }

    event = event_base_new();
    if (event == NULL) {
        log_error("Failed to allocate event_base");
        return -1;
    }

    g_clients = dictionary_init();
    if (g_clients == NULL) {
        return -1;
    }

    server = calloc(1, sizeof(*server));
    if (server == NULL) {
        log_error("Failed to calloc server struct: %m");
        return -1;
    }
    server->event = event;

    // FIXME: from config
    // rc = chroot(_PATH_VAREMPTY);
    // if (rc == -1) {
    //     log_error("chroot failed: %m");
    //     return -1;
    // }

    rc = start_server(server);
    if (rc == -1) {
        return -1;
    }

    event_set(&server_socket_event, server->fd, EV_READ|EV_PERSIST, on_server_event, server);
    event_base_set(event, &server_socket_event);
    rc = event_add(&server_socket_event, NULL);
    if (rc == -1) {
        log_error("Failed to event_add to listen incomming connection on server socket");
        return -1;
    }

    parent->queue = msg_queue_init(parent, event, network_process_msg);
    if (parent->queue == NULL) {
        log_error("Failed to init `master` queue");
        return -1;
    }

    server->priv_queue = parent->queue;

    event_base_dispatch(event);

    msg_queue_destroy(parent->queue);
    event_base_free(event);

    return 0;
}