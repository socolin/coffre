#ifndef COFFRE_WEBSOCKET_H
#define COFFRE_WEBSOCKET_H

#include "core/types.h"
#include "utils/string.h"
#include "utils/dictionary.h"
#include "utils/ring_buffer.h"

struct client;

#define MAX_HEADER_LINE_SIZE 512
#define MAX_FRAME_SIZE (2 * 1024 * 1024)
#define WS_HEADER_SEC_WEBSOCKET_KEY "Sec-WebSocket-Key"
#define WS_HEADER_SEC_WEBSOCKET_VERSION "Sec-WebSocket-Version"
#define WS_MAGIC_KEY "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
#define MAX_SND_PENDING_FRAME 5

enum client_state {
    READ_HANDSHAKE_REQUEST,
    COMMUNICATING,
    RESPONSE_HANDSHAKE,
    END,
};

enum frame_parser_state {
    WS_FRAME_STATE_HEADER,
    WS_FRAME_STATE_SIZE,
    WS_FRAME_STATE_MASK,
    WS_FRAME_STATE_PAYLOAD,
    WS_FRAME_STATE_READY,
};

enum ws_opcode {
    WS_OP_CONTINUE = 0x0,
    WS_OP_TEXT = 0x1,
    WS_OP_BINARY = 0x2,
    WS_OP_CLOSE = 0x8,
    WS_OP_PING = 0x9,
    WS_OP_PONG = 0xa,
};

struct ws_frame {
    uint8_t fin;
    uint8_t opcode;
    uint8_t use_mask;
    size_t payload_length;
    uint32_t mask;
    struct string *payload;
    size_t process_count;
};

typedef void (*ws_frame_sent_callback)(struct client *, operationid);

/* state data when ws->state == READ_HANDSHAKE_REQUEST */
struct ws_request_state_data {
    struct string *cur_line;
};
/* state data when ws->state == RESPONSE_HANDSHAKE */
struct ws_response_state_data {
    int status_code;
    const char *status_str;
    const char *error_msg;
    struct string *response;
    size_t written;
};
/* state data when ws->state == COMMUNICATING */
struct ws_communicating_state_data {
    enum frame_parser_state frame_state;
    struct ws_frame rcv_frame;
    size_t rcv_previous_frames_len;
    uint8_t rcv_previous_frames_opcode;
    size_t snd_pending_count;
    size_t snd_pending_pos;
    struct {
        struct string *header;
        struct string *payload;
        size_t written;
        ws_frame_sent_callback sent_callback;
        operationid operation_id;
    } snd_pending_frames[MAX_SND_PENDING_FRAME];
};

struct websocket_client_data {
    enum client_state state;
    struct string *path;
    struct dictionary *headers;
    struct {
        struct ws_request_state_data request;
        struct ws_response_state_data response;
        struct ws_communicating_state_data communicating;
    } state_data;
};

struct websocket_client_data *ws_init();
void ws_destroy(struct websocket_client_data *);
void ws_set_state_response(struct websocket_client_data *, enum client_state, int, const char *, const char *);
int ws_read_handshake_request(struct websocket_client_data *, struct ring_buf *);
int ws_read(struct websocket_client_data *, struct ring_buf *, struct client *);
int ws_write(struct websocket_client_data *, struct ring_buf *, struct client *);
int ws_parse_handshake_header(struct websocket_client_data *, struct string *);
struct string *ws_http_response(struct websocket_client_data *);
int ws_send_frame(struct websocket_client_data *, enum ws_opcode, struct string *, ws_frame_sent_callback, operationid);

#endif //COFFRE_WEBSOCKET_H
