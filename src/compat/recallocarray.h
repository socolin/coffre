#ifndef COFFRE_RECALLOCARRAY_H
#define COFFRE_RECALLOCARRAY_H

#ifdef __linux__

#include <string.h>

/*
 * This is sqrt(SIZE_MAX+1), as s1*s2 <= SIZE_MAX
 * if both s1 < MUL_NO_OVERFLOW and s2 < MUL_NO_OVERFLOW
 */
#define MUL_NO_OVERFLOW ((size_t)1 << (sizeof(size_t) * 4))

void * recallocarray(void *ptr, size_t oldnmemb, size_t newnmemb, size_t size);

#endif

#endif //COFFRE_RECALLOCARRAY_H
