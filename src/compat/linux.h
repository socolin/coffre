#ifndef COFFRE_COFFRE_H
#define COFFRE_COFFRE_H

#ifdef __linux__
#include <strings.h>

#include "compat/explicit_bzero.h"

#define pledge(unused1, unused2) 0;
#define freezero(memory, size)  \
explicit_bzero(memory, size);   \
free(memory);

#define __dead
#endif

#endif //COFFRE_COFFRE_H
