#ifndef COFFRE_EXPLICIT_BZERO_H
#define COFFRE_EXPLICIT_BZERO_H

#ifdef __linux__
#include <string.h>

void explicit_bzero(void *, size_t);

#endif

#endif //COFFRE_EXPLICIT_BZERO_H
