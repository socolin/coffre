#ifndef COFFRE_UTILS_H
#define COFFRE_UTILS_H

#include <stdbool.h>

#include "string.h"

bool is_valid_file_uid(const struct string *);

#endif //COFFRE_UTILS_H
