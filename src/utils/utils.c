#include <ctype.h>
#include <iso646.h>
#include "utils.h"

#include "core/types.h"

bool is_valid_file_uid(const struct string *file_uid) {
    if (string_length(file_uid) != FILE_UID_CHAR_LEN) {
        return false;
    }

    const char *data = string_data(file_uid);
    for (size_t i = 0; i < string_length(file_uid); i++) {
        if (not isxdigit(data[i])) {
            return false;
        }
    }

    return true;
}
