#ifndef COFFRE_SERIALIZE_H
#define COFFRE_SERIALIZE_H

#include <stdint.h>
#include "string.h"

int serialize_string(struct string *, const struct string *);
int serialize_cstring(struct string *, const char *);
int serialize_data(struct string *, const void *, size_t);

#define declaration_serialize_simple_type(type) \
    int serialize_ ## type (struct string *, type ## _t);

declaration_serialize_simple_type(int8);
declaration_serialize_simple_type(int32);
declaration_serialize_simple_type(int64);
declaration_serialize_simple_type(uint8);
declaration_serialize_simple_type(uint32);
declaration_serialize_simple_type(uint64);
declaration_serialize_simple_type(size);
declaration_serialize_simple_type(time);

int deserialize_string(const char *data, size_t len, size_t *, struct string **);
int deserialize_data(const char *data, size_t len, size_t *, void **, size_t *);

#define declaration_deserialize_simple_type(type) \
    int deserialize_ ## type (const char *, size_t, size_t *, type ## _t *);

declaration_deserialize_simple_type(int8);
declaration_deserialize_simple_type(int32);
declaration_deserialize_simple_type(int64);
declaration_deserialize_simple_type(uint8);
declaration_deserialize_simple_type(uint32);
declaration_deserialize_simple_type(uint64);
declaration_deserialize_simple_type(size);
declaration_deserialize_simple_type(time);

#endif //COFFRE_SERIALIZE_H
