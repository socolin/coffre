#include "dictionary.h"

#include <string.h>
#include <stdlib.h>

#include "log.h"
#include "compat/linux.h"

static int
dict_key_cmp(struct dictionary_node *e1, struct dictionary_node *e2)
{
    return strcmp(e1->key, e2->key);
}

RB_GENERATE_STATIC(dict_tree, dictionary_node, entry, dict_key_cmp)

struct dictionary *
dictionary_init()
{
    struct dictionary *dict = NULL;

    dict = calloc(1, sizeof(*dict));
    if (dict == NULL) {
        log_critical("Failed to calloc dictionary: %m");
        return NULL;
    }
    RB_INIT(&dict->head);

    return dict;
}

void
dictionary_destroy(struct dictionary *dict)
{
    struct dictionary_node *var = NULL;
    struct dictionary_node *nxt = NULL;

    if (dict == NULL) {
        return;
    }

    for (var = RB_MIN(dict_tree, &dict->head); var != NULL; var = nxt) {
        nxt = RB_NEXT(dict_tree, &dict->head, var);
        RB_REMOVE(dict_tree, &dict->head, var);
        free(var->key);
        free(var);
    }

    freezero(dict, sizeof(*dict));
}

int
dictionary_put(struct dictionary *dict, const char *key, void *value)
{
    struct dictionary_node *node;

    node = calloc(1, sizeof(*node));
    if (node == NULL) {
        log_critical("Failed to calloc dictionary node: %m");
        return -1;
    }

    node->key = strdup(key);
    node->value = value;

    RB_INSERT(dict_tree, &dict->head, node);

    return 0;
}

void *
dictionary_get(struct dictionary *dict, const char *key)
{
    struct dictionary_node find, *res;

    find.key = (char *) key;
    res = RB_FIND(dict_tree, &dict->head, &find);

    if (res == NULL) {
        return NULL;
    }

    return res->value;
}

void
dictionary_remove(struct dictionary *dict, const char *key)
{
    struct dictionary_node find, *res;

    find.key = (char *) key;
    res = RB_FIND(dict_tree, &dict->head, &find);
    if (res != NULL) {
        res = RB_REMOVE(dict_tree, &dict->head, res);
        freezero(res, sizeof(*res));
    }
}
