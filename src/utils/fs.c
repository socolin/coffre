#include "fs.h"
#include "log.h"

#include <ftw.h>
#include <unistd.h>

static int
delete_file(const char *path, const struct stat *stat, int flag, struct FTW *ftw)
{
    int rc = 0;

    if (flag == FTW_D || flag == FTW_DP) {
        rc = rmdir(path);
    } else {
        rc = unlink(path);
    }

    if (rc == -1) {
        log_error("Failed to delete path `%s': %m", path);
        return -1;
    }

    return 0;
}

int
fs_rmdir_recursive(const char *path)
{
    int rc = 0;

    rc = nftw(path, delete_file, 1, FTW_DEPTH | FTW_PHYS | FTW_MOUNT);
    if (rc == -1) {
        log_error("Failed to delete recursive path `%s'", path);
        return -1;
    }

    return 0;
}
