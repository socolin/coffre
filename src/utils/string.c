#include "string.h"

#include <string.h>
#include <stdlib.h>
#include <sys/param.h>
#include <stdarg.h>
#include <stdio.h>

#include "compat/linux.h"
#include "compat/recallocarray.h"
#include "utils/log.h"

struct string {
    size_t buf_size;
    size_t length;
    char *buffer;
};

struct string *
string_init()
{
    return string_init_reserve(16);
}

struct string *
string_init_reserve(size_t buf_size)
{
    struct string *str = NULL;
    str = calloc(1, sizeof(*str));
    if (str == NULL) {
        log_error("Failed to calloc string: %m");
        return NULL;
    }

    str->buf_size = buf_size;
    str->buffer = calloc(str->buf_size, sizeof(char));
    if (str->buffer == NULL) {
        log_error("Failed to calloc string buffer: %m");
        freezero(str, sizeof(*str));
        return NULL;
    }

    return str;
}

struct string *
string_from_data(const char *data, size_t len)
{
    struct string *str = NULL;
    str = calloc(1, sizeof(*str));
    if (str == NULL) {
        log_error("Failed to calloc string: %m");
        return NULL;
    }

    str->buf_size = len + 1;
    str->length = len;
    str->buffer = calloc(str->buf_size, sizeof(char));
    if (str->buffer == NULL) {
        log_error("Failed to calloc string buffer: %m");
        freezero(str, sizeof(*str));
        return NULL;
    }
    memcpy(str->buffer, data, len);

    return str;
}

struct string *
string_from_cstr(const char *cstr)
{
    if (cstr == NULL) {
        return NULL;
    }

    return string_from_data(cstr, strlen(cstr));
}

int
string_reserve(struct string *str, size_t len)
{
    if (len <= str->buf_size) {
        return 0;
    }

    str->buffer = recallocarray(str->buffer, str->buf_size, len, sizeof(char));

    if (str->buffer == NULL) {
        log_error("Failed to calloc to reserve string buffer: %m");
        return -1;
    }

    str->buf_size = len;

    return 0;
}

int
string_resize(struct string *str, size_t len)
{
    int rc = 0;

    if (len >= str->buf_size) {
        rc = string_reserve(str, len + 1);
        if (rc == -1) {
            return -1;
        }
    }

    str->length = len;
    str->buffer[len] = '\0';

    return 0;
}

void
string_destroy(struct string *str)
{
    if (str == NULL) {
        return;
    }

    freezero(str->buffer, str->buf_size);
    freezero(str, sizeof(*str));
}

void
string_destroy_keepdata(struct string *str)
{
    if (str == NULL) {
        return;
    }

    freezero(str, sizeof(*str));
}

static int
string_grow(struct string *str)
{
    str->buffer = recallocarray(str->buffer, str->buf_size, str->buf_size * 2, sizeof(char));

    if (str->buffer == NULL) {
        log_error("Failed to calloc string buffer: %m");
        return -1;
    }

    str->buf_size *= 2;

    return 0;
}

int
string_nappend(struct string *str, const char *data, size_t len)
{
    int rc;

    if (len == 0) {
        return 0;
    }

    while (str->length + len >= str->buf_size) {
        rc = string_grow(str);
        if (rc == -1) {
            return -1;
        }
    }

    memmove(str->buffer + str->length, data, len);
    str->length += len;
    str->buffer[str->length] = '\0';

    return 0;
}

int
string_append(struct string *str, const char *data)
{
    if (data == NULL) {
        return 0;
    }

    size_t len = strlen(data);
    return string_nappend(str, data, len);
}

int
string_vfappend(struct string *str, const char *format, va_list ap)
{
    ssize_t rc = 0;
    va_list ap_copy;

    while (1) {
        va_copy(ap_copy, ap);
        rc = vsnprintf(str->buffer + str->length, str->buf_size - str->length, format, ap_copy);
        if (rc >= str->buf_size - str->length) {
            rc = string_grow(str);
            if (rc == -1) {
                return -1;
            }
        } else {
            str->length += rc;
            break;
        }
    }
    return 0;
}

int
string_fappend(struct string *str, const char *format, ...)
{
    int rc = 0;
    va_list ap;

    va_start(ap, format);
    rc = string_vfappend(str, format, ap);
    va_end(ap);

   return rc;
}

int
string_sappend(struct string *str, const struct string *str2)
{
    if (str2 == NULL) {
        return 0;
    }

    return string_nappend(str, str2->buffer, str2->length);
}

int
string_cappend(struct string *str, char c)
{
    return string_nappend(str, &c, 1);
}

int
string_bappend(struct string *str, const void *data, size_t len)
{
    return string_nappend(str, data, len);
}

void
string_trim(struct string *str, const char c[])
{
    string_trim_start(str, c);
    string_trim_end(str, c);
}

void
string_trim_start(struct string *str, const char c[])
{
    size_t count = strlen(c);
    size_t start = 0;
    int match = 0;

    for (start = 0; start < str->length; start++) {
        match = 0;
        for (size_t i = 0; i < count; i++) {
            if (str->buffer[start] == c[i]) {
                match = 1;
                break;
            }
        }
        if (!match) {
            break;
        }
    }

    if (start > 0) {
        memmove(str->buffer, str->buffer + start, str->length - start);
        str->length -= start;
        bzero(str->buffer + str->length, start);
    }
}

void
string_trim_end(struct string *str, const char c[])
{
    size_t count = strlen(c);
    int match = 0;

    for (ssize_t i = str->length - 1; i >= 0; i--) {
        match = 0;
        for (size_t j = 0; j < count; j++) {
            if (str->buffer[i] == c[j]) {
                match = 1;
                string_chop(str);
                break;
            }
        }
        if (!match) {
            break;
        }
    }
}


void
string_chop(struct string *str)
{
    if (str->length > 0) {
        str->length--;
        str->buffer[str->length] = '\0';
    }
}

void
string_chomp(struct string *str)
{
    for (ssize_t i = str->length - 1; i >= 0; i--) {
        if (str->buffer[i] == '\r' || str->buffer[i] == '\n') {
            string_chop(str);
        }
    }
}

int
string_set(struct string *str, size_t pos, char *data, size_t len)
{
    int rc;

    if (pos + len >= str->buf_size) {
        rc = string_grow(str);
        if (rc == -1) {
            return -1;
        }
    }

    memmove(str->buffer + pos, data, len);

    return 0;
}

int
string_insert(struct string *str, char *data, size_t len)
{
    int rc;

    while (str->length + len >= str->buf_size) {
        rc = string_grow(str);
        if (rc == -1) {
            return -1;
        }
    }

    memmove(str->buffer + len, str->buffer, str->length);
    memmove(str->buffer, data, len);
    str->length += len;
    str->buffer[str->length] = '\0';

    return 0;
}

void
string_remove(struct string *str, size_t start, size_t len)
{
    if (len + start > str->length) {
        len = str->length - start;
    }
    if (len <= 0) {
        return;
    }

    memmove(str->buffer + start, str->buffer + start + len, str->length - start);
    str->length -= len;
    bzero(str->buffer + str->length, len);
}

char *
string_data(const struct string *str)
{
    if (str == NULL) {
        return NULL;
    }
    return str->buffer;
}

size_t
string_length(const struct string *str)
{
    if (str == NULL) {
        return 0;
    }
    return str->length;
}

size_t
string_count_char(struct string *str, char c)
{
    size_t count = 0;
    for (size_t i = 0; i < str->length; i++) {
        if (str->buffer[i] == c) {
            count++;
        }
    }
    return count;
}

static struct string **
string_init_string_array(size_t count)
{
    struct string **array = NULL;

    array = calloc(count, sizeof(*array));
    if (array == NULL) {
        log_error("Failed to calloc string array: %m");
        return NULL;
    }

    for (size_t i = 0; i < count; i++) {
        array[i] = string_init();
        if (array[i] == NULL) {
            goto error;
        }
    }

    return array;
error:
    for (size_t i = 0; i < count; i++) {
        string_destroy(array[i]);
    }

    return NULL;
}

ssize_t
string_split(struct string *str, char c, struct string **result[], size_t max)
{
    int rc = 0;
    size_t count = 0;
    size_t iter = 0;
    size_t start = 0;
    struct string **array = NULL;

    if (max == 0) {
        max = (size_t)-1;
    }

    count = string_count_char(str, c) + 1;
    count = MIN(count, max);

    array = string_init_string_array(count);
    if (array == NULL) {
        return -1;
    }

    for (size_t i = 0; i < str->length; i++) {
        if (str->buffer[i] == c) {
            rc = string_nappend(array[iter++], str->buffer + start, i - start);
            if (rc == -1) {
                goto error;
            }
            start = i + 1;
            if (iter + 1 >= max) {
                break;
            }
        }
    }

    rc = string_nappend(array[iter++], str->buffer + start, str->length - start);
    if (rc == -1) {
        goto error;
    }

    *result = array;

    return count;
error:
    for (size_t i = 0; i < count; i++) {
        string_destroy(array[i]);
    }
    freezero(array, sizeof(*array) * count);

    return -1;
}


int
string_eq(struct string *str1, struct string *str2)
{
    return strcmp(str1->buffer, str2->buffer) == 0;
}

int
string_eq_cstr(struct string *str1, const char *str2)
{
    return strcmp(str1->buffer, str2) == 0;
}


#include <snow/snow.h>

describe(string, {
    subdesc(init, {
        it("Allocate an empty string", {
            struct string *str = string_init();
            assert(str != NULL);
            asserteq(string_length(str), 0);
            string_destroy(str);
        });
        it("Allocate an empty string and reserve 32 bytes", {
            struct string *str = string_init_reserve(32);
            assert(str != NULL);
            asserteq(str->buf_size, 32);
            asserteq(string_length(str), 0);
            string_destroy(str);
        });
        it("Create a new string from cstring", {
            char cstring[] = "Hello World !";
            struct string *str = string_from_cstr(cstring);
            assert(str != NULL);
            asserteq_buf(str->buffer, cstring, sizeof(cstring));
            asserteq_str(str->buffer, cstring);
            asserteq(string_length(str), sizeof(cstring) - 1);
            string_destroy(str);
        });
        it("Create a new string from buffer", {
            char buffer[] = "\1\2\3\0\5\7";
            struct string *str = string_from_data(buffer, sizeof(buffer));
            assert(str != NULL);
            asserteq_buf(str->buffer, buffer, sizeof(buffer));
            asserteq(string_length(str), sizeof(buffer));
            string_destroy(str);
        });
    });
    subdesc(resize, {
        it("Reduce a string to a specific size", {
            char cstring[] = "Hello World !";
            struct string *str = string_from_cstr(cstring);
            assert(str != NULL);
            string_resize(str, 2);
            asserteq_buf(str->buffer, "He", 3);
            string_destroy(str);
        });
        it("Resize a string to 0 length", {
            char cstring[] = "Hello World !";
            struct string *str = string_from_cstr(cstring);
            assert(str != NULL);
            string_resize(str, 0);
            asserteq_buf(str->buffer, "", 0);
            asserteq(string_length(str), 0);
            string_destroy(str);
        });
        it("Grow string and then write into data", {
            char cstring[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+-";
            struct string *str = string_init();
            assert(str != NULL);
            asserteq(string_length(str), 0);
            string_resize(str, 64);
            asserteq(str->buf_size, 65);
            asserteq(string_length(str), 64);
            memmove(string_data(str), cstring, sizeof(cstring));
            assert(string_eq_cstr(str, cstring));
            string_destroy(str);
        });
    });
    subdesc(trim, {
        char cstring[] = "      \r\n\n\r Hello World !    \r\n\n\r  ";
        it("Remove the leading spaces of the string", {
            struct string *str = string_from_cstr(cstring);
            assert(str != NULL);
            string_trim_start(str, " \n\r");
            asserteq_str(string_data(str), "Hello World !    \r\n\n\r  ");
            string_destroy(str);
        });
        it("Remove the trailing spaces of the string", {
            struct string *str = string_from_cstr(cstring);
            assert(str != NULL);
            string_trim_end(str, " \n\r");
            asserteq_str(string_data(str), "      \r\n\n\r Hello World !");
            string_destroy(str);
        });
        it("Remove the trailing and leading spaces of the string", {
            struct string *str = string_from_cstr(cstring);
            assert(str != NULL);
            string_trim(str, " \n\r");
            asserteq_str(string_data(str), "Hello World !");
            string_destroy(str);
        });
        it("Remove the last trailing character of the string", {
            struct string *str = string_from_cstr("hello");
            assert(str != NULL);
            string_chop(str);
            asserteq_str(string_data(str), "hell");
            string_destroy(str);
        });
        it("Remove the last trailing new line of the string", {
            struct string *str = string_from_cstr("hello\r\n");
            assert(str != NULL);
            string_chomp(str);
            asserteq_str(string_data(str), "hello");
            string_destroy(str);
            str = string_from_cstr("hello\n\r");
            assert(str != NULL);
            string_chomp(str);
            asserteq_str(string_data(str), "hello");
            string_destroy(str);
        });
        it("Try to remove the last trailing new line of the string but there nothing to do", {
            struct string *str = string_from_cstr("hello");
            assert(str != NULL);
            string_chomp(str);
            asserteq_str(string_data(str), "hello");
            string_destroy(str);
        });
    });
    subdesc(append, {
        it("Append a cstring to a string", {
            struct string *str = string_from_cstr("Hello");
            assert(str != NULL);
            string_append(str, " world !");
            asserteq_str(string_data(str), "Hello world !");
            string_destroy(str);
        });
        it("Append a string to another strings", {
            struct string *str = string_from_cstr("Hello");
            struct string *str1 = string_from_cstr(" world !");
            string_sappend(str, str1);
            asserteq_str(string_data(str), "Hello world !");
            string_destroy(str);
        });
        it("Append a character to a strings", {
            struct string *str = string_from_cstr("Hello");
            string_cappend(str, ' ');
            asserteq_str(string_data(str), "Hello ");
            string_destroy(str);
        });
        it("Append a formatted string to a strings", {
            struct string *str = string_from_cstr("Hello");
            string_fappend(str, " %s %d", "World !", 42);
            asserteq_str(string_data(str), "Hello World ! 42");
            string_destroy(str);
        });
    });
    subdesc(split, {
        it("Split string on spaces", {
            struct string **splitted = NULL;
            struct string *str = string_from_cstr("Hello world !");
            assert(str != NULL);
            ssize_t count = string_split(str, ' ', &splitted, 0);
            asserteq(count, 3);
            asserteq_str(string_data(splitted[0]), "Hello");
            asserteq_str(string_data(splitted[1]), "world");
            asserteq_str(string_data(splitted[2]), "!");
            string_destroy(str);
            for (size_t i = 0; i < count; i++) {
                string_destroy(splitted[i]);
            }
        });
        it("Split string on spaces, to a maximum of 2 string", {
            struct string **splitted = NULL;
            struct string *str = string_from_cstr("Hello world !");
            assert(str != NULL);
            ssize_t count = string_split(str, ' ', &splitted, 2);
            asserteq(count, 2);
            asserteq_str(string_data(splitted[0]), "Hello");
            asserteq_str(string_data(splitted[1]), "world !");
            string_destroy(str);
            for (size_t i = 0; i < count; i++) {
                string_destroy(splitted[i]);
            }
        });
    });
    subdesc(insert, {
        it("Insert a string at the start of the string", {
            struct string *str = string_from_cstr("world !");
            assert(str != NULL);
            string_insert(str, "Hello ", 6);
            asserteq_str(string_data(str), "Hello world !");
            string_destroy(str);
        });
    });
    subdesc(misc, {
        it("Count the number of letter in given string", {
            struct string *str = string_from_cstr("Hello world !");
            assert(str != NULL);
            asserteq(string_count_char(str, 'H'), 1);
            asserteq(string_count_char(str, 'e'), 1);
            asserteq(string_count_char(str, 'l'), 3);
            asserteq(string_count_char(str, ' '), 2);
            asserteq(string_count_char(str, 'd'), 1);
            asserteq(string_count_char(str, '!'), 1);
            asserteq(string_count_char(str, 'z'), 0);
            string_destroy(str);
        });
    });
})