#ifndef COFFRE_STRING_H
#define COFFRE_STRING_H

#include <stdarg.h>
#include <sys/types.h>

struct string;

struct string *string_init();
struct string *string_init_reserve(size_t buf_size);
struct string *string_from_data(const char *, size_t);
struct string *string_from_cstr(const char *);
int string_reserve(struct string *, size_t);
int string_resize(struct string *, size_t);
void string_destroy(struct string *);
void string_destroy_keepdata(struct string *);
int string_nappend(struct string *, const char *, size_t);
int string_append(struct string *, const char *);
/**
 * @return 0 if it success, 1 if it need to be called again, -1 on error
 */
int string_vfappend(struct string *, const char *, va_list);
int string_fappend(struct string *, const char *, ...)
        __attribute__((__format__ (printf, 2, 3)))
        __attribute__((__nonnull__ (2)));
int string_cappend(struct string *, char);
int string_sappend(struct string *, const struct string *);
int string_bappend(struct string *, const void *, size_t);
void string_trim(struct string *, const char[]);
void string_trim_start(struct string *, const char[]);
void string_trim_end(struct string *, const char[]);
void string_chop(struct string *);
void string_chomp(struct string *);
int string_set(struct string *, size_t, char *, size_t);
int string_insert(struct string *, char *, size_t);
void string_remove(struct string *, size_t, size_t);
char *string_data(const struct string *);
size_t string_length(const struct string *);
size_t string_count_char(struct string *, char);
ssize_t string_split(struct string *, char, struct string **[], size_t);
int string_eq(struct string *, struct string *);
int string_eq_cstr(struct string *, const char *);
#endif //COFFRE_STRING_H
