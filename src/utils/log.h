#ifndef COFFRE_LOG_H
#define COFFRE_LOG_H

#include <sys/types.h>
#include <syslog.h>
#include <sys/resource.h>

#define PROF_INIT()                                             \
struct rusage start, stop;                                      \
struct timeval res;                                             \
getrusage(RUSAGE_SELF, &start);

#define PROF_END_START()                                    \
getrusage(RUSAGE_SELF, &stop);                                  \
timersub(&stop.ru_utime, &start.ru_utime, &res);                \
log_info("%d %llds %zums", __COUNTER__, res.tv_sec, res.tv_usec / 1000);  \
getrusage(RUSAGE_SELF, &start);

void log_hexdump(const char *, const char *, size_t);
extern char *log_levels_str[];

#ifdef SNOW_ENABLED
#include <stdio.h>
#define LOG_HELPER(level, fmt, ...) do {if (level <= LOG_WARNING) {fprintf(stderr, "%s: " fmt "%s\n", __FUNCTION__, __VA_ARGS__);}} while(0)
#define LOG_CHELPER(level, ident, fmt, ...) do {if (level <= LOG_WARNING) {fprintf(stderr, "[%s] " fmt "%s\n",  ident, __VA_ARGS__);}} while(0)
#elif defined(DEBUG)
#define STRINGIZE_DETAIL(x) #x
#define STRINGIZE(x) STRINGIZE_DETAIL(x)
#define LOG_HELPER(level, fmt, ...) \
    syslog(level, "%s " \
    "\033[38;5;239m" "%30s:" "\033[0m" "%s: "\
    fmt "%s", \
    log_levels_str[level], \
    __FILE__ ":" STRINGIZE(__LINE__), \
    __FUNCTION__, \
    __VA_ARGS__)
#define LOG_CHELPER(level, ident, fmt, ...) \
    LOG_HELPER(level, "[\033[38;5;%um%s\033[0m] " fmt, ident[0], ident, __VA_ARGS__)
#else
#define LOG_HELPER(level, fmt, ...) syslog(level, "%s: " fmt "%s", __FUNCTION__, __VA_ARGS__)
#define LOG_CHELPER(level, ident, fmt, ...) syslog(level, "[%s] " fmt "%s",  ident, __VA_ARGS__)
#endif

#define log_debug(...)      LOG_HELPER(LOG_DEBUG,   __VA_ARGS__, "")
#define log_info(...)       LOG_HELPER(LOG_INFO,    __VA_ARGS__, "")
#define log_notice(...)     LOG_HELPER(LOG_NOTICE,  __VA_ARGS__, "")
#define log_warning(...)    LOG_HELPER(LOG_WARNING, __VA_ARGS__, "")
#define log_error(...)      LOG_HELPER(LOG_ERR,     __VA_ARGS__, "")
#define log_critical(...)   LOG_HELPER(LOG_CRIT,    __VA_ARGS__, "")
#define log_alert(...)      LOG_HELPER(LOG_ALERT,   __VA_ARGS__, "")
#define log_emerg(...)      LOG_HELPER(LOG_EMERG,   __VA_ARGS__, "")

#define log_cdebug(...)      LOG_CHELPER(LOG_DEBUG,   clientid, __VA_ARGS__, "")
#define log_cinfo(...)       LOG_CHELPER(LOG_INFO,    clientid, __VA_ARGS__, "")
#define log_cnotice(...)     LOG_CHELPER(LOG_NOTICE,  clientid, __VA_ARGS__, "")
#define log_cwarning(...)    LOG_CHELPER(LOG_WARNING, clientid, __VA_ARGS__, "")
#define log_cerror(...)      LOG_CHELPER(LOG_ERR,     clientid, __VA_ARGS__, "")
#define log_ccritical(...)   LOG_CHELPER(LOG_CRIT,    clientid, __VA_ARGS__, "")
#define log_calert(...)      LOG_CHELPER(LOG_ALERT,   clientid, __VA_ARGS__, "")
#define log_cemerg(...)      LOG_CHELPER(LOG_EMERG,   clientid, __VA_ARGS__, "")

#define log_sdebug(...)      LOG_CHELPER(LOG_DEBUG,   safe->name, __VA_ARGS__, "")
#define log_sinfo(...)       LOG_CHELPER(LOG_INFO,    safe->name, __VA_ARGS__, "")
#define log_snotice(...)     LOG_CHELPER(LOG_NOTICE,  safe->name, __VA_ARGS__, "")
#define log_swarning(...)    LOG_CHELPER(LOG_WARNING, safe->name, __VA_ARGS__, "")
#define log_serror(...)      LOG_CHELPER(LOG_ERR,     safe->name, __VA_ARGS__, "")
#define log_scritical(...)   LOG_CHELPER(LOG_CRIT,    safe->name, __VA_ARGS__, "")
#define log_salert(...)      LOG_CHELPER(LOG_ALERT,   safe->name, __VA_ARGS__, "")
#define log_semerg(...)      LOG_CHELPER(LOG_EMERG,   safe->name, __VA_ARGS__, "")

#endif //COFFRE_LOG_H
