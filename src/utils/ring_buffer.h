#ifndef COFFRE_RING_BUFFER_H
#define COFFRE_RING_BUFFER_H

#include <sys/types.h>

struct ring_buf;

struct ring_buf *ringbuf_init(size_t);
void ringbuf_destroy(struct ring_buf *);
int ringbuf_is_full(struct ring_buf *, size_t);
size_t ringbuf_available_space(struct ring_buf *);
size_t ringbuf_available_data(struct ring_buf *);
size_t ringbuf_get_buffer_for_writing(struct ring_buf *, char **, size_t);
size_t ringbuf_get_buffer_for_reading(struct ring_buf *, char **, size_t);
void ringbuf_rewind_read(struct ring_buf *, size_t);
void ringbuf_rewind_write(struct ring_buf *, size_t);
ssize_t ringbuf_write(struct ring_buf *, char *, size_t);
ssize_t ringbuf_read(struct ring_buf *, char *, size_t);
ssize_t ringbuf_indexof(struct ring_buf *, char);

#endif //COFFRE_RING_BUFFER_H
