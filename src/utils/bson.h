#ifndef COFFRE_BSON_H
#define COFFRE_BSON_H

#include <sys/types.h>
#include <stdbool.h>
#include <stdint.h>

enum bson_node_type {
    BSON_TYPE_DOUBLE = 0x01,
    BSON_TYPE_STRING = 0x02,
    BSON_TYPE_DOCUMENT = 0x03,
    BSON_TYPE_ARRAY = 0x04,
    BSON_TYPE_BINARY = 0x05,
    BSON_TYPE_UNDEFINED = 0x065,
    BSON_TYPE_OBJECTID = 0x07,
    BSON_TYPE_BOOL = 0x08,
    BSON_TYPE_UTC_DATE = 0x09,
    BSON_TYPE_NULL = 0x0a,
    BSON_TYPE_REGEX = 0x0b,
    BSON_TYPE_DBPOINTER = 0x0c,
    BSON_TYPE_JS_CODE = 0x0d,
    BSON_TYPE_SYMBOL = 0x0e,
    BSON_TYPE_JS_S_CODE = 0x0f,
    BSON_TYPE_INT32 = 0x10,
    BSON_TYPE_TIMESTAMP = 0x11,
    BSON_TYPE_INT64 = 0x12,
    BSON_TYPE_DECIMAL_128 = 0x13,
    BSON_TYPE_MIN_KEY = 0xff,
    BSON_TYPE_MAX_KEY = 0x7f,
};

struct bson {
    struct string *key;
    char type;
    union {
        struct string *vu_string;
        struct bson *vu_child;
        uint8_t vu_bool;
        int64_t vu_int64;
        uint64_t vu_uint64;
        int32_t vu_int32;
        double vu_double;
    } value;
    union {
        uint8_t vu_subtype;
    } value2;
    struct bson *next;
};

#define v_string    value.vu_string
#define v_child     value.vu_child
#define v_bool      value.vu_bool
#define v_int64     value.vu_int64
#define v_uint64    value.vu_uint64
#define v_int32     value.vu_int32
#define v_double    value.vu_double
#define v_subtype   value2.vu_subtype

struct bson *bson_init(enum bson_node_type);
void bson_destroy(struct bson *);
struct bson *bson_decode(char *, size_t);
struct string *bson_encode(struct bson *);
struct string *bson_print(struct bson *);

bool bson_contains_key(struct bson*, const char *);
bool bson_contains_key_type(struct bson*, const char *, enum bson_node_type);
struct bson *bson_get(struct bson *, const char *);
bool bson_get_binary(struct bson *, const char *, struct string **);
bool bson_get_string(struct bson *, const char *, struct string **);
bool bson_get_int32(struct bson *, const char *, int32_t *);
bool bson_get_int64(struct bson *, const char *, int64_t *);
bool bson_get_timestamp(struct bson *, const char *, time_t *);
bool bson_get_document(struct bson *, const char *, struct bson **);
int bson_set_string(struct bson *, const char *, struct string *);
int bson_set_binary(struct bson *, const char *, struct string *);
int bson_set_cstring(struct bson *, const char *, const char *);
int bson_set_int32(struct bson *, const char *, int32_t);
int bson_set_int64(struct bson *, const char *, int64_t);
int bson_set_bool(struct bson *, const char *, bool);
int bson_set_timestamp(struct bson *, const char *, time_t);
int bson_set_document(struct bson *, const char *, struct bson *);
struct bson *bson_set_empty_array(struct bson *, const char *);
bool bson_remove_from_array(struct bson *, struct bson *);
struct bson *bson_find_in_array(struct bson *, bool (*)(struct bson *, const void *), const void *);

#endif //COFFRE_BSON_H
