#include "log.h"

#include <stdio.h>
#include <ctype.h>
#include <stdint.h>
#include <string.h>
#include <stdarg.h>

void log_hexdump(const char *id, const char *data, size_t len)
{
    char ascii_column[128];
    char hex_column[128];
    size_t ascii_index = 0;
    size_t hex_index = 0;
    int written = 0;
    size_t i = 0;

    syslog(LOG_DEBUG, "Dump: %s p=%p s=%zu", id, data, len);

    if (data == NULL) {
        syslog(LOG_DEBUG, "    (null)");
        return;
    }

    for (i = 0; i < len; i++) {
        if (isprint(data[i])) {
            written = snprintf(ascii_column + ascii_index, sizeof(ascii_column) - ascii_index, "%c", data[i]);
        } else {
            written = snprintf(ascii_column + ascii_index, sizeof(ascii_column) - ascii_index, ".");
        }
        ascii_index += written;
        written = snprintf(hex_column + hex_index, sizeof(hex_column) - hex_index, "%0.2x ", (uint8_t) data[i]);
        hex_index += written;
        if ((i % 16) == 7) {
            written = snprintf(hex_column + hex_index, sizeof(hex_column) - hex_index, " ");
            hex_index += written;
        }
        if ((i % 16) == 15) {
            syslog(LOG_DEBUG, "  %s %s", hex_column, ascii_column);
            bzero(ascii_column, ascii_index);
            bzero(hex_column, hex_index);
            hex_index = 0;
            ascii_index = 0;
        }
    }

    if ((i % 16) != 0) {
        while (hex_index < 49) {
            hex_column[hex_index++] = ' ';
        }
        hex_column[hex_index] = '\0';
        syslog(LOG_DEBUG, "  %s %s", hex_column, ascii_column);
    }
}

char *log_levels_str[] = {
        "\033[48;5;124m\033[38;5;255mEMR\033[0m",
        "\033[48;5;124m\033[38;5;255mALR\033[0m",
        "\033[48;5;124m\033[38;5;255mCRI\033[0m",
        "\033[38;5;88mERR\033[0m",
        "\033[38;5;202mWRN\033[0m",
        "\033[38;5;33mNOT\033[0m",
        "\033[38;5;34mINF\033[0m",
        "\033[38;5;239mDBG\033[0m",
};
