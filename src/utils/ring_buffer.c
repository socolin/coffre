#include "ring_buffer.h"

#include <sys/param.h>
#include <string.h>
#include <stdlib.h>

#include "utils/log.h"
#include "compat/linux.h"

struct ring_buf {
    size_t size;
    size_t mask;
    char *buffer;
    size_t read_pos;
    size_t write_pos;
};

#define RB_INDEX(rb, type) ((rb)->type ## _pos  & (rb)->mask)

struct ring_buf *
ringbuf_init(size_t size)
{
    struct ring_buf *buf = NULL;

    if ((size & (size - 1)) != 0) {
        log_critical("size must be a power of 2, was: %zu", size);
        return NULL;
    }

    buf = calloc(1, sizeof(*buf));
    if (buf == NULL) {
        log_critical("failed to calloc ringbuf metadata: %m");
        return NULL;
    }

    buf->buffer = calloc(size, sizeof(char));
    if (buf->buffer == NULL) {
        log_critical("failed to calloc ringbuf data: %m");
        return NULL;
    }

    buf->size = size;
    buf->mask = size - 1;

    return buf;
}

void
ringbuf_destroy(struct ring_buf *rb)
{
    if (rb == NULL) {
        return;
    }
    freezero(rb->buffer, rb->size);
    freezero(rb, sizeof(*rb));
}

int
ringbuf_is_full(struct ring_buf *rb, size_t len)
{
    return (rb->write_pos - rb->read_pos) + len >= rb->size;
}

size_t
ringbuf_available_space(struct ring_buf *rb)
{
    return rb->size - (rb->write_pos - rb->read_pos);
}

size_t
ringbuf_available_data(struct ring_buf *rb)
{
    return rb->write_pos - rb->read_pos;
}

ssize_t
ringbuf_write(struct ring_buf *rb, char *data, size_t len)
{
    len = MIN(len, ringbuf_available_space(rb));

    size_t first_write = MIN(rb->size - RB_INDEX(rb, write), len);
    memcpy(&(rb->buffer[RB_INDEX(rb, write)]), data, first_write);
    rb->write_pos += first_write;

    size_t second_write = len - first_write;
    if (second_write > 0) {
        memcpy(&(rb->buffer[RB_INDEX(rb, write)]), data + first_write, second_write);
        rb->write_pos += second_write;
    }

    return len;
}

size_t
ringbuf_get_buffer_for_writing(struct ring_buf *rb, char **out_buffer, size_t len)
{
    size_t available = ringbuf_available_space(rb);
    size_t write_len = MIN((rb->size - RB_INDEX(rb, write)), available);
    write_len = MIN(write_len, len);

    *out_buffer = &(rb->buffer[RB_INDEX(rb, write)]);
    rb->write_pos += write_len;
    return write_len;
}

size_t
ringbuf_get_buffer_for_reading(struct ring_buf *rb, char **out_buffer, size_t len)
{
    size_t available = ringbuf_available_data(rb);
    size_t read_count = MIN(rb->size - RB_INDEX(rb, read), available);
    if (len > 0) {
        read_count = MIN(read_count, len);
    }

    *out_buffer = &(rb->buffer[RB_INDEX(rb, read)]);
    rb->read_pos += read_count;

    return read_count;
}

void
ringbuf_rewind_read(struct ring_buf *rb, size_t len)
{
    if (rb->write_pos < len) {
        rb->write_pos += rb->size;
        rb->read_pos += rb->size;
    }

    rb->read_pos -= len;
}

void
ringbuf_rewind_write(struct ring_buf *rb, size_t len)
{
    if (rb->write_pos < len) {
        rb->write_pos += rb->size;
        rb->read_pos += rb->size;
    }

    rb->write_pos -= len;
}

ssize_t
ringbuf_read(struct ring_buf *rb, char *data, size_t len)
{
    if (rb->read_pos == rb->write_pos)
        return 0;

    size_t available = ringbuf_available_data(rb);

    size_t read_count = MIN(rb->size - RB_INDEX(rb, read), available);
    read_count = MIN(read_count, len);

    memcpy(data, &(rb->buffer[RB_INDEX(rb, read)]), read_count);
    rb->read_pos += read_count;

    if (read_count < len) {
        read_count += ringbuf_read(rb, data + read_count, len - read_count);
    }

    if (rb->write_pos == rb->read_pos) {
        rb->write_pos = RB_INDEX(rb, write);
        rb->read_pos = RB_INDEX(rb, read);
    }

    return read_count;
}


ssize_t
ringbuf_indexof(struct ring_buf *rb, char c)
{
    for (size_t i = rb->read_pos; i < rb->write_pos; i++) {
        if (rb->buffer[i & rb->mask] == c) {
            return i - rb->read_pos;
        }
    }

    return -1;
}
