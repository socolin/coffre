#ifndef COFFRE_DICTIONARY_H
#define COFFRE_DICTIONARY_H

#ifdef __linux__
// Should be defined in sys/cdefs.h
#define __unused __attribute__((unused))
#endif
#include <sys/tree.h>

struct dictionary_node {
    RB_ENTRY(dictionary_node) entry;
    char *key;
    void *value;
};

typedef RB_HEAD(dict_tree, dictionary_node) dict_tree_header;
struct dictionary {
    dict_tree_header head;
};

struct dictionary *dictionary_init();
void dictionary_destroy(struct dictionary *);
int dictionary_put(struct dictionary *, const char *, void *);
void *dictionary_get(struct dictionary *, const char *);
void dictionary_remove(struct dictionary *, const char *);

#endif //COFFRE_DICTIONARY_H
