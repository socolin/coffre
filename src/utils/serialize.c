#include "serialize.h"

#include <string.h>
#include <stdbool.h>

#include "log.h"

#define def_serialize_simple_type(type)                                 \
int                                                                     \
serialize_ ## type(struct string *data, type  ## _t value)        \
{                                                                       \
    return string_bappend(data, &value, sizeof(value));                 \
}

def_serialize_simple_type(int8)

def_serialize_simple_type(int32)

def_serialize_simple_type(int64)

def_serialize_simple_type(uint8)

def_serialize_simple_type(uint32)

def_serialize_simple_type(uint64)

def_serialize_simple_type(size)

def_serialize_simple_type(time)

int
serialize_string(struct string *data, const struct string *value)
{
    size_t len = 0;
    int rc = 0;

    len = string_length(value);

    rc = serialize_uint64(data, len);
    if (rc == -1) {
        return -1;
    }

    return string_bappend(data, string_data(value), len);
}

int
serialize_cstring(struct string *data, const char *value)
{
    size_t len = 0;
    int rc = 0;

    len = strlen(value);

    rc = serialize_uint64(data, len);
    if (rc == -1) {
        return -1;
    }

    return string_bappend(data, value, len);
}

int
serialize_data(struct string *data, const void *value, size_t len)
{
    int rc = 0;

    rc = serialize_uint64(data, len);
    if (rc == -1) {
        return -1;
    }

    return string_bappend(data, value, len);
}


#define check_size(len, offset, data_size)                                      \
    if ((len) < (*(offset)) + (data_size)) {                                    \
        log_error("Data to small, trying to read %zu bytes at offset %zu but data is %zu bytes long", \
                  (data_size), *(offset), (len));                               \
        return -1;                                                              \
    }


#define def_deserialize_simple_type(type)                                                       \
int                                                                                             \
deserialize_ ## type(const char *data, size_t len, size_t *offset, type  ## _t *value)          \
{                                                                                               \
    check_size(len, offset, sizeof(*value));                                                    \
    *value = *((type##_t *)((data) + *offset));                                                 \
    *offset += sizeof(*value);                                                                  \
    return 0;                                                                                   \
}

def_deserialize_simple_type(int8)

def_deserialize_simple_type(int32)

def_deserialize_simple_type(int64)

def_deserialize_simple_type(uint8)

def_deserialize_simple_type(uint32)

def_deserialize_simple_type(uint64)

def_deserialize_simple_type(size)

def_deserialize_simple_type(time)

int
deserialize_string(const char *data, size_t len, size_t *offset, struct string **value)
{
    struct string *str = NULL;
    size_t data_size = 0;
    int rc = 0;

    rc = deserialize_size(data, len, offset, &data_size);
    if (rc == -1) {
        return -1;
    }

    check_size(len, offset, data_size);

    str = string_from_data(data + *offset, data_size);
    if (str == NULL) {
        return -1;
    }
    *value = str;
    *offset += data_size;

    return 0;
}

int
deserialize_data(const char *data, size_t len, size_t *offset, void **value, size_t *value_len)
{
    size_t data_size = 0;
    int rc = 0;

    rc = deserialize_size(data, len, offset, &data_size);
    if (rc == -1) {
        return -1;
    }

    check_size(len, offset, data_size);

    *value_len = data_size;
    *value = (void *) (data + (*offset));
    *offset += data_size;

    return 0;
}
