#include "bson.h"

#include <iso646.h>
#include <inttypes.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "string.h"
#include "log.h"
#include "compat/linux.h"

struct bson_parser {
    char *data;
    size_t len;
    size_t pos;
};

struct bson *
bson_init(enum bson_node_type type)
{
    struct bson *bson = NULL;

    bson = calloc(1, sizeof(*bson));
    if (bson == NULL) {
        log_critical("Failed to calloc bson node: %m");
        return NULL;
    }
    bson->type = type;

    return bson;
}

void
bson_destroy(struct bson *bson)
{
    struct bson *cur = NULL;

    while (bson != NULL) {
        cur = bson;
        bson = bson->next;
        switch (cur->type) {
            case BSON_TYPE_DOCUMENT:
            case BSON_TYPE_ARRAY:
                bson_destroy(cur->v_child);
                break;
            case BSON_TYPE_STRING:
            case BSON_TYPE_BINARY:
                string_destroy(cur->v_string);
                break;
            case BSON_TYPE_DOUBLE:
            case BSON_TYPE_UNDEFINED:
            case BSON_TYPE_BOOL:
            case BSON_TYPE_UTC_DATE:
            case BSON_TYPE_NULL:
            case BSON_TYPE_INT32:
            case BSON_TYPE_TIMESTAMP:
            case BSON_TYPE_INT64:
                /* Nothing to do */
                break;
            default:
                log_critical("Not implemented destructor for bson type: %d", cur->type);
                break;
        }
        string_destroy(cur->key);
        freezero(cur, sizeof(*cur));
    }
}

static bool
bson_read_check_len(struct bson_parser *parser, const char *id, size_t data_length)
{
    if (parser->len < parser->pos + data_length) {
        log_error("bson error: Cannot read %s of %zu bytes, "
                  "from position %zu, document length is %zu", id, data_length, parser->pos, parser->len);
        return false;
    }
    return true;
}

static bool
bson_read_data(struct bson_parser *parser, void *dst, size_t data_length)
{
    if (not bson_read_check_len(parser, "data", data_length)) {
        return false;
    }

    memcpy(dst, parser->data + parser->pos, data_length);
    parser->pos += data_length;

    return true;
}

static struct string *
bson_read_cstring(struct bson_parser *parser)
{
    struct string *str = NULL;

    size_t str_length = strnlen(parser->data + parser->pos, parser->len - parser->pos);
    if (str_length == parser->len - parser->pos) {
        log_error("Invalid string length while decoding string node, exceed buffer size");
        return NULL;
    }

    str = string_from_data(parser->data + parser->pos, str_length);
    parser->pos += str_length + 1;

    return str;
}

static struct string *
bson_read_string(struct bson_parser *parser, size_t str_length)
{
    struct string *str = NULL;

    if (not bson_read_check_len(parser, "string", str_length)) {
        return 0;
    }

    str = string_from_data(parser->data + parser->pos, str_length - 1);
    parser->pos += str_length;

    return str;
}

static struct string *
bson_read_binary(struct bson_parser *parser, size_t data_length)
{
    struct string *str = NULL;

    if (not bson_read_check_len(parser, "binary", data_length)) {
        return 0;
    }

    str = string_from_data(parser->data + parser->pos, data_length);
    parser->pos += data_length;

    return str;
}

int
bson_decode_document(struct bson_parser *parser, struct bson **out_bson)
{
    struct bson *node = NULL;
    struct bson *head = NULL;
    struct bson *previous = NULL;
    enum bson_node_type type;
    uint32_t document_length;
    size_t start_pos = parser->pos;
    int8_t byte;
    int rc = 0;

    if (not bson_read_data(parser, &document_length, sizeof(document_length))) {
        goto error;
    }

    if (document_length > parser->len + parser->pos) {
        log_error("Invalid document length: %" PRIu32 ", available data: %zu", document_length,
                  parser->len + parser->pos);
        goto error;
    }

    while (parser->pos - start_pos < document_length) {
        if (not bson_read_data(parser, &byte, sizeof(byte))) {
            goto error;
        }
        type = (enum bson_node_type) byte;

        if (type == 0) {
            if (parser->pos - start_pos != document_length) {
                log_error("Unexpected end of document at pos %zu", parser->pos);
                goto error;
            }
            break;
        }
        node = bson_init(type);
        if (node == NULL) {
            log_error("Failed to alloc bson node");
            goto error;
        }
        if (head == NULL) {
            head = node;
        }
        if (previous != NULL) {
            previous->next = node;
        }
        previous = node;

        node->key = bson_read_cstring(parser);
        if (node->key == NULL) {
            log_error("Failed to allocate string for bson node key");
            goto error;
        }

        switch (type) {
            case BSON_TYPE_STRING: {
                uint32_t string_length;
                if (not bson_read_data(parser, &string_length, sizeof(string_length))) {
                    goto error;
                }

                node->v_string = bson_read_string(parser, string_length);
                if (node->v_string == NULL) {
                    goto error;
                }

                break;
            }
            case BSON_TYPE_BINARY: {
                uint32_t data_length;
                if (not bson_read_data(parser, &data_length, sizeof(data_length))) {
                    goto error;
                }

                if (not bson_read_data(parser, &node->v_subtype, sizeof(node->v_subtype))) {
                    goto error;
                }

                node->v_string = bson_read_binary(parser, data_length);
                if (node->v_string == NULL) {
                    goto error;
                }

                break;
            }
            case BSON_TYPE_ARRAY: {
                rc = bson_decode_document(parser, &node->v_child);
                if (rc == -1) {
                    log_error("Failed to decode bson array of `%s'", string_data(node->key));
                    goto error;
                }
                break;
            }
            case BSON_TYPE_DOCUMENT: {
                rc = bson_decode_document(parser, &node->v_child);
                if (rc == -1) {
                    log_error("Failed to decode bson child of `%s'", string_data(node->key));
                    goto error;
                }
                break;
            }
            case BSON_TYPE_DOUBLE: {
                if (not bson_read_data(parser, &node->v_double, sizeof(node->v_double))) {
                    goto error;
                }
                break;
            }
            case BSON_TYPE_INT32: {
                if (not bson_read_data(parser, &node->v_int32, sizeof(node->v_int32))) {
                    goto error;
                }
                break;
            }
            case BSON_TYPE_UTC_DATE:
            case BSON_TYPE_TIMESTAMP:
            case BSON_TYPE_INT64: {
                if (not bson_read_data(parser, &node->v_int64, sizeof(node->v_int64))) {
                    goto error;
                }
                break;
            }
            case BSON_TYPE_BOOL: {
                if (not bson_read_data(parser, &node->v_bool, sizeof(node->v_bool))) {
                    goto error;
                }
                break;
            }
            case BSON_TYPE_NULL:
            case BSON_TYPE_UNDEFINED:
                /* No data */
                break;
            default:
                log_error("Not implemented bson node decoder: for node `%s' type %d at pos: %zu",
                          string_data(node->key), type, parser->pos);
                goto error;
        }
    }

    *out_bson = head;
    return 0;
error:
    bson_destroy(head);
    return -1;
}

struct bson *
bson_decode(char *data, size_t len)
{
    struct bson *bson = NULL;
    struct bson *child = NULL;
    struct bson_parser parser;
    int rc = 0;

    parser.data = data;
    parser.len = len;
    parser.pos = 0;

    if (len < 4) {
        log_error("Invalid bson: length too small: %zu", len);
        return NULL;
    }

    int32_t document_size = *((int32_t *) data);
    if (len != document_size) {
        log_error("Invalid bson: size does not match %zu != %d", len, document_size);
        return NULL;
    }

    rc = bson_decode_document(&parser, &child);
    if (rc == -1) {
        log_error("Invalid bson: Failed to decode");
        return NULL;
    }

    bson = bson_init(BSON_TYPE_DOCUMENT);
    if (bson == NULL) {
        bson_destroy(child);
        return NULL;
    }
    bson->v_child = child;

    return bson;
}

static ssize_t
bson_encode_document(struct bson *bson, struct string *buf, size_t start_pos)
{
    size_t index = 0;
    uint32_t size = 0;
    struct bson *cur = bson->v_child;
    ssize_t rc;

    rc = string_nappend(buf, (char *) &size, sizeof(size));
    if (rc == -1) {
        return -1;
    }

    while (cur != NULL) {
        rc = string_cappend(buf, cur->type);
        if (rc == -1) {
            return -1;
        }

        if (bson->type == BSON_TYPE_ARRAY) {
            rc = string_fappend(buf, "%zu", index++);
            if (rc == -1) {
                return -1;
            }
            rc = string_cappend(buf, '\0');
            if (rc == -1) {
                return -1;
            }
        } else {
            rc = string_nappend(buf, string_data(cur->key), string_length(cur->key) + 1);
            if (rc == -1) {
                return -1;
            }
        }

        switch (cur->type) {
            case BSON_TYPE_DOCUMENT:
            case BSON_TYPE_ARRAY:
                rc = bson_encode_document(cur, buf, string_length(buf));
                if (rc == -1) {
                    return -1;
                }
                break;
            case BSON_TYPE_STRING: {
                uint32_t str_length = (uint32_t) string_length(cur->v_string) + 1;
                rc = string_bappend(buf, &str_length, sizeof(str_length));
                if (rc == -1) {
                    return -1;
                }
                rc = string_nappend(buf, string_data(cur->v_string), str_length);
                break;
            }
            case BSON_TYPE_BINARY: {
                uint32_t data_length = (uint32_t) string_length(cur->v_string);
                rc = string_bappend(buf, &data_length, sizeof(data_length));
                if (rc == -1) {
                    return -1;
                }
                rc = string_bappend(buf, &cur->v_subtype, sizeof(cur->v_subtype));
                if (rc == -1) {
                    return -1;
                }
                rc = string_nappend(buf, string_data(cur->v_string), data_length);
                break;
            }
            case BSON_TYPE_BOOL:
                rc = string_bappend(buf, &cur->v_bool, sizeof(cur->v_bool));
                break;
            case BSON_TYPE_TIMESTAMP:
            case BSON_TYPE_UTC_DATE:
                rc = string_bappend(buf, &cur->v_uint64, sizeof(cur->v_uint64));
                break;
            case BSON_TYPE_UNDEFINED:
            case BSON_TYPE_NULL:
                break;
            case BSON_TYPE_INT32:
                rc = string_bappend(buf, &cur->v_int32, sizeof(cur->v_int32));
                break;
            case BSON_TYPE_DOUBLE:
                rc = string_bappend(buf, &cur->v_double, sizeof(cur->v_double));
                break;
            case BSON_TYPE_INT64:
                rc = string_bappend(buf, &cur->v_int64, sizeof(cur->v_int64));
                break;
            default:
                log_error("Not supported encode for type: %d for key `%s'", cur->type, string_data(cur->key));
                return -1;
        }
        if (rc == -1) {
            return -1;
        }
        cur = cur->next;
    }


    size = (uint32_t) (string_length(buf) - start_pos) + 1;
    rc = string_set(buf, start_pos, (char *) &size, sizeof(size));
    if (rc == -1) {
        return -1;
    }
    rc = string_nappend(buf, "\0", sizeof(char));
    if (rc == -1) {
        return -1;
    }

    return 0;
}


struct string *
bson_encode(struct bson *bson)
{
    struct string *buf = string_init();
    ssize_t rc = 0;

    if (buf == NULL) {
        return NULL;
    }

    rc = bson_encode_document(bson, buf, 0);
    if (rc == -1) {
        string_destroy(buf);
        return NULL;
    }

    return buf;
}

static int
bson_print_string(struct string *str, struct string *value)
{
    int rc = 0;

    rc = string_cappend(str, '"');
    if (rc == -1) {
        return -1;
    }
    rc = string_sappend(str, value);
    if (rc == -1) {
        return -1;
    }
    rc = string_cappend(str, '"');
    if (rc == -1) {
        return -1;
    }

    return 0;
}

static int
bson_print_document(struct bson *bson, struct string *str)
{
    int rc = 0;
    struct bson *cur = bson->v_child;
    bool is_document = bson->type == BSON_TYPE_DOCUMENT;

    rc = string_append(str, is_document ? "{" : "[");
    if (rc == -1) {
        return -1;
    }

    while (cur != NULL) {
        if (is_document) {
            rc = bson_print_string(str, cur->key);
            if (rc == -1) {
                return -1;
            }
            rc = string_cappend(str, ':');
            if (rc == -1) {
                return -1;
            }
        }

        switch (cur->type) {
            case BSON_TYPE_DOCUMENT:
            case BSON_TYPE_ARRAY:
                rc = bson_print_document(cur, str);
                break;
            case BSON_TYPE_STRING:
                rc = bson_print_string(str, cur->v_string);
                break;
            case BSON_TYPE_BINARY:
                // FIXME: hex printer
                rc = string_append(str, "BINARY DATA");
                break;
            case BSON_TYPE_BOOL:
                rc = string_append(str, cur->v_bool ? "true" : "false");
                break;
            case BSON_TYPE_TIMESTAMP:
            case BSON_TYPE_UTC_DATE:
                rc = string_fappend(str, "%" PRIu64, cur->v_uint64);
                break;
            case BSON_TYPE_UNDEFINED:
                rc = string_append(str, "undefined");
                break;
            case BSON_TYPE_NULL:
                rc = string_append(str, "null");
                break;
            case BSON_TYPE_INT32:
                rc = string_fappend(str, "%" PRId32, cur->v_int32);
                break;
            case BSON_TYPE_DOUBLE:
                rc = string_fappend(str, "%f", cur->v_double);
                break;
            case BSON_TYPE_INT64:
                rc = string_fappend(str, "%" PRId64, cur->v_int64);
                break;
            default:
                rc = string_fappend(str, "<NOT IMPLEMENTED> %d", cur->type);
                break;
        }
        if (rc == -1) {
            return -1;
        }

        rc = string_cappend(str, ',');
        if (rc == -1) {
            return -1;
        }
        cur = cur->next;
    }

    string_trim_end(str, ",");

    rc = string_append(str, is_document ? "}" : "]");
    if (rc == -1) {
        return -1;
    }

    return 0;
}

struct string *
bson_print(struct bson *bson)
{
    struct string *str = NULL;
    int rc = 0;

    str = string_init();
    if (str == NULL) {
        return str;
    }

    rc = bson_print_document(bson, str);
    if (rc == -1) {
        string_destroy(str);
        return NULL;
    }

    return str;
}

bool
bson_contains_key(struct bson *bson, const char *key)
{
    return bson_get(bson, key) != NULL;
}

bool
bson_contains_key_type(struct bson *bson, const char *key, enum bson_node_type type)
{
    struct bson *b = NULL;
    b = bson_get(bson, key);
    return b != NULL && b->type == type;
}

struct bson *
bson_get(struct bson *bson, const char *key)
{
    for (struct bson *b = bson->v_child; b != NULL; b = b->next) {
        if (string_eq_cstr(b->key, key)) {
            return b;
        }
    }

    return NULL;
}

bool
bson_get_int32(struct bson *bson, const char *key, int32_t *out_value)
{
    struct bson *b = NULL;

    b = bson_get(bson, key);
    if (b != NULL) {
        if (b->type != BSON_TYPE_INT32) {
            return false;
        }
        *out_value = b->v_int32;
        return true;
    }

    return false;
}

bool
bson_get_int64(struct bson *bson, const char *key, int64_t *out_value)
{
    struct bson *b = NULL;

    b = bson_get(bson, key);
    if (b != NULL) {
        if (b->type == BSON_TYPE_INT64) {
            *out_value = b->v_int64;
        }
        else if (b->type == BSON_TYPE_INT32) {
            *out_value = b->v_int32;
        }
        else {
            return false;
        }
        return true;
    }

    return false;
}

bool
bson_get_timestamp(struct bson *bson, const char *key, time_t *out_value)
{
    struct bson *b = NULL;

    b = bson_get(bson, key);
    if (b != NULL) {
        if (b->type != BSON_TYPE_TIMESTAMP) {
            return false;
        }
        *out_value = b->v_int64;
        return true;
    }

    return false;
}

bool
bson_get_binary(struct bson *bson, const char *key, struct string **out_value)
{
    struct bson *b = NULL;

    b = bson_get(bson, key);
    if (b != NULL) {
        if (b->type != BSON_TYPE_BINARY) {
            return false;
        }
        *out_value = b->v_string;
        return true;
    }

    return false;
}

bool
bson_get_string(struct bson *bson, const char *key, struct string **out_value)
{
    struct bson *b = NULL;

    b = bson_get(bson, key);
    if (b != NULL) {
        if (b->type != BSON_TYPE_STRING) {
            return false;
        }
        *out_value = b->v_string;
        return true;
    }

    return false;
}

bool
bson_get_document(struct bson *bson, const char *key, struct bson **out_value)
{
    struct bson *b = NULL;

    b = bson_get(bson, key);
    if (b != NULL) {
        if (b->type != BSON_TYPE_DOCUMENT) {
            return false;
        }
        *out_value = b;
        return true;
    }

    return false;
}

static void
bson_set(struct bson *bson, const char *key, struct bson *value)
{
    struct bson *prev = NULL;
    struct bson *cur = NULL;

    assert(bson->type == BSON_TYPE_DOCUMENT);

    for (cur = bson->v_child; cur != NULL; cur = cur->next) {
        if (string_eq_cstr(cur->key, key)) {
            break;
        }
        prev = cur;
    }
    bson_destroy(cur);
    if (prev != NULL) {
        prev->next = value;
    } else {
        bson->v_child = value;
    }
}

int
bson_set_string(struct bson *bson, const char *key, struct string *str)
{
    struct bson *value = NULL;

    value = bson_init(BSON_TYPE_STRING);
    if (value == NULL) {
        return -1;
    }
    value->v_string = str;
    value->key = string_from_cstr(key);
    if (value->key == NULL) {
        return -1;
    }

    bson_set(bson, key, value);

    return 0;
}

int
bson_set_binary(struct bson *bson, const char *key, struct string *data)
{
    struct bson *value = NULL;

    value = bson_init(BSON_TYPE_BINARY);
    if (value == NULL) {
        return -1;
    }
    value->v_string = data;
    value->v_subtype = 0;
    value->key = string_from_cstr(key);
    if (value->key == NULL) {
        return -1;
    }

    bson_set(bson, key, value);

    return 0;
}

int
bson_set_cstring(struct bson *bson, const char *key, const char *cstr)
{
    struct bson *value = NULL;
    struct string *str = NULL;

    str = string_from_cstr(cstr);
    if (str == NULL) {
        return -1;
    }

    value = bson_init(BSON_TYPE_STRING);
    if (value == NULL) {
        string_destroy(str);
        return -1;
    }
    value->v_string = str;
    value->key = string_from_cstr(key);
    if (value->key == NULL) {
        string_destroy(str);
        bson_destroy(value);
        return -1;
    }

    bson_set(bson, key, value);

    return 0;
}

int
bson_set_int32(struct bson *bson, const char *key, int32_t num)
{
    struct bson *value = NULL;

    value = bson_init(BSON_TYPE_INT32);
    if (value == NULL) {
        return -1;
    }

    value->v_int32 = num;
    value->key = string_from_cstr(key);
    if (value->key == NULL) {
        return -1;
    }

    bson_set(bson, key, value);

    return 0;
}

int
bson_set_int64(struct bson *bson, const char *key, int64_t num)
{
    struct bson *value = NULL;

    value = bson_init(BSON_TYPE_INT64);
    if (value == NULL) {
        return -1;
    }

    value->v_int64 = num;
    value->key = string_from_cstr(key);
    if (value->key == NULL) {
        return -1;
    }

    bson_set(bson, key, value);

    return 0;
}

int
bson_set_bool(struct bson *bson, const char *key, bool v)
{
    struct bson *value = NULL;

    value = bson_init(BSON_TYPE_BOOL);
    if (value == NULL) {
        return -1;
    }

    value->v_bool = (uint8_t) v;
    value->key = string_from_cstr(key);
    if (value->key == NULL) {
        return -1;
    }

    bson_set(bson, key, value);

    return 0;

}

int
bson_set_timestamp(struct bson *bson, const char *key, time_t timestamp)
{
    struct bson *value = NULL;

    value = bson_init(BSON_TYPE_TIMESTAMP);
    if (value == NULL) {
        return -1;
    }

    value->v_int64 = timestamp;
    value->key = string_from_cstr(key);
    if (value->key == NULL) {
        return -1;
    }

    bson_set(bson, key, value);

    return 0;
}

int
bson_set_document(struct bson *bson, const char *key, struct bson *value)
{
    if (value->key == NULL) {
        value->key = string_from_cstr(key);
        if (value->key == NULL) {
            return -1;
        }
    } else if (not string_eq_cstr(value->key, key)) {
        string_destroy(value->key);
        value->key = string_from_cstr(key);
        if (value->key == NULL) {
            return -1;
        }
    }

    bson_set(bson, key, value);

    return 0;
}

struct bson *
bson_set_empty_array(struct bson *bson, const char *key)
{
    struct bson *value = NULL;
    int rc = 0;

    value = bson_init(BSON_TYPE_ARRAY);
    if (value == NULL) {
        return NULL;
    }

    rc = bson_set_document(bson, key, value);
    if (rc == -1) {
        log_error("Failed set array in bson with key `%s'", key);
        bson_destroy(value);
        return NULL;
    }

    return value;
}

struct bson *
bson_find_in_array(struct bson *array, bool (*cb)(struct bson *, const void*), const void *cb_arg)
{
    struct bson *child = NULL;

    child = array->v_child;
    while (child != NULL) {
        if (cb(child, cb_arg)) {
            return child;
        }
        child = child->next;
    }

    return NULL;
}

bool
bson_remove_from_array(struct bson *array, struct bson *node)
{
    struct bson *child = NULL;
    struct bson *previous = NULL;

    if (array->type != BSON_TYPE_ARRAY) {
        return false;
    }

    child = array->v_child;
    while (child != NULL) {
        if (child == node) {
            if (previous != NULL) {
                previous->next = child->next;
            } else {
                array->v_child = child->next;
                child->next = NULL;
            }
            return true;
        }
        previous = child;
        child = child->next;
    }

    return false;
}
