#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <event.h>

#include "core/privsep.h"
#include "core/conf.h"
#include "core/privsep_master.h"
#include "network/network.h"
#include "core/msg_queue.h"
#include "safe/safe.h"
#include "utils/log.h"
#include "compat/linux.h"

#ifdef SNOW_ENABLED
#include <snow/snow.h>
snow_main();
#else

__dead
void
usage()
{
    // FIXME
    exit(1);
}

static int
start_children()
{
    struct privsep_process *network_child = NULL;
    struct privsep_process *safe_child = NULL;
    struct event_base *event = NULL;
    int rc = 0;

    // 1)

    event = event_base_new();
    if (event == NULL) {
        log_error("Failed to allocate event_base");
        return -1;
    }

    // 2)

    network_child = start_child(PRIVSEP_NETWORK_NAME, start_network, event, privsep_master_process_msg);
    if (!network_child) {
        return -1;
    }

    // 3)

    safe_child = start_child(PRIVSEP_SAFE_NAME, start_safe, event, privsep_master_process_msg);
    if (!safe_child) {
        return -1;
    }

    // 4)

    rc = pledge("stdio", NULL);
    if (rc == -1) {
        return -1;
    }

    event_base_dispatch(event);

    event_base_free(event);

    return 0;
}


int
main(int argc, char *argv[], char *envp[])
{
    int ch;
    int log_flags;
    int log_level;
    const char *conf_file = DEFAULT_CONF_PATH;

#ifdef __linux__
    setproctitle_init(argc, argv, envp);
#endif
    conf_init();

    while ((ch = getopt(argc, argv, "df:")) != -1) {
        switch (ch) {
            case 'd':
                g_config.debug = 1;
                break;
            case 'f':
                conf_file = optarg;
                break;
            default:
                usage();
        }
    }
    argc -= optind;
    argv += optind;

    log_level = LOG_INFO;
    log_flags = LOG_NDELAY | LOG_PID;
    if (g_config.debug) {
        log_flags |= LOG_PERROR;
        log_level = LOG_DEBUG;
    }

    openlog("coffre[priv]", log_flags, LOG_DAEMON);
    setlogmask(LOG_UPTO(log_level));

    if (conf_load(conf_file) == -1) {
        return 1;
    }

    g_config.log_level = log_level;
    g_config.log_flags = log_flags;

    if (start_children() == -1) {
        return 1;
    }

    return 0;
}

#endif