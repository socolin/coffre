CC=clang
CFLAGS=-g -fcolor-diagnostics -O0 -DDEBUG -Wall
CPPFLAGS=-MMD -Isrc/ -Ilib/snow
LDLIBS=-ltls -levent

SRC_DIR=src/
SRC_FILES=\
	main.c \
	core/conf.c \
	core/msg_protocol.c \
	core/msg_queue.c \
	core/privsep.c \
	core/privsep_master.c \
	network/client.c \
	network/network.c \
	network/websocket.c \
	safe/directory.c \
	safe/safe.c \
	safe/safe_crypto.c \
	safe/safe_impl.c \
	utils/ring_buffer.c \
	utils/dictionary.c \
	utils/string.c \
	utils/log.c \
	utils/fs.c \
	utils/bson.c \
	utils/serialize.c \
	utils/utils.c

-include Makefile.linux

SRCS=$(addprefix $(SRC_DIR), $(SRC_FILES))

OBJ_DIR=obj
OBJ_TEST_DIR=obj-test
OBJS = $(patsubst src/%.c,obj/%.o,$(SRCS))
OBJS_TEST = $(patsubst src/%.c,obj-test/%.o,$(SRCS))
DEPS=$(OBJS:.o=.d)
DEPS_TEST=$(OBJS_TEST:.o=.d)

TARGET=coffre

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS)

test: test-$(TARGET)
	@rm -rf test_safes/
	@mkdir -p test_safes/
	LD_LIBRARY_PATH=${LIBRESSL_LIB_PATH} ./test-$(TARGET)

test-$(TARGET): $(OBJS_TEST)
	$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS)

-include $(DEPS)
-include $(DEPS_TEST)

obj-test/%.o: CPPFLAGS += -DSNOW_ENABLED
obj-test/%.o: src/%.c
	@mkdir -p $(dir $@)
	$(COMPILE.c) $(OUTPUT_OPTION) $<

obj/%.o: src/%.c
	@mkdir -p $(dir $@)
	$(COMPILE.c) $(OUTPUT_OPTION) $<

clean:
	rm -f $(OBJS) $(TARGET) $(DEPS) $(OBJS_TEST)
	rm -rf $(OBJ_DIR) $(OBJ_TEST_DIR)
