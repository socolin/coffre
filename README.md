
Coffre
======

Introduction
------------

Coffre is a file storage, accessible via a web interface, that aim to be
secure. The backend is written in C, the frontend is an Angular webapp.

Each files are store encrypted with a unique key, and these key are
encrypted with an RSA key. The private key is protected by a passphrase
which must be entered each time to access files. All other metadata like
folder name, file date/type etc... are also encrypted.

Architecture
------------

The client use a websocket to contact the backend. The protocol between
client and server use messages encoded in BSON.

The server is splitted in multiple process, to isolate each
functionality. All the network part is handle in one process,
and the crypto in another, and then a preview process is planned to
generate preview image of documents. Each process are
[chrooted](http://man.openbsd.org/man2/chroot.2),
[pledged](http://man.openbsd.org/pledge) and drop their privileges, to
mitigate potential security issue.

Compatibility
-------------

- OpenBSD 6.2

Build
-----
```
gmake
```

Run
---
```
doas ./coffre -f coffre.conf -d
```

Todo
----
- Create folder
- Delete folder
- Cancel upload
- Preview image/pdf
- "Multiple passphrase", with various permission, one per device
- Quota
- Maximum number of clients
- Custom file/folder icon
- Index for search
- Send error code when possible to client
- Remove clients files' information from logs (file names, ...)

Library
-------

- Libressl <http://www.libressl.org/>

Protocol
--------

- [Details of communications between client and backend processes](https://www.lucidchart.com/documents/view/b950e1c8-d88b-4c47-8990-3b88c8b8c5fa)

License
-------

MIT


Refactor
--------
safe_process_*_msg, safe as argument if possible